##PRO 2017 - Informations sur la charte graphique et la convention de codage##

###Charte graphique###

####Labels####

4 sortes de labels

**Label normal** appellés *.label* dans la feuille de style *application.css*

| Caractéristique | Valeur          |
| -------------------- |-----------------|
| Police                |   *Segeo UI*|
| Taille                 |       *11 pts* |
| Effet                  | *Semi-bold*|
| Couleur             | *Noir (#000000)*          | 
| Opacité             | *60%*         | 

**Label erreur** appellés *.label_erreur* dans la feuille de style *application.css*

| Caractéristique | Valeur          |
| -------------------- |-----------------|
| Police                |   *Segeo UI*|
| Taille                 |       *9 pts*   |
| Effet                  | *Semi-bold*|
| Couleur             | *#8B0000* | 
| Opacité             | *60%*         | 

**Label erreur générale** appellés *.label_erreur_general* dans la feuille de style *application.css*

| Caractéristique | Valeur          |
| -------------------- |-----------------|
| Police                |   *Segeo UI*|
| Taille                 |       *12 pts* |
| Effet                  | *Semi-bold*|
| Couleur             | *#8B0000* | 
| Opacité             | *60%*         | 

Contrairement au label erreur simple, il est utilisé dans les fenêtre ou plusieur erreurs complétement différentes entre elles peuvent se produire. Par exemple dans le fenetre du chef de projet (*ListeProjetChefProjet*). 

**Titre** appellés *.titre* dans la feuille de style *application.css*

| Caractéristique | Valeur          |
| -------------------- |-----------------|
| Police                |   *Segeo UI*|
| Taille                 |       *15 pts* |
| Effet                  | *Semi-bold* & *Italic*|
| Couleur             | *Noir (#000000)* | 
| Opacité             | *100%*         | 

####Boutons####

**Général**

*Padding interne* : 5 pixels verticalement, 22 pixels horizontalement
*Couleur texte* : blanc 

| Caractéristique | Valeur          |
| -------------------- |-----------------|
| Padding interne |   *5 pixels verticalement, 22 pixels horizontalement*|
| Couleur texte    |   *Blanc (#FFFFFF)* |
| Couleur quand survolé | *#3a3a3a* |
| Couleur  quand inactif | *#1d1d1d* | 

**Boutons verts** appellés *.bt_rechercher* dans la feuille de style *application.css*

| Caractéristique | Valeur          |
| -------------------- |----------------|
| Couleur             | *#339933* | 

**Boutons rouges** appellés *.bt_suppression* dans la feuille de style *application.css*

| Caractéristique | Valeur          |
| -------------------- |----------------|
| Couleur             | *#cc0000* | 


**Boutons gris** appellés *.bt_normal* dans la feuille de style *application.css*

| Caractéristique | Valeur          |
| -------------------- |----------------|
| Couleur             | *#999999* | 

**Bouton play/pause** appellé *.bt_pause* dans la feuille de style *application.css*

| Caractéristique | Valeur          |
| -------------------- |----------------|
| Couleur             | *#e6ac00* | 

**Bouton connexion** appellé *.bt_connexion* dans la feuille de style *application.css*

| Caractéristique | Valeur          |
| -------------------- |----------------|
| Couleur             | *#0099ff* | 

####Champs de texte####

| Caractéristique | Valeur          |
| -------------------- |-----------------|
| Police                |   *Segeo UI*|
| Taille                 |       *12 pts* |
| Effet                  | *Semi-bold*|

####Tailles de fenetres####

3 tailles de fenetres

|  | Taille          |
| -----------------|-----------------|
| Grande          |  *1000x625*|
| Moyenne       | *600x460*|
| Petite            | *460x300*|

**Grandes**

* MenuAdmin
* ListeProjetUtilisateur
* ListeProjetsChefProjet
* ListeTachesUtilisateur

**Moyennes**

* AjoutUtilisateurModule
* NouveauModule
* NouveauProjet

**Petites** 

* Connexion
* Création tâche
* TacheRemarques
* TimerTache

####Logos####

**Logo mini pour le cadre des fenêtres**

![](/home/lara/cours/PRO/pro_2017/WorksOn/src/main/java/Ressources/miniLogo.png) 

**Logo de la fenêtre de connexion**

![](/home/lara/cours/PRO/pro_2017/WorksOn/src/main/java/Ressources/logo.png) 

####Résumé####

**Couleurs**

| Couleur         | Valeur web  |
| -----------------|-----------------|
| Noir          |  *#000000* |
| Blanc       | *#FFFFFF* |
| Rouge label | *#8B0000* |
| Rouge bouton | *#cc0000* |
| Gris inactif | *#1d1d1d* |
| Gris survol | *#3a3a3a* |
| Gris | *#999999* |
| Vert | *#339933* |
| Bleu | *#0099ff* |
| Jaune | *#e6ac00* |

**Tailles**

|  Fenetre | Taille          |
| -----------------|-----------------|
| Grande          |  *1000x625*|
| Moyenne       | *600x460*|
| Petite            | *460x300*|

###Convention de codage###

####Extrait de code####

	public ListeProjets listeProjetsChef(String idChef)
	{
       	RequeteServeur requete = new RequeteServeur(ProtocoleWorksOn.CMD_GET_LISTE_PROJET_CHEF, idEntreprise, idChef);

        writer.println(gson.toJson(requete));
        writer.flush();

        ListeProjets reponse = null;

      	try
      	{
        	reponse = gson.fromJson(reader.readLine(), ListeProjets.class);
      	} catch (IOException e)
      	{
        	Journalisation.getERR().severe("Impossible de lire la réponse du serveur : " + e.getMessage());
      	}

      	return reponse;
   	}

####Autres####

**Taille maximale d'une ligne**  : 120 caractères

**Niveau de langage** : 1.8 (Lambdas, type annotations, etc)

**Convention de nommage utilisée dans le code et les commentaire** : En français, sauf pour les getters et les setters qui sont malgré tout préfixés en avec get et set.
