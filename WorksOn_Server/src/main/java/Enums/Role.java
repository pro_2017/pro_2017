package Enums;

/**
 * Classe représentant une enumération pour les rôles
 */
public enum Role
{
   UTILISATEUR,
   CHEF_PROJET,
   ADMINISTRATEUR;
}
