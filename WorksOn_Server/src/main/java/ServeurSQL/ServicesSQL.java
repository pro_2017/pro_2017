package ServeurSQL;

import Objets.*;
import Protocoles.ProtocoleWorksOn;
import Protocoles.ReponseConnexion;
import Utilitaires.GenerateurHash;
import Utilitaires.GenerateurSel;
import Utilitaires.GenerateurToken;
import Utilitaires.Journalisation;
import org.hibernate.Session;

import java.sql.Timestamp;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe permettant de traiter les requêtes du client reçues par le GestionnaireClient.
 * Chaque requête ouvre une connexion vers la BDD et la ferme une fois effectuée.
 */
public class ServicesSQL
{
   private final int CODE_ENTREPRISE = 1;
   private final int CODE_TOKEN = 2;
   private final int CODE_EMPLOYE = 3;
   private final int CODE_PROJET = 4;
   private final int CODE_MODULE = 5;
   private final int CODE_TACHE = 6;
   private final int CODE_LOG = 7;
   private final int CODE_MODULE_EFFECTIF = 8;
   private final int CODE_EMPLOYE_EXISTANT = 9;

   /**
    * getListeModuleEffectif retourne la liste de l'effectif d'un module correspandant à l'id donné
    *
    * @param idModule Int contenant l'id du module
    * @return Une liste de module effectif contenant les id de tous les employés
    */
   public List<ModuleEffectif> getListeModuleEffectif(int idModule)
   {
      Session session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();
      String hql = "SELECT  E FROM Objets.ModuleEffectif E join E.module M  where M.id =:idModule";
      List<ModuleEffectif> resultat = (List<ModuleEffectif>) session.createQuery(hql).setParameter("idModule", idModule).list();
      session.getTransaction().commit();
      return resultat;
   }

   /**
    * getListeModuleEmploye retourne la liste de l'effectif d'un module correspandant à l'id de l'employe donné
    *
    * @param idEmploye Int contenant l'id du module
    * @return Une liste de module effectif contenant les id de tous les mdoules de l'employe
    */
   public List<ModuleEffectif> getListeModuleEmploye(int idEmploye)
   {
      Session session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();
      String hql = "SELECT M from Objets.ModuleEffectif M join M.employe E where E.id =:idEmploye";
      List<ModuleEffectif> resultat = (List<ModuleEffectif>) session.createQuery(hql).setParameter("idEmploye", idEmploye).list();

      session.getTransaction().commit();
      return resultat;
   }

   /**
    * getEntreprise retourne l'entreprise correspandante à l'id donné
    *
    * @param entrepriseId id de l'entreprise
    * @return un objet entreprise contenant toutes les informations sur l'entreprise
    */
   private Entreprise getEntreprise(int entrepriseId)
   {
      Session session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();
      String hql = "from Objets.Entreprise E where E.idEntreprise =:entrepriseId";
      List<Entreprise> resultat = (List<Entreprise>) session.createQuery(hql).setParameter("entrepriseId", entrepriseId).list();
      session.getTransaction().commit();
      if (resultat.isEmpty())
      {
         return null;
      }
      else
      {
         return resultat.get(0);
      }
   }

   /**
    * getEntreprise retourne l'entreprise correspandante au nom donné
    *
    * @param entrepriseNom String contenant le nom de l'entreprise
    * @return Un objet Entreprise contenant toutes les informations sur l'entreprise
    */
   public Entreprise getEntreprise(String entrepriseNom)
   {
      Session session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();
      String hql = "from Objets.Entreprise E where E.nom =:entrepriseNom";
      List<Entreprise> resultat = (List<Entreprise>) session.createQuery(hql).setParameter("entrepriseNom", entrepriseNom).list();
      session.getTransaction().commit();
      if (resultat.isEmpty())
      {
         return null;
      }
      else
      {
         return resultat.get(0);
      }
   }

   /**
    * getListeProjets retourne tous les projets d'une entreprise donnée
    *
    * @param entrepriseId Id de l'entreprise
    * @return Une liste de projets
    */
   public List<Projet> getListeProjets(int entrepriseId)
   {
      Session session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();
      String hql = "select P from Objets.Projet P join P.chefProjet E join E.entreprise O WHERE O.idEntreprise =:entrepriseId";
      List<Projet> resultat = (List<Projet>) session.createQuery(hql).setParameter("entrepriseId", entrepriseId).list();
      session.getTransaction().commit();
      return resultat;
   }

   /**
    * getListeProjetsEmploye retourne tous les projets d'un employé donné
    *
    * @param employeId numéro de l'employé
    * @return une liste de projets
    */
   public List<Projet> getListeProjetsEmploye(int employeId)
   {
      Session session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();
      String hql = "select distinct P from Objets.ModuleEffectif E join E.module M join M.projet P WHERE E.employe.id =:employeId";
      List<Projet> resultat = (List<Projet>) session.createQuery(hql).setParameter("employeId", employeId).list();
      session.getTransaction().commit();
      return resultat;
   }

   /**
    * getListeProjetsChef retoune tous les projets qui sont supervisés par le chef de projet donné
    *
    * @param chefProjetId Numéro d'employé du chef de projet
    * @return Une liste de projets
    */
   public List<Projet> getListeProjetsChef(int chefProjetId)
   {
      Session session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();
      String hql = "from Objets.Projet P where P.chefProjet.id =:chefProjetId";
      List<Projet> resultat = (List<Projet>) session.createQuery(hql).setParameter("chefProjetId", chefProjetId).list();
      session.getTransaction().commit();
      return resultat;
   }

   /**
    * getListeModules retourne tous les modules d'un projet donné
    *
    * @param projetId = Numéro du projet
    * @return Retourne une liste de modules
    */
   public List<Module> getListeModules(int projetId)
   {
      Session session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();
      String hql = "from Objets.Module M where M.projet.id =:projetId";
      List<Module> resultat = (List<Module>) session.createQuery(hql).setParameter("projetId", projetId).list();
      session.getTransaction().commit();
      return resultat;
   }

   /**
    * getListeTachesModules retourne la liste des tâches d'un module donné
    *
    * @param moduleId Numéro du module
    * @return Retourne une liste de tâches
    */
   public List<Tache> getListeTachesModules(int moduleId)
   {
      Session session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();
      String hql = "from Objets.Tache T where T.module.id =:moduleId";
      List<Tache> resultat = (List<Tache>) session.createQuery(hql).setParameter("moduleId", moduleId).list();
      session.getTransaction().commit();
      return resultat;
   }

   /**
    * getListeTaches retourne toutes les tâches d'un employé donné sur un module donné
    *
    * @param employeId Numéro de l'employé
    * @param moduleId  Numéro du module
    * @return Retourne une liste de tâches
    */
   public List<Tache> getListeTachesEmployeModule(int employeId, int moduleId)
   {
      Session session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();
      String hql = "from Objets.Tache T where T.employe.id =:employeId and T.module.id =:moduleId";
      List<Tache> resultat = (List<Tache>) session.createQuery(hql).setParameter("employeId", employeId).setParameter("moduleId", moduleId).list();
      session.getTransaction().commit();
      return resultat;
   }

   /**
    * suprressionInfosSensibles supprime les mots de passes et les sels des employes avant de les envoyer
    *
    * @param listeEmploye Listes d'employes
    * @return La liste d'employés reçu en paramètres reçus sans les mots de passes et les sels
    */
   public List<Employe> suprressionInfosSensibles(List<Employe> listeEmploye)
   {
      //pour des raisons de securités ne retourne pas les hash des mots de passe et le sel
      for (Employe employe : listeEmploye)
      {
         //met le mdp et le sel a null par mesure de sécurité
         employe.setMotDePasse("");
         employe.setSel("");
      }
      return listeEmploye;
   }

   /**
    * ajouteModulesAuxEmployes ajoute aux listes d'employe les modules de chaque employe
    *
    * @param listeEmploye Liste d'employes
    * @return La même liste que reçu en paramètres mais avec les modules des employés ajoutés
    */
   public List<Employe> ajouteModulesAuxEmployes(List<Employe> listeEmploye)
   {
      for (Employe employe : listeEmploye)
      {
         //récupère les modules de l'utilisateur
         List<Module> listeModules = new ArrayList<Module>();
         List<ModuleEffectif> moduleEffectifs = getListeModuleEmploye(employe.getId());

         //charge la liste des employés
         for (ModuleEffectif moduleEffectif : moduleEffectifs)
         {
            listeModules.add(moduleEffectif.getModule());
         }

         //ajoute les modules dans la liste des modules
         employe.setListeDeModules(listeModules);
      }
      return listeEmploye;
   }

   /**
    * getListeEmployesModule retourne la liste des employés travaillant sur un module donné
    *
    * @param moduleId Numéro du module
    * @return Une liste d'employés
    */
   public List<Employe> getListeEmployesModule(int moduleId)
   {
      Session session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();
      String hql = "SELECT E.employe FROM Objets.ModuleEffectif E WHERE E.module.id = :moduleId";
      List<Employe> resultattmp = session.createQuery(hql).setParameter("moduleId", moduleId).list();
      session.getTransaction().commit();
      List<Employe> resultat = suprressionInfosSensibles(resultattmp);
      resultat = ajouteModulesAuxEmployes(resultat);
      return resultat;
   }

   /**
    * getListeEmployesProjet retourne une liste avec tous les employés travaillant sur un projet donné
    *
    * @param projetId Numéro du projet
    * @return Une liste d'employés
    */
   public List<Employe> getListeEmployesProjet(int projetId)
   {
      Session session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();
      String hql = "select distinct E.employe from Objets.ModuleEffectif E join E.module M join M.projet P WHERE P.id =:projetId";
      List<Employe> resultattmp = (List<Employe>) session.createQuery(hql).setParameter("projetId", projetId).list();
      session.getTransaction().commit();
      List<Employe> resultat = suprressionInfosSensibles(resultattmp);
      resultat = ajouteModulesAuxEmployes(resultat);
      return resultat;
   }

   /**
    * getListeEmployesProjet retourne une liste avec tous les employés travaillant pour une entreprise donnée
    *
    * @param entrepriseId Numéro de l'entreprise
    * @return Une liste d'employés
    */
   public List<Employe> getListeEmployes(int entrepriseId)
   {
      Session session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();
      String hql = "from Objets.Employe E where E.entreprise.idEntreprise =:entrepriseId";
      List<Employe> resultattmp = (List<Employe>) session.createQuery(hql).setParameter("entrepriseId", entrepriseId).list();
      session.getTransaction().commit();
      List<Employe> resultat = suprressionInfosSensibles(resultattmp);
      resultat = ajouteModulesAuxEmployes(resultat);
      return resultat;
   }

   /**
    * getEmploye retourne les informations d'un employé donné avec sel et mot de passe mis à null
    *
    * @param employeId Numéro de l'employé
    * @return Retourne un objet employé contenant toutes les infos de celui-ci
    */
   public Employe getEmploye(int employeId)
   {
      List<Module> listeModules = new ArrayList<Module>();
      Session session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();
      String hql = "FROM Objets.Employe E WHERE E.id = :employeId";
      Employe employe = (Employe) session.createQuery(hql).setParameter("employeId", employeId).uniqueResult();
      List<ModuleEffectif> moduleEffectifs = getListeModuleEmploye(employeId);

      //charge la liste des employés
      for (ModuleEffectif moduleEffectif : moduleEffectifs)
      {
         listeModules.add(moduleEffectif.getModule());
      }

      if (employe == null)
      {
         Journalisation.getERR().warning("L'employe avec l'id: " + employeId + "n'existe pas!");
         return null;
      }

      employe.setListeDeModules(listeModules);
      //set a null le mot de passe pour des raisons de sécurité
      employe.setMotDePasse("");
      employe.setSel("");
      return employe;
   }

   /**
    * getEmploye retourne les informations d'un employé donné sans avoir mis à nul son mdp et sel
    * SEULEMENT A USAGE INTERNE SUR LE SERVEUR
    *
    * @param employeId Numéro de l'employé
    * @return Retourne un objet employé contenant toutes les infos de celui-ci
    */
   public Employe getEmployeAvecMDP(int employeId)
   {
      List<Module> listeModules = new ArrayList<Module>();
      Session session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();
      String hql = "FROM Objets.Employe E WHERE E.id = :employeId";
      Employe employe = (Employe) session.createQuery(hql).setParameter("employeId", employeId).uniqueResult();
      List<ModuleEffectif> moduleEffectifs = getListeModuleEmploye(employeId);

      //charge la liste des employés
      for (ModuleEffectif moduleEffectif : moduleEffectifs)
      {
         listeModules.add(moduleEffectif.getModule());
      }


      if (employe == null)
      {
         Journalisation.getERR().warning("L'employe avec l'id: " + employeId + "n'existe pas!");
         return null;
      }

      employe.setListeDeModules(listeModules);
      return employe;
   }

   /**
    * getProjet retoune toutes les informations sur un projet donné
    *
    * @param projetId Numéro d'un projet
    * @return Retourne un Objet Projet avec toutes les infos sur celui-ci dedans
    */
   public Projet getProjet(int projetId)
   {
      Session session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();

      //recherche si il y a un projet avec ce nom
      String hql = "from Objets.Projet P where P.id =:projetId";
      Projet resultat = (Projet) session.createQuery(hql).setParameter("projetId", projetId).uniqueResult();
      session.getTransaction().commit();

      if (resultat == null)
      {
         Journalisation.getERR().warning("Le projet avec l'id: " + projetId + "n'existe pas!");
         return null;
      }

      return resultat;
   }

   /**
    * getLogTache retourne tous les logs d'une tâche donnée
    *
    * @param tacheId Numéro de la tache
    * @return Retourne une liste de logs
    */
   public List<Log> getLogTache(int tacheId)
   {
      Session session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();

      //recherche si il y a un projet avec ce nom
      String hql = "from Objets.Log L where L.tache.id =:tacheId";
      List<Log> resultat = (List<Log>) session.createQuery(hql).setParameter("tacheId", tacheId).list();
      session.getTransaction().commit();

      //teste si le resultat est vide
      if (resultat.isEmpty())
      {
         Journalisation.getERR().warning("Tache inconnu");
      }
      return resultat;
   }

   /**
    * getLog retourne toutes les infos d'un log donné
    *
    * @param logId Numéro du log
    * @return Retourne un objet Log avec toutes les infos sur celui-ci
    */
   public Log getLog(int logId)
   {
      Session session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();

      //recherche si il y a un projet avec ce nom
      String hql = "from Objets.Log L where L.id =:logId";
      Log resultat = (Log) session.createQuery(hql).setParameter("logId", logId).uniqueResult();
      session.getTransaction().commit();

      if (resultat == null)
      {
         Journalisation.getERR().warning("Le log avec l'id: " + logId + "n'existe pas!");
         return null;
      }

      return resultat;
   }

   /**
    * getTache retourne toutes les infos d'une tâche donnée
    *
    * @param tacheId Numéro de la tâche
    * @return Retourne un objet Tache avec toutes les infos sur celle-ci
    */
   public Tache getTache(int tacheId)
   {
      Session session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();

      //recherche si il y a un projet avec ce nom
      String hql = "from Objets.Tache T where T.id =:tacheId";
      Tache resultat = (Tache) session.createQuery(hql).setParameter("tacheId", tacheId).uniqueResult();
      session.getTransaction().commit();

      if (resultat == null)
      {
         Journalisation.getERR().warning("La tache avec l'id: " + tacheId + "n'existe pas!");
         return null;
      }

      return resultat;
   }

   /**
    * getModule retourne toutes les infos d'un module donné
    *
    * @param moduleId Numéro du module
    * @return Retourne un objet Module avec toutes les infos sur celui-ci
    */
   public Module getModule(int moduleId)
   {
      List<Employe> listeEmployes = new ArrayList<Employe>();
      Session session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();

      //recherche si il y a un Module avec ce nom
      String hql = "from Objets.Module M where M.id =:moduleId";
      Module resultat = (Module) session.createQuery(hql).setParameter("moduleId", moduleId).uniqueResult();
      List<ModuleEffectif> moduleEffectifs = getListeModuleEffectif(moduleId);

      //charge la liste des employés
      for (ModuleEffectif moduleEffectif : moduleEffectifs)
      {
         listeEmployes.add(moduleEffectif.getEmploye());
      }

      resultat.setListeEmployes(listeEmployes);
      session.getTransaction().commit();
      if (resultat == null)
      {
         Journalisation.getERR().warning("Le module avec l'id: " + moduleId + "n'existe pas!");
         return null;
      }

      return resultat;
   }

   /**
    * getEtat retourne toutes les infos d'un etat donné
    *
    * @param etatId Numéro d'un etat
    * @return Retourne un objet Etat avec toutes les infos sur celui-ci
    */
   public Etat getEtat(int etatId)
   {
      Session session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();

      //recherche si il y a un Module avec ce nom
      String hql = "from Objets.Etat E where E.id =:etatId";
      Etat resultat = (Etat) session.createQuery(hql).setParameter("etatId", etatId).uniqueResult();
      session.getTransaction().commit();

      if (resultat == null)
      {
         Journalisation.getERR().warning("L'état avec l'id: " + etatId + "n'existe pas!");
         return null;
      }

      return resultat;
   }

   /**
    * ListeEtats retourne tous les etats disponibles
    *
    * @return Retourne un objet ListeEtat avec toutes les infos sur ceux-ci
    */
   public List<Etat> getListeEtats()
   {
      Session session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();

      //recherche si il y a un Module avec ce nom
      String hql = "from Objets.Etat ";
      List<Etat> resultat = session.createQuery(hql).list();
      session.getTransaction().commit();

      if (resultat == null)
      {
         Journalisation.getERR().warning("Aucun état trouvé!");
         return null;
      }

      return resultat;
   }

   /**
    * modifierEmploye met à jour un employe
    *
    * @param employe Employe à modifier
    * @return Une string pour avertir d'une éventuelle erreur
    */
   public String modifierEmploye(Employe employe)
   {
      //recupère les mdp de l'employé
      Employe tmp = getEmployeAvecMDP(employe.getId());
      employe.setSel(tmp.getSel());
      employe.setMotDePasse(tmp.getMotDePasse());

      // Essai Lara
      List<Module> nouvelleListe = employe.getListeDeModules();
      List<ModuleEffectif> ancienneListe = getListeModuleEmploye(employe.getId());

      for (ModuleEffectif m : ancienneListe)
      {
         nouvelleListe.remove(m.getModule());
      }

      Module nouveauModule = nouvelleListe.get(0);
      creerModuleEffectif(new ModuleEffectif(employe, nouveauModule, new Timestamp(System.currentTimeMillis())));

		/* Ce que Thibaud faisait
      List<Module> listeModulesAAjouter = employe.getListeDeModules();
		List<ModuleEffectif> moduleEffectifs = getListeModuleEmploye(employe.getId());
		List<Module> listeModule = new ArrayList();
		boolean existeDeja;

		//liste les employes à ajouter et a supprimer
		for (ModuleEffectif moduleEffectif : moduleEffectifs)
		{
			existeDeja = false;
			//test si il faut supprimer
			if (!employe.getListeDeModules().contains(moduleEffectif.getModule()))
			{
				suprimerModuleEffectif(moduleEffectif);
			}
			else
			{
				listeModule.add(moduleEffectif.getModule());
			}
		}

		//garde seulement les nouveaux modules
		listeModulesAAjouter.removeAll(listeModule);

		for (Module module : listeModulesAAjouter)
		{
			creerModuleEffectif(new ModuleEffectif(employe, module,new Timestamp(System.currentTimeMillis())));
		}
		*/

      return sauverObjet(employe, CODE_EMPLOYE_EXISTANT);
   }

   /**
    * modifierProjet met à jour un projet
    *
    * @param projet Objet projet à modifier
    * @return Une string pour avertir d'une éventuelle erreur
    */
   public String modifierProjet(Projet projet)
   {
      return sauverObjet(projet, CODE_PROJET);
   }

   /**
    * modifierModule met à jour un module
    *
    * @param module Objet module à modifier
    * @return Une string pour avertir d'une éventuelle erreur
    */
   public String modifierModule(Module module)
   {

      List<Employe> listeEmploye = new ArrayList();
      List<Employe> listeEmployesAAjouter = module.getListeEmployes();
      List<ModuleEffectif> moduleEffectifs = getListeModuleEffectif(module.getId());
      boolean existeDeja;

      //liste les employes à ajouter et a supprimer
      for (ModuleEffectif moduleEffectif : moduleEffectifs)
      {
         existeDeja = false;
         //test si il faut supprimer
         if (!(module.getListeEmployes().contains(moduleEffectif.getEmploye())))
         {
            suprimerModuleEffectif(moduleEffectif);
         }
         else
         {
            listeEmploye.add(moduleEffectif.getEmploye());
         }
      }

      //garde seulement les nouveaux modules
      listeEmployesAAjouter.removeAll(listeEmploye);

      for (Employe employe : listeEmployesAAjouter)
      {
         creerModuleEffectif(new ModuleEffectif(employe, module, new Timestamp(System.currentTimeMillis())));
      }

      return sauverObjet(module, CODE_MODULE);
   }

   /**
    * modifierTache met à jour une tâche
    *
    * @param tache Objet tache à modifier
    * @return Une string pour avertir d'une éventuelle erreur
    */
   public String modifierTache(Tache tache)
   {
      return sauverObjet(tache, CODE_TACHE);
   }

   /**
    * creerToken sauve dans la base de données un objet token
    *
    * @param token Objet token à sauver
    * @return Une string pour avertir d'une éventuelle erreur
    */
   private String creerToken(Token token)
   {
      return sauverObjet(token, CODE_TOKEN);
   }

   /**
    * creerEmploye sauve dans la base de données un objet employe
    *
    * @param employe Objet employe à sauver
    * @return Une string pour avertir d'une éventuelle erreur
    */
   public String creerEmploye(Employe employe)
   {
      return sauverObjet(employe, CODE_EMPLOYE);
   }

   /**
    * creerProjet sauve dans la base de données un objet projet
    *
    * @param projet Objet projet à sauver
    * @return Une string pour avertir d'une éventuelle erreur
    */
   public String creerProjet(Projet projet)
   {
      return sauverObjet(projet, CODE_PROJET);
   }

   /**
    * creerModule sauve dans la base de données un objet module
    *
    * @param module Objet module à sauver
    * @return Une string pour avertir d'une éventuelle erreur
    */
   public String creerModule(Module module)
   {
      return sauverObjet(module, CODE_MODULE);
   }

   /**
    * creerTache sauve dans la base de données un objet tache
    *
    * @param tache Objet tache à sauver
    * @return Une string pour avertir d'une éventuelle erreur
    */
   public String creerTache(Tache tache)
   {
      return sauverObjet(tache, CODE_TACHE);
   }

   /**
    * creerModuleEffectif sauve dans la base de données un objet moduleEffectif
    *
    * @param moduleEffectif Objet moduleEffectif à sauver
    * @return Une string pour avertir d'une éventuelle erreur
    */
   public String creerModuleEffectif(ModuleEffectif moduleEffectif)
   {
      return sauverObjet(moduleEffectif, CODE_MODULE_EFFECTIF);
   }


   /**
    * creerLog sauve dans la base de données un objet log
    *
    * @param log Objet log à sauver
    * @return Une string pour avertir d'une éventuelle erreur
    */
   //TODO a tester en production
   public String creerLog(Log log)
   {
      Session session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();

      //recupère la tache pour ajouter le temps du log au temps passé sur la tâche
      Tache tache = getTache(log.getTache().getId());

      //convertit en localtime pour faciliter les calculs
      LocalTime temp = tache.getDureeEffective().toLocalTime();
      //calcul la différence entre le timestamp du début et de fin
      long duree = log.getFin().getTime() - log.getDebut().getTime();
      //passage de millisecondes en seconde
      duree /= 1000;

      //ajoute le temps à la tache
      temp = temp.plusSeconds(duree);
      tache.setDureeEffective(java.sql.Time.valueOf(temp));
      //met la tâche à jour
      modifierTache(tache);
      session.getTransaction().commit();
      //sauve le log
      return sauverObjet(log, CODE_LOG);
   }


   /**
    * Fonction générique qui sert à ajouter et à modifier des objets dans la bdd. Elle est appelée
    * par toutes les fonctions ajouter et modifier
    *
    * @param objet     Objet à sauver
    * @param typeObjet Code de l'objet à sauver
    * @return Une string pour avertir d'une éventuelle erreur
    */
   private String sauverObjet(Object objet, int typeObjet)
   {
      try
      {
         Session session = HibernateUtil.getSessionFactory().openSession();
         session.beginTransaction();
         switch (typeObjet)
         {
            case CODE_ENTREPRISE:
               session.saveOrUpdate((Entreprise) objet);
               break;
            case CODE_TOKEN:
               session.saveOrUpdate((Token) objet);
               break;
            case CODE_EMPLOYE:
               Employe employe = (Employe) objet;
               employe.setSel(GenerateurSel.genererSel());
               employe.setMotDePasse(GenerateurHash.calculerHash(employe.getSel() + employe.getMotDePasse()));
               session.saveOrUpdate(employe);
               break;
            case CODE_PROJET:
               session.saveOrUpdate((Projet) objet);
               break;
            case CODE_MODULE:
               session.saveOrUpdate((Module) objet);
               break;
            case CODE_TACHE:
               session.saveOrUpdate((Tache) objet);
               break;
            case CODE_LOG:
               session.saveOrUpdate((Log) objet);
               break;
            case CODE_MODULE_EFFECTIF:
               session.saveOrUpdate((ModuleEffectif) objet);
               break;
            case CODE_EMPLOYE_EXISTANT:
               session.saveOrUpdate((Employe) objet);
               break;
         }
         session.getTransaction().commit();
         return ProtocoleWorksOn.RSP_OK;
      } catch (Exception e)
      {
         Journalisation.getERR().severe("Erreur lors de la sauvegarde d'un objet");
         return "Erreur : " + e.getMessage();
      }
   }


   /**
    * suprimerModuleEffectif supprime un employe dans la liste des employes travaillant sur ce module
    *
    * @param moduleEffectif Objet moduleEffectif à supprimer
    * @return Une string pour avertir d'une éventuelle erreur
    */
   public String suprimerModuleEffectif(ModuleEffectif moduleEffectif)
   {
      try
      {
         Session session = HibernateUtil.getSessionFactory().openSession();
         session.beginTransaction();
         session.delete(moduleEffectif);
         session.getTransaction().commit();

         return ProtocoleWorksOn.RSP_OK;
      } catch (Exception e)
      {
         Journalisation.getERR().severe("Erreur lors de la supression d'un ModuleEffectif");
         return "Erreur " + e.getMessage();
      }
   }

   /**
    * controlerLogin vérifie que les informations transmise par l'utilisateur soient correctes afin qu'il puisse
    * utiliser le programme
    *
    * @param nomUtilisateurAVerifier Nom d'utilisateur dans l'application
    * @param password                Mot de passe de l'utilisateur
    * @return un objet reponseConnexion si tout s'est bien passé , sinon renvoie null
    */
   public ReponseConnexion controlerLogin(String nomUtilisateurAVerifier, String password)
   {
      Session session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();

      //requete pour récupérer les infos de l'utilisateur
      String hql = "from Objets.Employe E where E.nomUtilisateur =:nomUtilisateur";
      Employe employeTmp = (Employe) session.createQuery(hql).setParameter("nomUtilisateur", nomUtilisateurAVerifier).uniqueResult();
      session.getTransaction().commit();

      //On prends le seul element de la liste resultat car le nomUtilisateur est unique donc 1 seul element existe
      if (employeTmp == null)
      {
         Journalisation.getERR().warning("Le nom d'utilisateur \"" + nomUtilisateurAVerifier + "\" est introuvable");
         return null;
      }

      // ensuite, il faut qu'on le calcule en local. Du coup je prend le sel dans "hashSel", je l'append au début du mdp
      // "password" et ensuite je calcule le HashSel. finalement on compare ce hash avec le hash contenu dans la bdd
      String hashClient = GenerateurHash.calculerHash(employeTmp.getSel() + password);
      if (hashClient.equals(employeTmp.getMotDePasse()))
      {
         Token nouveauToken = GenerateurToken.genererToken(employeTmp);
         creerToken(nouveauToken);
         ReponseConnexion reponseConnexion = new ReponseConnexion(nouveauToken, employeTmp);

         return reponseConnexion;
      }
      else
      {
         //si erreur retourne null
         Journalisation.getERR().warning("Le mot de passe est incorrect");
         return null;
      }
   }

   /**
    * controlerToken controle que l'utilisateur possède bien un token valide
    *
    * @param token Token de l'utilisateur
    * @return un objet reponseConnexion si tous est bien passé , sinon revoie null
    */
   public ReponseConnexion controlerToken(Token token)
   {
      Session session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();

      //requete pour récupérer les infos du token
      String hql = "from Objets.Token T where T.token =:token";
      Token resultat = (Token) session.createQuery(hql).setParameter("token", token.getToken()).uniqueResult();
      session.getTransaction().commit();

      //On prend le seul element de la liste resultat car un token est unique donc 1 seul element existe
      if (resultat == null)
      {
         Journalisation.getERR().warning("Token inconnu");
         return null;
      }
      else
      {
         return new ReponseConnexion(token, token.getEmploye());
      }
   }
}
