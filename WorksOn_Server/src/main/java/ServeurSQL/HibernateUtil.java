package ServeurSQL;

import Utilitaires.Journalisation;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil
{
   public static final SessionFactory sessionFactory;

   static
   {
      try
      {
         // Création de la SessionFactory à partir du fichier de configuration hibernate.cfg.xml
         sessionFactory = new Configuration().configure().buildSessionFactory();
      } catch (Throwable ex)
      {
         // Make sure you log the exception, as it might be swallowed
         Journalisation.getERR().severe("Initial SessionFactory creation failed." + ex);
         throw new ExceptionInInitializerError(ex);
      }
   }

   public static final ThreadLocal session = new ThreadLocal();

   //retourne la session de connexion avec la base donnée
   public static SessionFactory getSessionFactory()
   {
      return sessionFactory;
   }
}
