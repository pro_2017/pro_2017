package Utilitaires;

import org.apache.commons.codec.binary.Base64;

import java.security.SecureRandom;

/**
 * Classe permettant la génération d'un sel unique utilisé pour les nouveaux mots de passe
 */
public class GenerateurSel
{
   private static SecureRandom randomGenerator = new SecureRandom();

   /**
    * Méthode permettant de générer un Sel aléatoire de manière unique
    *
    * @return le sel généré
    */
   public static String genererSel()
   {
      byte[] salt = new byte[6];
      randomGenerator.nextBytes(salt);

      return Base64.encodeBase64String(salt);
   }
}
