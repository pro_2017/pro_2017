package Utilitaires;

import java.security.MessageDigest;

/**
 * Classe permettant d'effectuer les opérations cryptographiques de hachage avec SHA-256
 */
public class GenerateurHash
{
   private static MessageDigest md;

   /**
    * Méthode statique permettant de calculer le hash d'une chaine de caractères
    *
    * @param input la chaîne dont on veut calculer le hash
    * @return le hash de la chaîne reçue en entrée calculé avec SHA-256
    */
   public static String calculerHash(String input)
   {
      // Sanity check -- Si on ne reçoit pas une chaine ou une chaîne vide, retourner une chaine vide
      if (input == null || input.equals(""))
      {
         return "";
      }

      try
      {
         md = MessageDigest.getInstance("SHA-256");

         // Utiliser UTF-8 pour la chaîne en entrée
         md.update(input.getBytes("UTF-8"));

      } catch (Exception e)
      {
         e.printStackTrace();
      }

      byte[] hash = md.digest();

      StringBuffer hexString = new StringBuffer();

      for (int i = 0; i < hash.length; i++)
      {
         String hex = Integer.toHexString(0xFF & hash[i]);
         if (hex.length() == 1)
         {
            hexString.append('0');
         }

         hexString.append(hex);
      }

      // Retourner le hash
      return hexString.toString();
   }
}
