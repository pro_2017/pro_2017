package Utilitaires;

import Objets.Employe;
import Objets.Token;
import org.apache.commons.codec.binary.Base64;

import java.security.SecureRandom;
import java.sql.Timestamp;

/**
 * Classe permettant de générer un token de connexion.
 */
public class GenerateurToken
{
   private static SecureRandom randomGenerator = new SecureRandom();

   /**
    * Méthode permettant de générer un Token de connexion
    *
    * @param employe employé possédant le token
    * @return le token généré
    */
   public static Token genererToken(Employe employe)
   {
      byte[] token = new byte[20];
      randomGenerator.nextBytes(token);

      return new Token(Base64.encodeBase64String(token), new Timestamp(System.currentTimeMillis()), employe);
   }


   /**
    * Méthode permettant de générer un Token de connexion
    *
    * @return le token généré
    */
   public static Token genererToken()
   {
      byte[] token = new byte[20];
      randomGenerator.nextBytes(token);

      return new Token(Base64.encodeBase64String(token), new Timestamp(System.currentTimeMillis()));
   }
}
