package Utilitaires;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe permettant de logger les erreurs qui arrivent dans le serveur afin de pouvoir vérifier / débugger les problèmes
 * Pour utiliser le logger, faire appel a une des fonctions suivantes :
 * Journalisation.getLOG.severe("string");
 * Journalisation.getLOG.warning("string");
 * Journalisation.getLOG.fine("string");
 * Journalisation.getLOG.finer("string");
 * Journalisation.getLOG.finest("string");
 */
public class Journalisation
{
   // Le journal de log pour les erreurs
   final private static java.util.logging.Logger ERR = java.util.logging.Logger.getLogger("Erreurs Serveur");

   // Le journal de log pour les connexions au serveur et les démarrages du serveur
   final private static java.util.logging.Logger LOG = java.util.logging.Logger.getLogger("Connexions Serveur");

   static
   {
      try
      {
         // Indique quels niveaux on veut log
         LOG.setLevel(Level.FINEST);
         ERR.setLevel(Level.WARNING);

         // Indique dans quel fichier inscrire les entrées
         LOG.addHandler(new FileHandler("connexions.log", true));
         ERR.addHandler(new FileHandler("erreurs.log", true));
      } catch (IOException e)
      {
         e.printStackTrace();
      }
   }

   public static Logger getLOG()
   {
      return LOG;
   }

   public static Logger getERR()
   {
      return ERR;
   }
}
