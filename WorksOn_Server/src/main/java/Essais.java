import Utilitaires.GenerateurHash;
import Utilitaires.GenerateurSel;
import Utilitaires.GenerateurToken;
import Utilitaires.Journalisation;

/**
 * Created by matthieu on 3/25/17.
 */
public class Essais
{

   public static void main(String[] argv)
   {
      for (int i = 0; i < 9; i++)
      {
         System.out.println(GenerateurSel.genererSel());

      }
   }
}

// Sel + mdp / hash
// HXQrrl0V + password = 5c5420982436d4723449b85394e09dc0a07c508b4b26607285802c658c0916a1
// 2Ui6b/aZ + jaimelesolives = 75304ed277c1a9c53ddb81611c3ebbd41c95c550ce6819b34b81e1311020d9cf
// JWYwfLwZ + supermdpdefou = 5e83218b89963dfb3ac50d869bf64eaac4a5864c9ccd4fd01835e24dbbf2ac07
// Np9akx+8 + jaimelespommes = 2ab2dcbec253110e4345fb7adb11e8b2b886917ef398f28551fb7d676c498a55
// 3JZaPd70 + jaimemoniphone = f3e0bf02bb690d3159779604508e89b7390ecefb786769827bd3498214624d73