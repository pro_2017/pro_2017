package Protocoles;

/**
 * Protocole de communication utilisé entre le serveur et le client.
 */
public class ProtocoleWorksOn
{
   public final static String VERSION = "1.0";
   public final static int DEFAULT_PORT = 4242;

   public final static int CMD_GET_LISTE_PROJETS = 1;
   public final static int CMD_GET_LISTE_PROJET_UTILISATEUR = 2;
   public final static int CMD_GET_LISTE_PROJET_CHEF = 3;
   public final static int CMD_GET_LISTE_MODULES = 4;
   public final static int CMD_GET_LISTE_TACHES_MODULE = 5;
   public final static int CMD_GET_LISTE_TACHES = 6;
   public final static int CMD_GET_LISTE_UTILISATEURS_MODULE = 7;
   public final static int CMD_GET_LISTE_UTILISATEURS = 9;
   public final static int CMD_GET_PROJET = 10;
   public final static int CMD_GET_MODULE = 11;
   public final static int CMD_CREER_UTILISATEUR = 12;
   public final static int CMD_CREER_PROJET = 13;
   public final static int CMD_CREER_MODULE = 14;
   public final static int CMD_CREER_TACHE = 15;
   public final static int CMD_CREER_LOG = 16;
   public final static int CMD_MODIFIER_PROJET = 17;
   public final static int CMD_MODIFIER_MODULE = 18;
   public final static int CMD_MODIFIER_TACHE = 19;
   public final static int CMD_GET_LOGS_TACHE = 21;
   public final static int CMD_FERMER_SOCKET = 22;
   public final static int CMD_GET_EMPLOYE = 23;
   public final static int CMD_GET_TACHE = 24;
   public final static int CMD_GET_LOG = 25;
   public final static int CMD_GET_ETAT = 26;
   public final static int CMD_GET_LISTE_ETATS = 27;
   public final static int CMD_MODIFIER_EMPLOYE = 28;

   public final static String RSP_OK = "OK";
}