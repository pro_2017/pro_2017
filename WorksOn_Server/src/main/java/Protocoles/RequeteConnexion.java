package Protocoles;

import Objets.Token;

/**
 * Objet utilisé pour toute demande d'authentification d'un client.
 *
 * Deux cas possibles : 1) Le client fourni un couple mdp + login et laisse token vide
 *                      2) Le client fourni un token (qu'il avait reçu) puis laisse vide login + mdp
 */
public class RequeteConnexion
{
   private String username;
   private String password;
   private Token token;

   /**
    * Constructeur utilisé dans le cas d'une demande de connexion avec le couple login + mdp
    */
   public RequeteConnexion(String username, String password)
   {
      this.username = username;
      this.password = password;
   }

   /**
    * Constructeur utilisé dans le cas d'une demande de connexion avec le token
    */
   public RequeteConnexion(Token token)
   {
      this.token = token;
   }

   // GETTERS & SETTERS

   public String getUsername()
   {
      return username;
   }

   public String getPassword()
   {
      return password;
   }

   public Token getToken()
   {
      return token;
   }

   public void setUsername(String username)
   {
      this.username = username;
   }

   public void setPassword(String password)
   {
      this.password = password;
   }

   public void setToken(Token token)
   {
      this.token = token;
   }
}
