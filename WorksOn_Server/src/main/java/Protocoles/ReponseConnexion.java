package Protocoles;

import Objets.Employe;
import Objets.Token;

/**
 * Classe représentant une réponse à une demande de connexion.
 * En cas de demande d'authentification via token, retourner le token dans la réponse si il est valide
 * En cas de demande d'authentification via mdp + login, retourner un nouveau token et l'id de l'entreprise dans la réponse.
 */
public class ReponseConnexion
{
   private Token token;
   private Employe employe;

   /**
    * Constructeur utilisé pour répondre à une demande de connexion via Token
    */
   public ReponseConnexion(Token token)
   {
      this.token = token;
      employe = null;
   }

   /**
    * Constructeur utilisé pour répondre à une demande de connexion via mdp + login
    */
   public ReponseConnexion(Token token, Employe employe)
   {
      this.token = token;
      this.employe = employe;
   }

   // GETTERS & SETTERS

   public Token getToken()
   {
      return token;
   }

   public void setToken(Token token)
   {
      this.token = token;
   }

   public Employe getEmploye()
   {
      return employe;
   }

   public void setEmploye(Employe employe)
   {
      this.employe = employe;
   }
}