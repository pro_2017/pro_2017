package Protocoles;

/**
 * Un objet représentant les requetes possibles vers le serveur depuis le client. Utilisé pour toJSON et fromJSON.
 * Le type de requête correspond à une des entrées établies dans le protocole.
 */
public class RequeteServeur
{
   private int typeRequete;            // Le type de requête à traiter (selon Protocoles.ProtocoleWorksOn)
   private int idEntreprise;           // L'id de l'entreprise qui fait les requetes
   private String[] arguments;         // Les éventuels arguments

   /**
    * Constructeur élaboré pour la création d'une requête au serveur.
    *
    * @param typeRequete  Le type de requête selon le protocole établis (Protocole.ProtocoleWorksOn)
    * @param idEntreprise L'id de l'entreprise concernée
    * @param arguments    Les différents arguments envoyés
    */
   public RequeteServeur(int typeRequete, int idEntreprise, String... arguments)
   {
      this.typeRequete = typeRequete;
      this.idEntreprise = idEntreprise;
      this.arguments = arguments;
   }

   // GETTERS & SETTERS

   public int getTypeRequete()
   {
      return typeRequete;
   }

   public int getIdEntreprise()
   {
      return idEntreprise;
   }

   public String[] getArguments()
   {
      return arguments;
   }

   public void setTypeRequete(int typeRequete)
   {
      this.typeRequete = typeRequete;
   }

   public void setIdEntreprise(int idEntreprise)
   {
      this.idEntreprise = idEntreprise;
   }

   public void setArguments(String[] arguments)
   {
      this.arguments = arguments;
   }
}
