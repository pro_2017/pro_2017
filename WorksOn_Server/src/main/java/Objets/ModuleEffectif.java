package Objets;

import java.sql.Timestamp;

/**
 * Classe représentant un module effectif
 * Cette classe est utilisée pour la relation many to many dans Hibernate. Cette classe n'est donc pas présente dans
 * le client pour lequel toutes ces manipulations sont transparentes.
 */
public class ModuleEffectif
{
   private int id;
   private Employe employe;
   private Module module;
   private Timestamp dateAjout;

   /**
    * Constructeur par défaut
    */
   public ModuleEffectif()
   {
   }

   /**
    * Constructeur utilisé par Hibernate (entier).
    *
    * @param id        L'id du module récupéré
    * @param employe   L'employé de la relation
    * @param module    Le module de la relation
    * @param dateAjout La date d'ajout de la relation
    */
   public ModuleEffectif(int id, Employe employe, Module module, Timestamp dateAjout)
   {
      this.id = id;
      this.employe = employe;
      this.module = module;
      this.dateAjout = dateAjout;
   }

   /**
    * Constructeur utilisé par Hibernate dans certains cas de figure.
    *
    * @param employe   L'employé de la relation
    * @param module    Le module de la relation
    * @param dateAjout La date d'ajout de la relation
    */
   public ModuleEffectif(Employe employe, Module module, Timestamp dateAjout)
   {
      this.employe = employe;
      this.module = module;
      this.dateAjout = dateAjout;
   }

   /**
    * Constructeur de copie utilisé par Hibernate
    *
    * @param moduleEffectif Le module à copier
    */
   public ModuleEffectif(ModuleEffectif moduleEffectif)
   {
      this.id = moduleEffectif.getId();
      this.employe = moduleEffectif.getEmploye();
      this.module = moduleEffectif.getModule();
      this.dateAjout = moduleEffectif.getDateAjout();
   }

   // GETTERS & SETTERS

   public int getId()
   {
      return id;
   }

   public void setId(int id)
   {
      this.id = id;
   }

   public Employe getEmploye()
   {
      return employe;
   }

   public void setEmploye(Employe employe)
   {
      this.employe = employe;
   }

   public Module getModule()
   {
      return module;
   }

   public void setModule(Module module)
   {
      this.module = module;
   }

   public Timestamp getDateAjout()
   {
      return dateAjout;
   }

   public void setDateAjout(Timestamp dateAjout)
   {
      this.dateAjout = dateAjout;
   }
}
