package Objets;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe représentant une liste de projets.
 * Sa seule utilité est de "wrapper" une ArrayList pour faciliter la sérialisation.
 */
public class ListeProjets
{
   private List<Projet> listeProjets;

   /**
    * Constructeur par défaut
    */
   public ListeProjets()
   {
      listeProjets = new ArrayList<Projet>();
   }

   /**
    * Constructeur de base
    *
    * @param liste L'arrayList de projets à "wrapper"
    */
   public ListeProjets(List<Projet> liste)
   {
      this();
      listeProjets.addAll(liste);
   }

   /**
    * Méthode permettant de récupérer la liste sous forme normale
    *
    * @return L'arrayList de projets
    */
   public List<Projet> getListeProjets()
   {
      return listeProjets;
   }
}
