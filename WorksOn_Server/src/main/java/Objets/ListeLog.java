package Objets;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe représentant une liste de logs.
 * Sa seule utilité est de "wrapper" une ArrayList pour faciliter la sérialisation.
 */
public class ListeLog
{
   private List<Log> listeLog;

   /**
    * Constructeur par défaut
    */
   public ListeLog()
   {
      listeLog = new ArrayList<Log>();
   }

   /**
    * Constructeur de base
    *
    * @param liste L'arrayList de logs à "wrapper"
    */
   public ListeLog(List<Log> liste)
   {
      this();
      listeLog.addAll(liste);
   }

   /**
    * Méthode permettant de récupérer la liste sous forme normale
    *
    * @return L'arrayList de logs
    */
   public List<Log> getListeLog()
   {
      return listeLog;
   }
}
