package Objets;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe représentant une liste d'utilisateurs.
 * Sa seule utilité est de "wrapper" une ArrayList pour faciliter la sérialisation.
 */
public class ListeEmployes
{
   private List<Employe> listeEmployes;

   /**
    * Constructeur par défaut
    */
   public ListeEmployes()
   {
      listeEmployes = new ArrayList<Employe>();
   }

   /**
    * Constructeur de base
    *
    * @param liste L'arrayList d'employés à "wrapper"
    */
   public ListeEmployes(List<Employe> liste)
   {
      this();
      listeEmployes.addAll(liste);
   }

   /**
    * Méthode permettant de récupérer la liste sous forme normale
    *
    * @return L'arrayList d'employés
    */
   public List<Employe> getListeEmployes()
   {
      return listeEmployes;
   }
}
