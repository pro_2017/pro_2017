package Objets;

import java.sql.Timestamp;

/**
 * Classe représentant une entrée de log
 */
public class Log
{
   private int id;
   private String remarques;
   private Timestamp debut;
   private Timestamp fin;
   private Tache tache;

   /**
    * Constructeur par défaut
    */
   public Log()
   {
   }

   /**
    * Constructeur utilisé par Hibernate (entier).
    *
    * @param id        L'id du log récupéré
    * @param remarques Les remarques du log récupéré
    * @param debut     La date de début du log récupéré
    * @param fin       La date de fin du log récupéré
    * @param tache     La tache qui contient le log récupéré
    */
   public Log(int id, String remarques, Timestamp debut, Timestamp fin, Tache tache)
   {
      this(remarques, debut, fin, tache);
      this.id = id;
   }

   /**
    * Constructeur utilisé lors de la création d'un log par un client.
    *
    * @param remarques Les remarques du nouveau log
    * @param debut     La date de début du nouveau log
    * @param fin       La date de fin du nouveau log
    * @param tache     La tâche qui contient le nouveau log
    */
   public Log(String remarques, Timestamp debut, Timestamp fin, Tache tache)
   {
      this.remarques = remarques;
      this.debut = debut;
      this.fin = fin;
      this.tache = tache;
   }

   // GETTERS & SETTERS

   public int getId()
   {
      return id;
   }

   public void setId(int id)
   {
      this.id = id;
   }

   public String getRemarques()
   {
      return remarques;
   }

   public void setRemarques(String remarque)
   {
      this.remarques = remarque;
   }

   public Timestamp getDebut()
   {
      return debut;
   }

   public void setDebut(Timestamp debut)
   {
      this.debut = debut;
   }

   public Timestamp getFin()
   {
      return fin;
   }

   public void setFin(Timestamp fin)
   {
      this.fin = fin;
   }

   public Tache getTache()
   {
      return tache;
   }

   public void setTache(Tache tache)
   {
      this.tache = tache;
   }
}
