package Objets;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe représentant une liste de tâches.
 * Sa seule utilité est de "wrapper" une ArrayList pour faciliter la sérialisation.
 */
public class ListeTaches
{
   private List<Tache> listeTaches;

   /**
    * Constructeur par défaut
    */
   public ListeTaches()
   {
      listeTaches = new ArrayList<Tache>();
   }

   /**
    * Constructeur de base
    *
    * @param liste L'arrayList de tâches à "wrapper"
    */
   public ListeTaches(List<Tache> liste)
   {
      this();
      listeTaches.addAll(liste);
   }

   /**
    * Méthode permettant de récupérer la liste sous forme normale
    *
    * @return L'arrayList de tâches
    */
   public List<Tache> getListeTaches()
   {
      return listeTaches;
   }
}
