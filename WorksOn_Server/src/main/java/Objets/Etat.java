package Objets;

/**
 * Classe représentant un état (d'un projet, d'un module ou d'une tâche)
 */
public class Etat
{
   private int id;
   private String nom;

   /**
    * Constructeur par défaut
    */
   public Etat()
   {
   }

   /**
    * Constructeur utilisé par Hibernate (entier).
    *
    * @param id  L'id de l'état récupéré
    * @param nom Le nom de l'état récupéré
    */
   public Etat(int id, String nom)
   {
      this.id = id;
      this.nom = nom;
   }

   /**
    * Constructeur utilisé par Hibernate dans certains cas.
    *
    * @param nom Le nom de l'état
    */
   public Etat(String nom)
   {
      this.nom = nom;
   }

   // GETTERS & SETTERS

   public int getId()
   {
      return id;
   }

   public void setId(int id)
   {
      this.id = id;
   }

   public String getNom()
   {
      return nom;
   }

   public void setNom(String nom)
   {
      this.nom = nom;
   }

   @Override
   public String toString()
   {
      return nom;
   }
}
