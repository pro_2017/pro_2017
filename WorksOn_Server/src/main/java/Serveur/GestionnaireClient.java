package Serveur;

import Enums.Role;
import Objets.*;
import Protocoles.ProtocoleWorksOn;
import Protocoles.ReponseConnexion;
import Protocoles.RequeteConnexion;
import Protocoles.RequeteServeur;
import ServeurSQL.ServicesSQL;
import Utilitaires.Journalisation;
import com.google.gson.Gson;

import java.io.*;

/**
 * Classe permettant la gestion d'un client. Dans cette classe, nous avons accès aux différents
 * flux d'entrée et de sortie vers le client ainsi qu'un objet GSON afin de sérialiser et désérialiser les
 * requêtes et les réponses.
 * Les requêtes possibles depuis un client sont décrites dans le protocole (Protocoles.ProtocoleWorksOn)
 */
public class GestionnaireClient
{
   private BufferedReader reader;
   private PrintWriter writer;
   private Gson gson;
   private int idEntreprise;
   private Role role;

   /**
    * Constructeur de la classe permettant la création des flux d'entrée et de sortie
    *
    * @param is le InputStream du client
    * @param os le OutputStream du client
    */
   public GestionnaireClient(InputStream is, OutputStream os)
   {
      // Création des canneaux de communication
      writer = new PrintWriter(new OutputStreamWriter(os), true);
      reader = new BufferedReader(new InputStreamReader(is));

      // Création d'un nouveau moteur GSON
      gson = new Gson();
   }

   /**
    * Fonction qui permet de traiter le client en répondant à ses requêtes. Premièrement, le client
    * doit passer une phase d'authentification avant de pouvoir faire des requêtes. Une fois cette
    * phase passée, il lui est possible d'effectuer plus d'une requête. La connexion au serveur
    * se termine si le client demande avec la commande CMD_FERMER_SOCKET ou si le client tente de
    * forger des demandes non authorisées avec son niveau d'accréditation. Pour chaque requête du client, on s'assure
    * qu'il ne forge pas l'id de son entreprise ou qu'il ne tente pas d'effectuer une action que son rôle ne permet pas.
    *
    * Si il tente de falsifier son id d'entreprise, il est déconnecté. Si en revanche il contrefait une requête non permise
    * par son rôle, rien ne lui est renvoyé et il se retrouvera bloqué sur un "reader.readLine()" côté client.
    * Ce cas de figure n'est pas censé arriver lors d'une utilisation normale du programme, uniquement en cas de tentative
    * de modification non authorisée.
    *
    * @param services une instance du gestionnaire de services SQL
    * @throws IOException
    */
   public void traiterRequetes(ServicesSQL services) throws IOException
   {
      boolean running = true;             // Tant que le client ne termine pas la connexion ou qu'une erreur n'est détectée
      String commande;                    // La commande reçue du client
      ReponseConnexion reponseConnexion;  // La réponse envoyée après la demande de connexion
      String reponseBDD;                  // La réponse de la BDD lors de certaines actions

      // Traitement de l'authentification (récupération de la requête de connexion)
      RequeteConnexion requeteConnexion = gson.fromJson(reader.readLine(), RequeteConnexion.class);

      // Si la requete ne contient pas de Token, on a un nouveau client pour la journée. Controler le couple username+mdp
      if (requeteConnexion.getToken() == null)
      {
         // On contrôle le mdp du client avec son identifiant.
         reponseConnexion = services.controlerLogin(requeteConnexion.getUsername(), requeteConnexion.getPassword());

         // Si le contrôle échoue
         if (reponseConnexion == null)
         {
            // Préparer une réponse avec comme token la valeur "invalide"
            reponseConnexion = new ReponseConnexion(new Token());
            running = false;
            Journalisation.getLOG().severe("Problème d'authentification : Identifiant / mot de passe invalide");
         }
      }
      else // On reçoit un token, on vérifie qu'il soit valide
      {
         reponseConnexion = services.controlerToken(requeteConnexion.getToken());

         // Si le token est incorrecte, on retourne une nouvelle requete avec un token "invalide"
         if (reponseConnexion == null)
         {
            reponseConnexion = new ReponseConnexion(new Token());
            running = false;
            Journalisation.getLOG().severe("Problème d'authentification : Token invalide");
         }
      }

      // Si le client est authentifié, garder certaines infos a des fins de sécurité
      if (running)
      {
         idEntreprise = reponseConnexion.getEmploye().getEntreprise().getIdEntreprise();
         role = reponseConnexion.getEmploye().getRole();
      }

      // Envoyer la réponse à la demande de connexion au client
      writer.println(gson.toJson(reponseConnexion));

      // Ensuite, traiter ses requêtes tant qu'il ne demande pas l'interruption de la communication (si authentifié)
      while (running && (commande = reader.readLine()) != null)
      {
         // Convertir la requete en un objet RequeteServeur
         RequeteServeur requete = gson.fromJson(commande, RequeteServeur.class);

         // Si le client tente une falsification des données, le déconnecter et garder une trace de sa tentative
         if (requete.getIdEntreprise() != idEntreprise)
         {
            Journalisation.getERR().severe("Tentative de falsification des données");
            break;
         }

         // Effectuer le traitement nécessaire selon le type de requête reçue
         switch (requete.getTypeRequete())
         {
            case ProtocoleWorksOn.CMD_GET_LISTE_PROJETS:
               if (role == Role.ADMINISTRATEUR)
               {
                  ListeProjets lp = new ListeProjets(services.getListeProjets(requete.getIdEntreprise()));
                  writer.println(gson.toJson(lp));
               }
               break;

            case ProtocoleWorksOn.CMD_GET_LISTE_PROJET_UTILISATEUR:
               if (role == Role.CHEF_PROJET || role == Role.UTILISATEUR)
               {
                  ListeProjets lp1 = new ListeProjets(services.getListeProjetsEmploye(Integer.parseInt(requete.getArguments()[0])));
                  writer.println(gson.toJson(lp1));
               }
               break;

            case ProtocoleWorksOn.CMD_GET_LISTE_PROJET_CHEF:
               if (role == Role.CHEF_PROJET)
               {
                  ListeProjets lp2 = new ListeProjets(services.getListeProjetsChef(Integer.parseInt(requete.getArguments()[0])));
                  writer.println(gson.toJson(lp2));
               }
               break;

            case ProtocoleWorksOn.CMD_GET_LISTE_MODULES:
               if (role == Role.CHEF_PROJET || role == Role.UTILISATEUR)
               {
                  ListeModules lm = new ListeModules(services.getListeModules(Integer.parseInt(requete.getArguments()[0])));
                  writer.println(gson.toJson(lm));
               }
               break;

            case ProtocoleWorksOn.CMD_GET_LISTE_TACHES_MODULE:
               if (role == Role.CHEF_PROJET || role == Role.UTILISATEUR)
               {
                  ListeTaches lt = new ListeTaches(services.getListeTachesEmployeModule(Integer.parseInt(requete.getArguments()[0]), Integer.parseInt(requete.getArguments()[1])));
                  writer.println(gson.toJson(lt));
               }
               break;

            case ProtocoleWorksOn.CMD_GET_LISTE_TACHES:
               if (role == Role.CHEF_PROJET)
               {
                  ListeTaches lt1 = new ListeTaches(services.getListeTachesModules(Integer.parseInt(requete.getArguments()[0])));
                  writer.println(gson.toJson(lt1));
               }
               break;

            case ProtocoleWorksOn.CMD_GET_LISTE_UTILISATEURS_MODULE:
               if (role == Role.CHEF_PROJET || role == Role.UTILISATEUR)
               {
                  ListeEmployes le = new ListeEmployes(services.getListeEmployesModule(Integer.parseInt(requete.getArguments()[0])));
                  writer.println(gson.toJson(le));
               }
               break;

            case ProtocoleWorksOn.CMD_GET_LISTE_UTILISATEURS:
               if (role == Role.ADMINISTRATEUR || role == Role.CHEF_PROJET)
               {
                  ListeEmployes le1 = new ListeEmployes(services.getListeEmployes(requete.getIdEntreprise()));
                  writer.println(gson.toJson(le1));
               }
               break;

            case ProtocoleWorksOn.CMD_GET_PROJET:
               if (role == Role.CHEF_PROJET || role == Role.UTILISATEUR)
               {
                  writer.println(gson.toJson(services.getProjet(Integer.parseInt(requete.getArguments()[0]))));
               }
               break;

            case ProtocoleWorksOn.CMD_GET_MODULE:
               if (role == Role.CHEF_PROJET || role == Role.UTILISATEUR)
               {
                  writer.println(gson.toJson(services.getModule(Integer.parseInt(requete.getArguments()[0]))));
               }
               break;

            case ProtocoleWorksOn.CMD_CREER_UTILISATEUR:
               if (role == Role.ADMINISTRATEUR)
               {
                  reponseBDD = services.creerEmploye(gson.fromJson(requete.getArguments()[0], Employe.class));
                  writer.println(reponseBDD);
               }
               break;

            case ProtocoleWorksOn.CMD_CREER_PROJET:
               if (role == Role.CHEF_PROJET)
               {
                  reponseBDD = services.creerProjet(gson.fromJson(requete.getArguments()[0], Projet.class));
                  writer.println(reponseBDD);
               }
               break;

            case ProtocoleWorksOn.CMD_CREER_MODULE:
               if (role == Role.CHEF_PROJET)
               {
                  reponseBDD = services.creerModule(gson.fromJson(requete.getArguments()[0], Module.class));
                  writer.println(reponseBDD);
               }
               break;

            case ProtocoleWorksOn.CMD_CREER_TACHE:
               if (role == Role.CHEF_PROJET || role == Role.UTILISATEUR)
               {
                  reponseBDD = services.creerTache(gson.fromJson(requete.getArguments()[0], Tache.class));
                  writer.println(reponseBDD);
               }
               break;

            case ProtocoleWorksOn.CMD_CREER_LOG:
               if (role == Role.CHEF_PROJET || role == Role.UTILISATEUR)
               {
                  reponseBDD = services.creerLog(gson.fromJson(requete.getArguments()[0], Log.class));
                  writer.println(reponseBDD);
               }
               break;

            case ProtocoleWorksOn.CMD_MODIFIER_EMPLOYE:
               if (role == Role.UTILISATEUR || role == Role.CHEF_PROJET)
               {
                  reponseBDD = services.modifierEmploye(gson.fromJson(requete.getArguments()[0], Employe.class));
                  writer.println(reponseBDD);
               }
               break;

            case ProtocoleWorksOn.CMD_MODIFIER_PROJET:
               if (role == Role.CHEF_PROJET)
               {
                  reponseBDD = services.modifierProjet(gson.fromJson(requete.getArguments()[0], Projet.class));
                  writer.println(reponseBDD);
               }
               break;

            case ProtocoleWorksOn.CMD_MODIFIER_MODULE:
               if (role == Role.CHEF_PROJET)
               {
                  reponseBDD = services.modifierModule(gson.fromJson(requete.getArguments()[0], Module.class));
                  writer.println(reponseBDD);
               }
               break;

            case ProtocoleWorksOn.CMD_MODIFIER_TACHE:
               if (role == Role.CHEF_PROJET || role == Role.UTILISATEUR)
               {
                  reponseBDD = services.modifierTache(gson.fromJson(requete.getArguments()[0], Tache.class));
                  writer.println(reponseBDD);
               }
               break;

            case ProtocoleWorksOn.CMD_GET_LOGS_TACHE:
               if (role == Role.CHEF_PROJET || role == Role.UTILISATEUR)
               {
                  ListeLog liste = new ListeLog(services.getLogTache(Integer.parseInt(requete.getArguments()[0])));
                  writer.println(gson.toJson(liste));
               }
               break;

            case ProtocoleWorksOn.CMD_GET_EMPLOYE:
               if (role == Role.CHEF_PROJET || role == Role.UTILISATEUR)
               {
                  writer.println(gson.toJson(services.getEmploye(Integer.parseInt(requete.getArguments()[0]))));
               }
               break;

            case ProtocoleWorksOn.CMD_GET_LOG:
               if (role == Role.CHEF_PROJET || role == Role.UTILISATEUR)
               {
                  writer.println(gson.toJson(services.getLog(Integer.parseInt(requete.getArguments()[0]))));
               }
               break;

            case ProtocoleWorksOn.CMD_GET_TACHE:
               if (role == Role.CHEF_PROJET || role == Role.UTILISATEUR)
               {
                  writer.println(gson.toJson(services.getTache(Integer.parseInt(requete.getArguments()[0]))));
               }
               break;

            case ProtocoleWorksOn.CMD_GET_LISTE_ETATS:
               if (role == Role.CHEF_PROJET || role == Role.UTILISATEUR)
               {
                  ListeEtats let = new ListeEtats(services.getListeEtats());
                  writer.println(gson.toJson(let));
               }
               break;

            case ProtocoleWorksOn.CMD_GET_ETAT:
               if (role == Role.CHEF_PROJET || role == Role.CHEF_PROJET)
               {
                  writer.println(gson.toJson(services.getEtat(Integer.parseInt(requete.getArguments()[0]))));
               }

            case ProtocoleWorksOn.CMD_FERMER_SOCKET:
               running = false;
               break;
         }
      }
   }
}
