package Serveur;

import ServeurSQL.ServicesSQL;
import Utilitaires.Journalisation;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * Classe permettant la gestion du socket associé au client tout au long de son existance
 */
public class GestionnaireSocket implements Runnable
{
   private Socket socket;                             // Le socket du client
   private GestionnaireClient gestionnaireClient;
   private InputStream is;
   private OutputStream os;
   private Serveur serveur;                           // Le serveur de WorksOn

   /**
    * Constructeur de la classe
    *
    * @param serveur le serveur qui a lancé ce gestionnaire de sockets (dans lequel le pool de clients est disponible)
    * @param socket  le socket du client
    */
   public GestionnaireSocket(Serveur serveur, Socket socket)
   {
      this.socket = socket;
      this.serveur = serveur;

      try
      {
         // Récupération des streams d'entrée et de sortie
         is = socket.getInputStream();
         os = socket.getOutputStream();
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Erreur lors de la création du gestionnaire de socket : " + e.getMessage());
      }

      // Création du GestionnaireClient
      gestionnaireClient = new GestionnaireClient(is, os);
   }

   /**
    * Méthode lancée au lancement du thread client. Traite les requêtes du client. Quand ce dernier
    * à terminé, on ferme les flux I/O et le socket
    */
   public void run()
   {
      try
      {
         // Tant que le client fait des requêtes, les traiter
         gestionnaireClient.traiterRequetes(new ServicesSQL());

      } catch (IOException e)
      {
         Journalisation.getERR().severe("Erreur dans le traitement des requêtes : " + e.getMessage());
      } finally
      {
         try
         {
            // On se retire du pool de clients
            serveur.supprimerSocket(socket);

            // Fermer le socket
            socket.close();
         } catch (IOException e)
         {
            Journalisation.getERR().severe("Erreur dans le gestionnaire debuild  socket lors de la fermeture / " +
                                                 "retrait de la liste des sockets ouverts dans le serveur : " + e.getMessage());
         }
      }
   }
}
