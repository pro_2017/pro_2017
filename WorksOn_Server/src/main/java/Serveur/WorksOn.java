package Serveur;

import Utilitaires.Journalisation;

import java.io.IOException;

/**
 * Programme principal. Ce dernier se charge de créer un thread Serveur afin de pouvoir traiter des clients.
 * Un hook pour le shutdown est créé afin de capturer un éventuel Ctrl + C (SIGINT) pour fermer les connexions aux clients
 * de manière propre (fermer les sockets).
 */
public class WorksOn
{
   public static void main(String[] argv) throws IOException
   {
      final Serveur serveur = new Serveur();
      Thread threadServeur = new Thread(serveur);

      // On démarre le thread serveur
      threadServeur.start();

      // Intercepter la fin du programme (Ctrl + C), et termine toutes les connexion vers les clients afin d'être propre
      Runtime.getRuntime().addShutdownHook(new Thread()
      {
         public void run()
         {
            serveur.fermerServeur();
         }
      });

      // Attendre la fin du thread
      try
      {
         threadServeur.join();
      } catch (InterruptedException e)
      {
         Journalisation.getLOG().severe("Problème durant l'attente du thread serveur : " + e.getMessage());
      }
   }
}
