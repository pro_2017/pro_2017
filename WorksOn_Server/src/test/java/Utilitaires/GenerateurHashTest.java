package Utilitaires;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by matthieu on 4/23/17.
 */
public class GenerateurHashTest
{
   @Test
   public void leCalculDuHashDoitEtreCorrect() throws Exception
   {
      String result = GenerateurHash.calculerHash("testDeHash");
      assertEquals("56641dd4ee119556cb1a1f0a78acac4b57263a4b8bb453f0d4285469731ca9aa", result);
      String result2 = GenerateurHash.calculerHash("1234abcd");
      assertEquals("221b37fcdb52d0f7c39bbd0be211db0e1c00ca5fbecd5788780463026c6b964b", result2);
      String result3 = GenerateurHash.calculerHash("ZEFL35FRthisisasuperpasswordwithsalt");
      assertEquals("f1252af876e765888da236882ae4b694a8e9943f3eaf8bb1057129be8f62a971", result3);
   }

   @Test
   public void retourneUneChaineVideEnCasDeChaineVideFournie()
   {
      String result = GenerateurHash.calculerHash("");
      assertEquals("", result);
   }

   @Test
   public void retournerUneChaineVideEnCasDeNullPtr()
   {
      String result = GenerateurHash.calculerHash(null);
      assertEquals("", result);
   }
}