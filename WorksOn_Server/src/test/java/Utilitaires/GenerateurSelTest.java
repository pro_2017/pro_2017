package Utilitaires;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Classe permettant de tester le bon fonctionnement du générateur de Sel
 */
public class GenerateurSelTest
{
   @Test
   public void doitGenererDesSelsDifferents() throws Exception
   {
      ArrayList<String> liste = new ArrayList<String>();

      for (int i = 0; i < 50000; i++)
      {
         String sel = GenerateurSel.genererSel();

         // Si la liste ne le contient pas déjà, l'ajouter
         boolean contient = liste.contains(sel);
         assertFalse(contient);

         liste.add(sel);
      }
   }

}