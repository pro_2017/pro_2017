package Utilitaires;

import Objets.Token;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by matthieu on 4/23/17.
 */
public class GenerateurTokenTest
{
   @Test
   public void doitGenererDesTokensUniques() throws Exception
   {
      ArrayList<Token> liste = new ArrayList<Token>();

      for (int i = 0; i < 50000; i++)
      {
         Token token = GenerateurToken.genererToken();

         boolean contient = liste.contains(token);
         assertFalse(contient);

         liste.add(token);
      }
   }

}