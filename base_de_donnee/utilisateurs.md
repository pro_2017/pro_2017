#Liste des utilisateurs disponibles#

| Nom d'utilisateur | Mot de passe | Entreprise | Rôle|
|---|---|--|--|
|Tim.Cook|password|Apple|Administateur|
|Steve.Jobs.cdp|123456|Apple|Chef de projet|
|Jonathan.Ive|iloveyou|Apple|Utilisateur|
|Jeff.Bezos|titi42|Amazon|Administateur|
|Brian.Olsavsky.cdp|helloworld|Amazon|Chef de projet|
|Werner.Vogels|ilovepie|Amazon|Utilisateur|
|Bill.Gates|iamrich|Microsoft|Administateur|
|Satya.Nadella.cdp|iampoor|Microsoft|Chef de projet|
|Kevin.Scott|iknownothing|Microsoft|Utilisateur|