DROP DATABASE workson
--
-- Auteur :  Thibaud Besseau
-- Crée le : 18 Avril 2017
-- Version MySQL :  5.6.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";
-- Active l'Events Scheduler
SET GLOBAL EVENT_SCHEDULER = ON;


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `workson`
--
CREATE DATABASE IF NOT EXISTS `workson` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `workson`;

-- --------------------------------------------------------

--
-- Structure de la table `employe`
--

DROP TABLE IF EXISTS `employe`;
CREATE TABLE IF NOT EXISTS `employe` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Prenom` varchar(25) NOT NULL,
  `Nom` varchar(25) NOT NULL,
  `Nom_Utilisateur` varchar(25) NOT NULL,
  `Mot_De_Passe` varchar(64) NOT NULL,
  `Sel_MDP` varchar(32) NOT NULL,
  `Id_Entreprise` int(11) NOT NULL,
  `Id_Role` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Nom_Utilisateur` (`Nom_Utilisateur`),
  KEY `Id_Entreprise` (`Id_Entreprise`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `employe`
--


INSERT INTO workson.employe (Id,Prenom, Nom, Nom_Utilisateur, Mot_De_Passe, Sel_MDP, Id_Entreprise, Id_Role) VALUES (1,'Tim', 'Cook', 'Tim.Cook', '5c5420982436d4723449b85394e09dc0a07c508b4b26607285802c658c0916a1', 'HXQrrl0V', 1, 2);
INSERT INTO workson.employe (Id,Prenom, Nom, Nom_Utilisateur, Mot_De_Passe, Sel_MDP, Id_Entreprise, Id_Role) VALUES (2,'Steve', 'Jobs', 'Steve.Jobs', 'cb19c574bfa5e9514d96c934d5c182d2402eda7b51065641af537be36d2aee75', 'lQCFFKBC', 1, 1);
INSERT INTO workson.employe (Id,Prenom, Nom, Nom_Utilisateur, Mot_De_Passe, Sel_MDP, Id_Entreprise, Id_Role) VALUES (3,'Jonathan', 'Ive', 'Jonathan.Ive', 'c92a07838b2f56e9da1f7d269be758b4d9ec3bf3442f936d4acb1e594fefec40', 'jLcZqN0H', 1, 0);
INSERT INTO workson.employe (Id,Prenom, Nom, Nom_Utilisateur, Mot_De_Passe, Sel_MDP, Id_Entreprise, Id_Role) VALUES (4,'Jeff', 'Bezos', 'Jeff.Bezos', 'c92a07838b2f56e9da1f7d269be758b4d9ec3bf3442f936d4acb1e594fefec40', 'MHbHwITa', 2, 2);
INSERT INTO workson.employe (Id,Prenom, Nom, Nom_Utilisateur, Mot_De_Passe, Sel_MDP, Id_Entreprise, Id_Role) VALUES (5,'Brian', 'Olsavsky', 'Brian.Olsavsky', '9fe44b43bbac47b33c64e8816a6854785f16009d825a8751dcd8d2bd69a08b05', 'eJy5Rzic', 2, 1);
INSERT INTO workson.employe (Id,Prenom, Nom, Nom_Utilisateur, Mot_De_Passe, Sel_MDP, Id_Entreprise, Id_Role) VALUES (6,'Werner', 'Vogels', 'Werner.Vogels', '4ad92e55f9f24dbc9df9a6c225794936c9c7827887f81f06db872254d5839139', 'mCQHRiCg', 2, 0);
INSERT INTO workson.employe (Id,Prenom, Nom, Nom_Utilisateur, Mot_De_Passe, Sel_MDP, Id_Entreprise, Id_Role) VALUES (7,'Bill', 'Gates', 'Bill.Gates', '4bb2825aa4318c645b096f6e257c37dcada23762d61e518696fcf5fed2c3ef53', 'nhGVL4zL', 6, 2);
INSERT INTO workson.employe (Id,Prenom, Nom, Nom_Utilisateur, Mot_De_Passe, Sel_MDP, Id_Entreprise, Id_Role) VALUES (8,'Satya', 'Nadella', 'Satya.Nadella', '0c62e196026e017bc419b7dea827d6a6437bf3cee3e2ced1114616a04fbf882e', 'VPNKASUX', 6, 1);
INSERT INTO workson.employe (Id,Prenom, Nom, Nom_Utilisateur, Mot_De_Passe, Sel_MDP, Id_Entreprise, Id_Role) VALUES (9,'Kevin', 'Scott', 'Kevin.Scott', '3545ff9006ab09dac95ccf81ef167aa51a36388aca57ba1a9b967b377fdd6496', 'zDeMu/al', 6, 0);
INSERT INTO workson.employe (Id,Prenom, Nom, Nom_Utilisateur, Mot_De_Passe, Sel_MDP, Id_Entreprise, Id_Role) VALUES (10,'Matthieu', 'Chatelan', 'Matthieu.Chatelan', 'f87587c41dad6c31b1c051eb937135e4b98ac4ad63d194e8686aca20af6525a8', 'MT8CdQih', 6, 1);
INSERT INTO workson.employe (Prenom, Nom, Nom_Utilisateur, Mot_De_Passe, Sel_MDP, Id_Entreprise, Id_Role) VALUES ('John', 'Doe', 'John.Doe', '67e7a40c4598b2b26bf7e1348027dcd0250d53c9ab7a3611141b61aa56254eee', 'o963t4xM', 1, 0);
INSERT INTO workson.employe (Prenom, Nom, Nom_Utilisateur, Mot_De_Passe, Sel_MDP, Id_Entreprise, Id_Role) VALUES ('Marcel', 'Proust', 'Marcel.Proust', 'c2bf4fd7254c89d192ecd5003ad7c01297f8e91e61125d0576279a5cdeead06a', 'XbeOiUZf', 1, 0);
INSERT INTO workson.employe (Prenom, Nom, Nom_Utilisateur, Mot_De_Passe, Sel_MDP, Id_Entreprise, Id_Role) VALUES ('Michael', 'Ratzolf', 'Michael.Ratzolf', 'c2bf4fd7254c89d192ecd5003ad7c01297f8e91e61125d0576279a5cdeead06a', 'IuJtGrfp', 1, 0);
INSERT INTO workson.employe (Prenom, Nom, Nom_Utilisateur, Mot_De_Passe, Sel_MDP, Id_Entreprise, Id_Role) VALUES ('Loan', 'Lassalle', 'Loan.Lassalle', '91541af8a2e69d4c4f57f92588dba6e2d2fe0c1d32be8d9611b78120b42a753a', 'IjdjZjEf', 1, 0);
INSERT INTO workson.employe (Prenom, Nom, Nom_Utilisateur, Mot_De_Passe, Sel_MDP, Id_Entreprise, Id_Role) VALUES ('Tano', 'Iannetta', 'Tano.Iannetta', '95dae9176deee747d3c0b00a29341d2260f196bd81e402f80190987328642acb', 'NfzWPOIk', 1, 0);
INSERT INTO workson.employe (Prenom, Nom, Nom_Utilisateur, Mot_De_Passe, Sel_MDP, Id_Entreprise, Id_Role) VALUES ('Freddy', 'Mercury', 'Freddy.Mercury', '23fd6dc9b6a37b877f7a924e6b0ef52a9b21b5c1f6695f3cd87f397058d6b85e', 'LkfHueZf', 1, 0);
INSERT INTO workson.employe (Prenom, Nom, Nom_Utilisateur, Mot_De_Passe, Sel_MDP, Id_Entreprise, Id_Role) VALUES ('Homer', 'Simpson', 'Homer.Simpson', 'cfef242e109e6142899179429e0adb1c4e4e3993bf2715f0133ca79e473c89b5', 'IedQWEac', 1, 1);
INSERT INTO workson.employe (Prenom, Nom, Nom_Utilisateur, Mot_De_Passe, Sel_MDP, Id_Entreprise, Id_Role) VALUES ('Eric', 'Lefrancois', 'Eric.Lefrancois', '4f1dd0bf28107f884d09345d34e881c6499ebe995fb08e1a11503033016e0c13', 'PokUjmLo', 1, 0);
INSERT INTO workson.employe (Prenom, Nom, Nom_Utilisateur, Mot_De_Passe, Sel_MDP, Id_Entreprise, Id_Role) VALUES ('Jean', 'Crypte', 'Jean.Crypte', '4a6c40f32f1dc77b8a068d0002adff486b48ddc9de5a1f49898083375bf3f127', 'UjtFreDW', 1, 0);
INSERT INTO workson.employe (Prenom, Nom, Nom_Utilisateur, Mot_De_Passe, Sel_MDP, Id_Entreprise, Id_Role) VALUES ('Jean', 'Kevin', 'Jean.Kevin', 'bbc244dd0ecb075fce80fa8477b39a248a51106d9c97fc11a5836152de8d29d6', 'EdwRTzHU', 1, 0);
INSERT INTO workson.employe (Prenom, Nom, Nom_Utilisateur, Mot_De_Passe, Sel_MDP, Id_Entreprise, Id_Role) VALUES ('Marcel', 'Graaf', 'Marcel.Graaf', '5a46eeb14b93014aef6fc8dfa7b1725647045dbd007a842073168c36b112fed7', 'YXCRTztr', 1, 0);
INSERT INTO workson.employe (Prenom, Nom, Nom_Utilisateur, Mot_De_Passe, Sel_MDP, Id_Entreprise, Id_Role) VALUES ('René', 'Rentsch', 'René.Rentsch', '0ccca71625b7adb52934797eceb724af663d80f462cb8d24e8d0eefc619b155d', 'UjHztGrF', 1, 1);
INSERT INTO workson.employe (Prenom, Nom, Nom_Utilisateur, Mot_De_Passe, Sel_MDP, Id_Entreprise, Id_Role) VALUES ('Walid', 'Kouba', 'Walid.Kouba', '97031a0010832175c872f638252204eb348d0010f2f50f8559984c143c0e9ccc', 'RfgToiJz', 1, 1);
INSERT INTO workson.employe (Prenom, Nom, Nom_Utilisateur, Mot_De_Passe, Sel_MDP, Id_Entreprise, Id_Role) VALUES ('Lara', 'Chauffoureaux', 'Lara.Chauffoureaux', '213d68eda4c6abe4dc1608aea1d74e46086d1ed30c38fdb33ef379c0d107cd92', 'JAHFjshf', 1, 1);
INSERT INTO workson.employe (Prenom, Nom, Nom_Utilisateur, Mot_De_Passe, Sel_MDP, Id_Entreprise, Id_Role) VALUES ('Ludovic', 'Richard', 'Ludovic.Richard', 'dea46955ba83f3b41db8d20c0ec2f3939799bab0b479706583ff00c15b1f1fb4', 'AShgsfAs', 1, 1);
INSERT INTO workson.employe (Prenom, Nom, Nom_Utilisateur, Mot_De_Passe, Sel_MDP, Id_Entreprise, Id_Role) VALUES ('Fred', 'Red', 'Fred.Red', '5f90fa61c7303462b02da26d88a652643304508044fc2c749ec937c7fa7b46cc', 'IQfnsjSA', 1, 0);
INSERT INTO workson.employe (Prenom, Nom, Nom_Utilisateur, Mot_De_Passe, Sel_MDP, Id_Entreprise, Id_Role) VALUES ('Carla', 'Marlu', 'Carla.Marlu', '9f3bf463144c948ad3fee24aea50db2fee6185a70857af6e4754ebc86e0bf50d', 'OASJhfhs', 1, 0);
INSERT INTO workson.employe (Prenom, Nom, Nom_Utilisateur, Mot_De_Passe, Sel_MDP, Id_Entreprise, Id_Role) VALUES ('Damien', 'Carnal', 'Damien.Carnal', '8f4f6397569f46427aa796e3149a4c7ea39596efa47311544b4e2828124c928a', 'IUidfhzE', 1, 0);
INSERT INTO workson.employe (Prenom, Nom, Nom_Utilisateur, Mot_De_Passe, Sel_MDP, Id_Entreprise, Id_Role) VALUES ('Thibaud', 'Besseau', 'Thibaud.Besseau', '81facaee0e0d28bf0e5d8043f81a9e2fa92bc70e0c54c5e5bc0118577fb64b2b', 'JjgEpqow', 1, 0);
INSERT INTO workson.employe (Prenom, Nom, Nom_Utilisateur, Mot_De_Passe, Sel_MDP, Id_Entreprise, Id_Role) VALUES ('Yann', 'Mahmoudi', 'Yann.Mahmoudi', '7162a4db47f0bc86f8e2222cf9f75250ecc145d104bc4d60788b785c09e8dffc', 'IsuQfQew', 1, 0);
INSERT INTO workson.employe (Prenom, Nom, Nom_Utilisateur, Mot_De_Passe, Sel_MDP, Id_Entreprise, Id_Role) VALUES ('Carlos', 'Pena', 'Carlos.Pena', '2850d88e69e28fa3f6c1aba2785e071dd8a36bb58e22f3a72a91d7fad1dc0e86', 'WWewlkis', 1, 0);
INSERT INTO workson.employe (Prenom, Nom, Nom_Utilisateur, Mot_De_Passe, Sel_MDP, Id_Entreprise, Id_Role) VALUES ('Laura', 'Rutzberg', 'Laura.Rutzberg', 'e6f2ee0a91a35fd1f0cc935700b6ea14eef55485d4afa1076cbd90f0d28b271b', 'ASkfieEW', 1, 0);
INSERT INTO workson.employe (Prenom, Nom, Nom_Utilisateur, Mot_De_Passe, Sel_MDP, Id_Entreprise, Id_Role) VALUES ('Hans', 'Müller', 'Hans.Müller', '4229c5f6172d2a3ba4322f7e9d2ca61da2c0d67c05d3542e0694477bde2f78fc', 'AQnBVgeH', 1, 0);
INSERT INTO workson.employe (Prenom, Nom, Nom_Utilisateur, Mot_De_Passe, Sel_MDP, Id_Entreprise, Id_Role) VALUES ('', '', '', 'a9bc0a6e3962ae4ee5d512c86813de670c58cbba3b0a5c16fcb873d54e5d2a30', 'SOtsBRHC', 1, 0);




-- --------------------------------------------------------

--
-- Structure de la table `entreprise`
--

DROP TABLE IF EXISTS `entreprise`;
CREATE TABLE IF NOT EXISTS `entreprise` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nom` varchar(30) NOT NULL,
  `Telephone` varchar(15) NOT NULL,
  `Adresse` varchar(75) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Nom` (`Nom`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42 ;

--
-- Contenu de la table `entreprise`
--

INSERT INTO `entreprise` (`Id`, `Nom`, `Telephone`, `Adresse`) VALUES
(1,"Apple","408-996-1010","1 Infinite Loop Cupertino, CA 95014"),
(2,"Amazon","(206)-266-1000","410 Terry Ave. North Seattle, WA"),
(3,"Dell","(512)-338-4400","1 Dell Way Roundrock, TX 78682"),
(4,"Facebook","(650)-308-7300","1601 Willow Rd. Menlo Park, CA 94025"),
(5,"Google","(650)-253-0000","1600 Amphitheatre Parkway Mountain View, CA 94043"),
(6,"Microsoft","(425)-882-8080","1 Microsoft Way Redmond, WA 98052"),
(7,"Oracle","+1.650.506.7000","Oracle Corporation 500 Oracle Parkway Redwood Shores, CA 94065"),
(8,"SAP","+49 (0)6227 / 7-47474","Dietmar-Hopp-Allee 16 69190 Walldorf ");



-- --------------------------------------------------------

--
-- Structure de la table `etat`
--

DROP TABLE IF EXISTS `etat`;
CREATE TABLE IF NOT EXISTS `etat` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nom` varchar(20) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `etat`
--

INSERT INTO `etat` (`Id`, `Nom`) VALUES

(1, 'En cours'),
(2, 'En Attente'),
(3, 'Annulé'),
(4, 'Terminé');

-- --------------------------------------------------------

--
-- Structure de la table `log`
--

DROP TABLE IF EXISTS `log`;
CREATE TABLE IF NOT EXISTS `log` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Debut` timestamp NOT NULL,
  `Fin` timestamp NOT NULL,
  `Remarques` text NOT NULL,
  `Id_Tache` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `Id_Tache` (`Id_Tache`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `log`
--

INSERT INTO `log` (`Id`, `Debut`, `Fin`, `Remarques`, `Id_Tache`) VALUES
(2, '2017-04-17 10:32:02', '2017-04-17 21:32:02', "J'ai fini très tard car rien ne marchait", 2),
(3, '2017-05-15 08:30:00', '2017-05-15 11:39:13', 'Tache terminé, je passe à la suivante', 2);

-- --------------------------------------------------------

--
-- Structure de la table `module`
--

DROP TABLE IF EXISTS `module`;
CREATE TABLE IF NOT EXISTS `module` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nom` varchar(25) NOT NULL,
  `DeadLine` datetime NOT NULL,
  `Description` text NOT NULL,
  `Id_Projet` int(11) NOT NULL,
  `Id_Etat` int(11) NOT NULL,
  `Id_Priorite` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `Id_Priorite` (`Id_Priorite`),
  KEY `Id_Etat` (`Id_Etat`),
  KEY `Id_Projet` (`Id_Projet`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `module`
--

INSERT INTO `module` (`Id`, `Nom`, `DeadLine`, `Description`, `Id_Projet`, `Id_Etat`, `Id_Priorite`) VALUES
(1, 'Module 1', '2017-04-17 21:36:08', 'Creation de la base de donnée', 1, 1, 1),
(2, 'Module 2', '2017-04-22 21:38:04', 'Creation des vues', 1, 1, 1),
(3, 'Module 3', '2017-05-17 21:45:23', 'Creation du client', 1, 1, 1),
(4, 'Module 4', '2017-05-18 21:46:19', 'Creation du serveur', 1, 1, 1),
(5, 'Module 5', '2017-04-22 21:38:04', 'Creation des documentation', 2, 1, 1),
(6, 'Module 6', '2017-05-17 21:45:23', 'Creation du test', 2, 1, 1),
(7, 'Module 7', '2017-04-22 21:38:04', 'Creation des vues', 3, 1, 1),
(8, 'Module 8', '2017-05-17 21:45:23', 'Creation du serveur', 3, 1, 1),
(9, 'Module 7', '2017-05-22 21:38:04', 'Mise en place infra', 3, 1, 1),
(10, 'Module 8', '2017-05-27 21:45:23', 'Test infra', 3, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `module_effectif`
--
DROP TABLE IF EXISTS `module_effectif`;
CREATE TABLE IF NOT EXISTS `module_effectif` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Id_Employe` int(11) NOT NULL,
  `Id_Module` int(11) NOT NULL,
  `Date_Ajout` datetime NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `Id_Employe` (`Id_Employe`,`Id_Module`),
  KEY `Id_Module` (`Id_Module`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
--
-- Contenu de la table `module_effectif`
--


INSERT INTO workson.module_effectif (Id_Module, Id_Employe,Date_Ajout) VALUES (2, 1,'2017-05-23 00:00:00');
INSERT INTO workson.module_effectif (Id_Module, Id_Employe,Date_Ajout) VALUES (2, 2,'2017-05-23 00:00:00');
INSERT INTO workson.module_effectif (Id_Module, Id_Employe,Date_Ajout) VALUES (2, 3,'2017-05-23 00:00:00');
INSERT INTO workson.module_effectif (Id_Module, Id_Employe,Date_Ajout) VALUES (3, 3,'2017-05-23 00:00:00');
INSERT INTO workson.module_effectif (Id_Module, Id_Employe,Date_Ajout) VALUES (5, 1,'2017-05-23 00:00:00');
INSERT INTO workson.module_effectif (Id_Module, Id_Employe,Date_Ajout) VALUES (5, 2,'2017-05-23 00:00:00');
INSERT INTO workson.module_effectif (Id_Module, Id_Employe,Date_Ajout) VALUES (6, 1,'2017-05-23 00:00:00');
INSERT INTO workson.module_effectif (Id_Module, Id_Employe,Date_Ajout) VALUES (9, 6,'2017-05-23 00:00:00');
INSERT INTO workson.module_effectif (Id_Module, Id_Employe,Date_Ajout) VALUES (10, 4,'2017-05-23 00:00:00');
INSERT INTO workson.module_effectif (Id_Module, Id_Employe,Date_Ajout) VALUES (10, 5,'2017-05-23 00:00:00');
INSERT INTO workson.module_effectif (Id_Module, Id_Employe,Date_Ajout) VALUES (10, 6,'2017-05-23 00:00:00');
INSERT INTO workson.module_effectif (Id_Module, Id_Employe,Date_Ajout) VALUES (5, 3,'2017-05-23 00:00:00');
INSERT INTO workson.module_effectif (Id_Module, Id_Employe,Date_Ajout) VALUES (6, 3,'2017-05-23 00:00:00');


-- --------------------------------------------------------

--
-- Structure de la table `projet`
--

DROP TABLE IF EXISTS `projet`;
CREATE TABLE IF NOT EXISTS `projet` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nom` varchar(20) NOT NULL,
  `Description` text NOT NULL,
  `Date_Depart` datetime NOT NULL,
  `Date_Rendu` datetime NOT NULL,
  `Id_Chef_De_Projet` int(11) NOT NULL,
  `Id_Etat` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `Id_Chef_De_Projet` (`Id_Chef_De_Projet`,`Id_Etat`),
  KEY `Id_Etat` (`Id_Etat`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `projet`
--

INSERT INTO `projet` (`Id`, `Nom`, `Description`, `Date_Depart`, `Date_Rendu`, `Id_Chef_De_Projet`, `Id_Etat`) VALUES
(1, 'Projet 1', 'très long ce projet', '2017-04-17 20:07:02', '2017-05-17 20:07:02', 1, 1),
(2, 'Projet 2', 'très court ce projet', '2017-03-17 20:33:25', '2017-03-18 20:33:25', 2, 1),
(3, 'Projet 3', 'très court ce projet', '2017-03-17 20:33:25', '2017-03-18 20:33:25', 5, 1);


-- --------------------------------------------------------

--
-- Structure de la table `tache`
--

DROP TABLE IF EXISTS `tache`;
CREATE TABLE IF NOT EXISTS `tache` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nom` varchar(30) NOT NULL,
  `Duree_Planifiee` time NOT NULL,
  `Duree_Effective` time NOT NULL,
  `Id_Module` int(11) NOT NULL,
  `Id_Etat` int(11) NOT NULL,
  `Id_Employe` int(11) NOT NULL,
  `Id_Priorite` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `Id_Module` (`Id_Module`,`Id_Etat`,`Id_Employe`),
  KEY `Id_Etat` (`Id_Etat`),
  KEY `Id_Utilisateur` (`Id_Employe`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `tache`
--

INSERT INTO `tache` (`Id`, `Nom`, `Duree_Planifiee`, `Duree_Effective`, `Id_Module`, `Id_Etat`, `Id_Employe`, `Id_Priorite`) VALUES
(2, 'tache 1', '01:08:23', '01:00:00', 2, 1, 3, 1),
(3, 'tache 2', '02:00:00', '01:00:00', 3, 1, 2, 1),
(4, 'tache 3', '02:00:00', '00:00:00', 2, 1, 3, 1);

-- --------------------------------------------------------

--
-- Structure de la table `token`
--
CREATE TABLE IF NOT EXISTS `token` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Token` varchar(32) NOT NULL,
  `Date_Generation` timestamp NOT NULL,
  `Id_Employe` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `Id_Employe` (`Id_Employe`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


--
-- Contenu de la table `token`
--

INSERT INTO `token` (`Token`, `Date_Generation`) VALUES
( '2/1Dn3gnCO6yBpDsFhXRwz2X76w=', '2017-04-17 22:00:38');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `employe`
--
ALTER TABLE `employe`
  ADD CONSTRAINT `employe_ibfk_1` FOREIGN KEY (`Id_Entreprise`) REFERENCES `entreprise` (`Id`);

--
-- Contraintes pour la table `log`
--
ALTER TABLE `log`
  ADD CONSTRAINT `log_ibfk_1` FOREIGN KEY (`Id_Tache`) REFERENCES `tache` (`Id`);

--
-- Contraintes pour la table `module`
--
ALTER TABLE `module`
  ADD CONSTRAINT `module_ibfk_1` FOREIGN KEY (`Id_Projet`) REFERENCES `projet` (`Id`),
  ADD CONSTRAINT `module_ibfk_2` FOREIGN KEY (`Id_Etat`) REFERENCES `etat` (`Id`);

--
-- Contraintes pour la table `module_effectif`
--
ALTER TABLE `module_effectif`
  ADD CONSTRAINT `module_effectif_ibfk_3` FOREIGN KEY (`Id_Module`) REFERENCES `module` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `module_effectif_ibfk_2` FOREIGN KEY (`Id_Employe`) REFERENCES `employe` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;
--
-- Contraintes pour la table `projet`
--
ALTER TABLE `projet`
  ADD CONSTRAINT `projet_ibfk_1` FOREIGN KEY (`Id_Chef_De_Projet`) REFERENCES `employe` (`Id`),
  ADD CONSTRAINT `projet_ibfk_2` FOREIGN KEY (`Id_Etat`) REFERENCES `etat` (`Id`);

--
-- Contraintes pour la table `tache`
--
ALTER TABLE `tache`
  ADD CONSTRAINT `tache_ibfk_1` FOREIGN KEY (`Id_Module`) REFERENCES `module` (`Id`),
  ADD CONSTRAINT `tache_ibfk_2` FOREIGN KEY (`Id_Employe`) REFERENCES `employe` (`Id`),
  ADD CONSTRAINT `tache_ibfk_3` FOREIGN KEY (`Id_Etat`) REFERENCES `etat` (`Id`);

--
-- Contraintes pour la table `token`
--
ALTER TABLE `token`
  ADD CONSTRAINT `token_ibfk_1` FOREIGN KEY (`Id_Employe`) REFERENCES `employe` (`Id`);



CREATE EVENT supprimerTokenExpires
  ON SCHEDULE EVERY 1 HOUR
  DO
  DELETE FROM token WHERE Date_Generation<= DATE_SUB(NOW(),INTERVAL 8 HOUR);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;






