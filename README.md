# README #

- Etablir le cahier des charges (et regarder ce qui se fait deja comme prog similaire)
- Se répartir le travail entre nous
- Schéma UML de la DB
- Schéma du programme (fonctionnement général ou schéma des classes)
- Avoir une idée de la gueule générale de l'interface
- Code couleur du logo vert : #00c00, noir : #000000

Sockets SSL : 

keytool -genkey -keystore mySrvKeystore -keyalg RSA
java -Djavax.net.ssl.keyStore=mySrvKeystore -Djavax.net.ssl.keyStorePassword=123456 EchoServer

java -Djavax.net.ssl.trustStore=mySrvKeystore -Djavax.net.ssl.trustStorePassword=123456 EchoClient


Procédure pour build l'image docker 
docker build -t image_workson_serveur .
docker tag image_workson_serveur monDEPO/Workson
docker push monDepo/Workson