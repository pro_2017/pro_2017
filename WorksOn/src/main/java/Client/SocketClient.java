package Client;

import Objets.*;
import Protocoles.*;
import Utilitaires.Journalisation;

import com.google.gson.Gson;
import java.lang.String;
import javax.net.SocketFactory;
import javax.net.ssl.SSLSocketFactory;
import java.io.*;
import java.net.Socket;

/**
 * Classe ayant la responsabilité de créer les requeteServeur et les requeteConnexion selon les arguments (valides) que
 * nous envoie le controleur. Gère également les ouvertures/fermetures du socket et l'envoie de ces différentes requetes
 * au serveur.
 */
public class SocketClient
{
   private Socket socket;         // Le socket ssl établissant la connexion vers le serveur
   private BufferedReader reader; // Le reader de ce socket
   private PrintWriter writer;    // Le writer de ce socket
   private int idEntreprise;      // L'id de l'entreprise du client actuellement connecté setter à l'authentification
   private Token token;           // Le token de la session, setter à l'authentification
   private Gson gson;             // L'instance gson qui permettra de faire la sérialisation / désérialisation

   /**
    * Constructeur du socketClient, on initialise déjà l'instance du gson la suite viendra en temps voulu.
    */
   public SocketClient()
   {
      socket = null;
      reader = null;
      writer = null;
      idEntreprise = -1;
      token = null;
      gson = new Gson();
   }

   /**
    * Méthode envoyant à travers le socket la requeteServeur permettant de récupérer la liste de tous les projets d'une
    * entreprise.
    *
    * @return La liste de tous les projets d'une entreprise
    */
   public ListeProjets listeProjets()
   {
      RequeteServeur requete = new RequeteServeur(ProtocoleWorksOn.CMD_GET_LISTE_PROJETS, idEntreprise);

      writer.println(gson.toJson(requete));
      writer.flush();

      ListeProjets reponse = null;

      try
      {
         reponse = gson.fromJson(reader.readLine(), ListeProjets.class);
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Impossible de lire la réponse du serveur : " + e.getMessage());
      }

      return reponse;
   }

   /**
    * Méthode envoyant à travers le socket la requeteServeur permettant de récupérer la liste des projets d'un
    * utilisateur.
    *
    * @param idUtilisateur L'id de l'utilisateur dont on veut récupérer les projets
    * @return La liste des projets de l'utilisateur
    */
   public ListeProjets listeProjetsUtilisateur(String idUtilisateur)
   {
      RequeteServeur requete = new RequeteServeur(ProtocoleWorksOn.CMD_GET_LISTE_PROJET_UTILISATEUR, idEntreprise, idUtilisateur);

      writer.println(gson.toJson(requete));
      writer.flush();

      ListeProjets reponse = null;

      try
      {
         reponse = gson.fromJson(reader.readLine(), ListeProjets.class);
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Impossible de lire la réponse du serveur : " + e.getMessage());
      }

      return reponse;
   }

   /**
    * Méthode envoyant à travers le socket la requeteServeur permettant de récupérer la liste des projets qu'un chef
    * gère.
    *
    * @param idChef L'id du chef dont on veut récupérer les projets
    * @return La liste des projets gérés par ce chef
    */
   public ListeProjets listeProjetsChef(String idChef)
   {
      RequeteServeur requete = new RequeteServeur(ProtocoleWorksOn.CMD_GET_LISTE_PROJET_CHEF, idEntreprise, idChef);

      writer.println(gson.toJson(requete));
      writer.flush();

      ListeProjets reponse = null;

      try
      {
         reponse = gson.fromJson(reader.readLine(), ListeProjets.class);
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Impossible de lire la réponse du serveur : " + e.getMessage());
      }

      return reponse;
   }

   /**
    * Méthode envoyant à travers le socket la requeteServeur permettant de récupérer la liste des modules pour un projet
    * donné.
    *
    * @param idProjet L'id du projet dont on veut récupérer les modules
    * @return La liste des modules du projet
    */
   public ListeModules listeModulesProjets(String idProjet)
   {
      RequeteServeur requete = new RequeteServeur(ProtocoleWorksOn.CMD_GET_LISTE_MODULES, idEntreprise, idProjet);

      writer.println(gson.toJson(requete));
      writer.flush();

      ListeModules reponse = null;

      try
      {
         reponse = gson.fromJson(reader.readLine(), ListeModules.class);
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Impossible de lire la réponse du serveur : " + e.getMessage());
      }

      return reponse;
   }

   /**
    * Méthode envoyant à travers le socket la requeteServeur permettant de récupérer la liste des tâches d'un
    * utilisateur donné sur un module donné.
    *
    * @param idUtilisateur L'id de l'utilisateur
    * @param idModule      L'id du module
    * @return La liste des tâches de cet utilisateur sur ce module
    */
   public ListeTaches listeTachesUtilisateurSurModule(String idUtilisateur, String idModule)
   {
      RequeteServeur requete = new RequeteServeur(ProtocoleWorksOn.CMD_GET_LISTE_TACHES_MODULE, idEntreprise, idUtilisateur, idModule);

      writer.println(gson.toJson(requete));
      writer.flush();

      ListeTaches reponse = null;

      try
      {
         reponse = gson.fromJson(reader.readLine(), ListeTaches.class);
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Impossible de lire la réponse du serveur : " + e.getMessage());
      }

      return reponse;
   }

   /**
    * Méthode envoyant à travers le socket la requeteServeur permettant de récupérer la liste des tâches d'un
    * module donné.
    *
    * @param idModule L'id de ce module
    * @return La liste des tâches de ce module
    */
   public ListeTaches listeTachesModule(String idModule)
   {
      RequeteServeur requete = new RequeteServeur(ProtocoleWorksOn.CMD_GET_LISTE_TACHES, idEntreprise, idModule);

      writer.println(gson.toJson(requete));
      writer.flush();

      ListeTaches reponse = null;

      try
      {
         reponse = gson.fromJson(reader.readLine(), ListeTaches.class);
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Impossible de lire la réponse du serveur : " + e.getMessage());
      }

      return reponse;
   }

   /**
    * Méthode envoyant à travers le socket la requeteServeur permettant de récupérer la liste des utilisateurs
    * travaillants sur un module donné.
    *
    * @param idModule L'id du module
    * @return La liste des utilisateurs travaillants sur ce module
    */
   public ListeEmployes listeUtilisateursModule(String idModule)
   {
      RequeteServeur requete = new RequeteServeur(ProtocoleWorksOn.CMD_GET_LISTE_UTILISATEURS_MODULE, idEntreprise, idModule);

      writer.println(gson.toJson(requete));
      writer.flush();

      ListeEmployes reponse = null;

      try
      {
         reponse = gson.fromJson(reader.readLine(), ListeEmployes.class);
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Impossible de lire la réponse du serveur : " + e.getMessage());
      }

      return reponse;
   }

    /*
    public ListeEmployes listeUtilisateursProjet(String idProjet) throws IOException
    {
        RequeteServeur requete = new RequeteServeur(ProtocoleWorksOn.CMD_GET_LISTE_UTILISATEURS_PROJET, idEntreprise, idProjet);

        writer.println(gson.toJson(requete));
        writer.flush();

        ListeEmployes reponse;

        try
        {
            reponse = gson.fromJson(reader.readLine(), ListeEmployes.class);
        }
        catch (IOException e)
        {
            Journalisation.getERR().severe("Impossible de lire la réponse du serveur : " + e.getMessage());
            throw e;
        }

        return reponse;
    }
    */

   /**
    * Méthode envoyant à travers le socket la requeteServeur permettant de récupérer la liste des tous les utilisateurs
    * travaillant dans l'entreprise.
    *
    * @return La liste des employés travaillants dans l'entreprise
    */
   public ListeEmployes listeEmployes()
   {
      RequeteServeur requete = new RequeteServeur(ProtocoleWorksOn.CMD_GET_LISTE_UTILISATEURS, idEntreprise);

      writer.println(gson.toJson(requete));
      writer.flush();

      ListeEmployes reponse = null;

      try
      {
         reponse = gson.fromJson(reader.readLine(), ListeEmployes.class);
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Impossible de lire la réponse du serveur : " + e.getMessage());
      }

      return reponse;
   }

   /**
    * Méthode envoyant à travers le socket la requeteServeur permettant de récupérer l'objet entier d'un employé donné
    * par son id.
    *
    * @param idEmploye L'id de l'employé
    * @return L'objet Employe de l'employé donné
    */
   public Employe employeParId(String idEmploye)
   {
      RequeteServeur requete = new RequeteServeur(ProtocoleWorksOn.CMD_GET_EMPLOYE, idEntreprise, idEmploye);

      writer.println(gson.toJson(requete));
      writer.flush();

      Employe reponse = null;

      try
      {
         reponse = gson.fromJson(reader.readLine(), Employe.class);
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Impossible de lire la réponse du serveur : " + e.getMessage());
      }

      return reponse;
   }

   /**
    * Méthode envoyant à travers le socket la requeteServeur permettant de récupérer l'objet entier d'un projet donné
    * par son id.
    *
    * @param idProjet L'id du projet
    * @return L'objet Projet du projet donné
    */
   public Projet projetParId(String idProjet)
   {
      RequeteServeur requete = new RequeteServeur(ProtocoleWorksOn.CMD_GET_PROJET, idEntreprise, idProjet);

      writer.println(gson.toJson(requete));
      writer.flush();

      Projet reponse = null;

      try
      {
         reponse = gson.fromJson(reader.readLine(), Projet.class);
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Impossible de lire la réponse du serveur : " + e.getMessage());
      }

      return reponse;
   }

   /**
    * Méthode envoyant à travers le socket la requeteServeur permettant de récupérer l'objet entier d'un module donné
    * par son id.
    *
    * @param idModule L'id du module
    * @return L'objet Module du module donné
    */
   public Module moduleParId(String idModule)
   {
      RequeteServeur requete = new RequeteServeur(ProtocoleWorksOn.CMD_GET_MODULE, idEntreprise, idModule);

      writer.println(gson.toJson(requete));
      writer.flush();

      Module reponse = null;

      try
      {
         reponse = gson.fromJson(reader.readLine(), Module.class);
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Impossible de lire la réponse du serveur : " + e.getMessage());
      }

      return reponse;
   }

   /**
    * Méthode envoyant à travers le socket la requeteServeur permettant de récupérer l'objet entier d'une tâche donnée
    * par son id.
    *
    * @param idTache L'id de la tâche
    * @return L'objet Tache de la tâche donnée
    */
   public Tache tacheParId(String idTache)
   {
      RequeteServeur requete = new RequeteServeur(ProtocoleWorksOn.CMD_GET_TACHE, idEntreprise, idTache);

      writer.println(gson.toJson(requete));
      writer.flush();

      Tache reponse = null;

      try
      {
         reponse = gson.fromJson(reader.readLine(), Tache.class);
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Impossible de lire la réponse du serveur : " + e.getMessage());
      }

      return reponse;
   }

   /**
    * Méthode envoyant à travers le socket la requeteServeur permettant de récupérer l'objet entier d'un log donné par
    * son id.
    *
    * @param idLog L'id du log
    * @return L'objet Log du log donné
    */
   public Log logParId(String idLog)
   {
      RequeteServeur requete = new RequeteServeur(ProtocoleWorksOn.CMD_GET_LOG, idEntreprise, idLog);

      writer.println(gson.toJson(requete));
      writer.flush();

      Log reponse = null;

      try
      {
         reponse = gson.fromJson(reader.readLine(), Log.class);
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Impossible de lire la réponse du serveur : " + e.getMessage());
      }

      return reponse;
   }

   /**
    * Méthode envoyant à travers le socket la requeteServeur permettant de demander l'ajout d'un employé à la base de
    * données.
    *
    * @param employe L'employe à ajouter
    * @return Une string valant ok si tout c'est passé correctement, une string contenant l'erreur sinon
    */
   public String creerEmploye(Employe employe)
   {
      String employeJsonifie = gson.toJson(employe);
      RequeteServeur requete = new RequeteServeur(ProtocoleWorksOn.CMD_CREER_UTILISATEUR, idEntreprise, employeJsonifie);

      writer.println(gson.toJson(requete));
      writer.flush();

      String reponse = null;

      try
      {
         reponse = reader.readLine();
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Impossible de lire la réponse du serveur : " + e.getMessage());
      }

      return reponse;
   }

   /**
    * Méthode envoyant à travers le socket la requeteServeur permettant de demander l'ajout d'un projet à la base de
    * données.
    *
    * @param projet Le projet à ajouter
    * @return Une string valant ok si tout c'est passé correctement, une string contenant l'erreur sinon
    */
   public String creerProjet(Projet projet)
   {
      String projetJsonifie = gson.toJson(projet);
      RequeteServeur requete = new RequeteServeur(ProtocoleWorksOn.CMD_CREER_PROJET, idEntreprise, projetJsonifie);

      writer.println(gson.toJson(requete));
      writer.flush();

      String reponse = null;

      try
      {
         reponse = reader.readLine();
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Impossible de lire la réponse du serveur : " + e.getMessage());
      }

      return reponse;
   }

   /**
    * Méthode envoyant à travers le socket la requeteServeur permettant de demander l'ajout d'un module à la base de
    * données.
    *
    * @param module Le module à ajouter
    * @return Une string valant ok si tout c'est passé correctement, une string contenant l'erreur sinon
    */
   public String creerModule(Module module)
   {
      String moduleJsonifie = gson.toJson(module);
      RequeteServeur requete = new RequeteServeur(ProtocoleWorksOn.CMD_CREER_MODULE, idEntreprise, moduleJsonifie);

      writer.println(gson.toJson(requete));
      writer.flush();

      String reponse = null;

      try
      {
         reponse = reader.readLine();
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Impossible de lire la réponse du serveur : " + e.getMessage());
      }

      return reponse;
   }

   /**
    * Méthode envoyant à travers le socket la requeteServeur permettant de demdander l'ajout d'une tâche à la base de
    * données.
    *
    * @param tache La tâche à ajouter
    * @return Une string valant ok si tout c'est passé correctement, une string contenant l'erreur sinon
    */
   public String creerTache(Tache tache)
   {
      String tacheJsonifie = gson.toJson(tache);
      RequeteServeur requete = new RequeteServeur(ProtocoleWorksOn.CMD_CREER_TACHE, idEntreprise, tacheJsonifie);

      writer.println(gson.toJson(requete));
      writer.flush();

      String reponse = null;

      try
      {
         reponse = reader.readLine();
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Impossible de lire la réponse du serveur : " + e.getMessage());
      }

      return reponse;
   }

   /**
    * Méthode envoyant à travers le socket la requeteServeur permettant de demander l'ajout d'un log à la base de
    * données.
    *
    * @param log Le log à ajouter
    * @return Une string valant ok si tout c'est passé correctement, une string contenant l'erreur sinon
    */
   public String creerLog(Log log)
   {
      String logJsonifie = gson.toJson(log);
      RequeteServeur requete = new RequeteServeur(ProtocoleWorksOn.CMD_CREER_LOG, idEntreprise, logJsonifie);

      writer.println(gson.toJson(requete));
      writer.flush();


      String reponse = null;

      try
      {
         reponse = reader.readLine();
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Impossible de lire la réponse du serveur : " + e.getMessage());
      }

      return reponse;
   }

   /**
    * Méthode envoyant à travers le socket la requeteServeur permettant de demander la modification d'un employe.
    *
    * @param employe L'employe avec le nouveau module
    * @return Une string valant ok si tout c'est passé correctement, une string contenant l'erreur sinon
    */
   public String modifierEmploye(Employe employe)
   {
      RequeteServeur requete = new RequeteServeur(ProtocoleWorksOn.CMD_MODIFIER_EMPLOYE, idEntreprise,
                                                  gson.toJson(employe));

      writer.println(gson.toJson(requete));
      writer.flush();

      String reponse = null;

      try
      {
         reponse = reader.readLine();
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Impossible de lire la réponse du serveur : " + e.getMessage());
      }

      return reponse;
   }

   /**
    * Méthode envoyant à travers le socket la requeteServeur permettant de demander la modification d'un projet.
    *
    * @param projet Le projet avec les nouvelles valeurs
    * @return Une string valant ok si tout c'est passé correctement, une string contenant l'erreur sinon
    */
   public String modifierProjet(Projet projet)
   {
      RequeteServeur requete = new RequeteServeur(ProtocoleWorksOn.CMD_MODIFIER_PROJET, idEntreprise,
                                                  gson.toJson(projet));

      writer.println(gson.toJson(requete));
      writer.flush();

      String reponse = null;

      try
      {
         reponse = reader.readLine();
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Impossible de lire la réponse du serveur : " + e.getMessage());
      }

      return reponse;
   }

   /**
    * Méthode envoyant à travers le socket la requeteServeur permettant de demander la modification d'un module.
    *
    * @param module Le module avec les nouvelles valeurs
    * @return Une string valant ok si tout c'est passé correctement, une string contenant l'erreur sinon
    */
   public String modifierModule(Module module)
   {
      RequeteServeur requete = new RequeteServeur(ProtocoleWorksOn.CMD_MODIFIER_MODULE, idEntreprise,
                                                  gson.toJson(module));

      writer.println(gson.toJson(requete));
      writer.flush();

      String reponse = null;

      try
      {
         reponse = reader.readLine();
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Impossible de lire la réponse du serveur : " + e.getMessage());
      }

      return reponse;
   }

   /**
    * Méthode envoyant à travers le socket la requeteServeur permettant de demander la modification d'une tâche.
    *
    * @param tache La tache avec les nouvelles valeurs
    * @return Une string valant ok si tout c'est passé correctement, une string contenant l'erreur sinon
    */
   public String modifierTache(Tache tache)
   {
      RequeteServeur requete = new RequeteServeur(ProtocoleWorksOn.CMD_MODIFIER_TACHE, idEntreprise,
                                                  gson.toJson(tache));

      writer.println(gson.toJson(requete));
      writer.flush();

      String reponse = null;

      try
      {
         reponse = reader.readLine();
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Impossible de lire la réponse du serveur : " + e.getMessage());
      }

      return reponse;
   }

   /**
    * Méthode envoyant à travers le socket la requeteServeur permettant de récupérer la liste des logs existants pour
    * une tâche donnée.
    *
    * @param idTache L'id de la tâche
    * @return La liste des logs de cette tâche
    */
   public ListeLog listeLogsTache(String idTache)
   {
      RequeteServeur requete = new RequeteServeur(ProtocoleWorksOn.CMD_GET_LOGS_TACHE, idEntreprise, idTache);

      writer.println(gson.toJson(requete));
      writer.flush();

      ListeLog reponse = null;

      try
      {
         reponse = gson.fromJson(reader.readLine(), ListeLog.class);
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Impossible de lire la réponse du serveur : " + e.getMessage());
      }

      return reponse;
   }

   /**
    * Méthode envoyant à travers le socket la requeteConnexion demandant la connexion à l'aide d'un nom d'utilisateur
    * donné et d'un mot de passe donné.
    *
    * @param username   Le nom d'utilisateur de la connexion à vérifer
    * @param motDePasse Le mot de passe de la connexion à vérifier
    * @return Un objet réponse connexion contenant soit, un token valide et l'employé connecté, soit un token invalide
    * @throws IOException Si l'ouverture du socket a échouer
    */
   public ReponseConnexion authentification(String username, String motDePasse) throws IOException
   {
      ouvrirSocket();

      RequeteConnexion requete = new RequeteConnexion(username, motDePasse);

      writer.println(gson.toJson(requete));
      writer.flush();

      ReponseConnexion reponse = null;

      try
      {
         reponse = gson.fromJson(reader.readLine(), ReponseConnexion.class);

         // Les credentials sont invalides
         if (!reponse.getToken().getToken().equalsIgnoreCase("invalide"))
         {
            this.token = reponse.getToken();
            this.idEntreprise = reponse.getEmploye().getEntreprise().getIdEntreprise();
         }
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Impossible de lire la réponse du serveur : " + e.getMessage());
      }

      return reponse;
   }

   /**
    * Méthode envoyant à travers le socket la requeteConnexion demandant la re-connexion à l'aide d'un token donné.
    *
    * @return True si la réauthentification a réussi, false sinon
    */
   public boolean authentification() throws IOException
   {
      try
      {
         ouvrirSocket();
      } catch (Exception e)
      {
         Journalisation.getERR().severe("Le serveur est introuvable : " + e.getMessage());
         throw e;
      }

      RequeteConnexion requete = new RequeteConnexion(token);

      writer.println(gson.toJson(requete));
      writer.flush();

      try
      {
         ReponseConnexion reponse = gson.fromJson(reader.readLine(), ReponseConnexion.class);

         this.token = reponse.getToken();

      } catch (IOException e)
      {
         Journalisation.getERR().severe("Impossible de lire la réponse du serveur : " + e.getMessage());
      }

      return !token.getToken().equalsIgnoreCase("invalide");
   }

   /**
    * Méthode envoyant à travers le socket la requeteServeur permettant de récupérer l'objet entier d'un état donné par
    * son id.
    *
    * @param idEtat L'id de l'état
    * @return L'objet Etat de l'état donné
    */
   public Etat etatParId(String idEtat)
   {
      RequeteServeur requete = new RequeteServeur(ProtocoleWorksOn.CMD_GET_ETAT, idEntreprise, idEtat);

      writer.println(gson.toJson(requete));
      writer.flush();

      Etat reponse = null;

      try
      {
         reponse = gson.fromJson(reader.readLine(), Etat.class);
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Impossible de lire la réponse du serveur : " + e.getMessage());
      }

      return reponse;
   }

   /**
    * Méthode envoyant à travers le socket la requeteServeur permettant de récupérer la liste des états disponibles dans
    * le système.
    *
    * @return La liste des Etats disponibles dans le système
    */
   public ListeEtats listeEtats()
   {
      RequeteServeur requete = new RequeteServeur(ProtocoleWorksOn.CMD_GET_LISTE_ETATS, idEntreprise);

      writer.println(gson.toJson(requete));
      writer.flush();

      ListeEtats reponse = null;

      try
      {
         reponse = gson.fromJson(reader.readLine(), ListeEtats.class);
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Impossible de lire la réponse du serveur : " + e.getMessage());
      }

      return reponse;
   }

   /**
    * Méthode publique permettant de fermer le socket, appelée lors du lancement du timer
    */
   public void fermerSocket() throws IOException
   {
      RequeteServeur requete = new RequeteServeur(ProtocoleWorksOn.CMD_FERMER_SOCKET, idEntreprise);

      writer.println(gson.toJson(requete));
      writer.flush();

      try
      {
         socket.close();
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Impossible de fermer le socket coté client : " + e.getMessage());
         throw e;
      }
   }

   /**
    * Méthode privée permettant d'ouvrir le socket SSL.
    *
    * @throws IOException Si l'ouverture du socket à échouée
    */
   private void ouvrirSocket() throws IOException
   {
      SocketFactory socketFactory = SSLSocketFactory.getDefault();
      try
      {
         socket = socketFactory.createSocket(ProtocoleWorksOn.DEFAULT_HOST, ProtocoleWorksOn.DEFAULT_PORT);
         reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
         writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
      } catch (IOException e)
      {
         Journalisation.getERR().severe("La création du socket client a échoué : " + e.getMessage());
         throw e;
      }
   }
}
