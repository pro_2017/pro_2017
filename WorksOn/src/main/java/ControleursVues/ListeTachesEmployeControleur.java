package ControleursVues;

import Controleur.Controleur;
import Enums.Priorite;
import Objets.*;
import Utilitaires.Journalisation;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Contrôleur de la vue ListeTachesEmploye.fxml
 * Ce contrôleur regroupe toutes les actions qu'il est possible de faire sur cette vue.
 */
public class ListeTachesEmployeControleur
{
   @FXML
   public Button
         boutonAjouterTache,
         boutonRetour;

   @FXML
   public TableView<Tache> tableauTaches;

   @FXML
   private TableColumn
         colIdTache,
         colResponsable,
         colNomTache,
         colPrioriteTache,
         colEtatTache,
         colDureeEstimeeTache,
         colDureeEffectiveTache;

   @FXML
   private Label
         labelNomProjet,
         labelNomModule,
         labelManager,
         labelEtat,
         labelPriorite,
         labelDateRendu,
         labelErreur;

   // Membres non-FXML
   private Controleur controleur;
   private ListeTaches taches;
   private Integer idEmploye;
   private Integer idModule;

   /**
    * Méthode appelée lors d'un changement de vue.
    * Initialise les données de la vue.
    *
    * @param controleur Controleur principal communiquant avec le serveur.
    */
   public void setDonnees(Controleur controleur)
   {
      this.controleur = controleur;

      idEmploye = controleur.getEmploye().getId();
      idModule = controleur.getModule().getId();

      labelNomProjet.setText(controleur.getProjet().getNom());
      labelManager.setText(controleur.getProjet().getChefProjet().getPrenom() + " " + controleur.getProjet().getChefProjet().getNom());
      labelNomModule.setText(controleur.getModule().getNom());
      labelEtat.setText(controleur.getModule().getEtat().getNom());
      labelPriorite.setText(controleur.getModule().getPriorite().toString());
      labelDateRendu.setText(new SimpleDateFormat("yyyy-MM-dd").format(controleur.getModule().getDeadline()));
      labelErreur.setText("");

      // Demande de la liste des projets au controleur
      taches = controleur.listeTachesUtilisateurSurModule(idEmploye.toString(), idModule.toString());


      ObservableList<Tache> oListeTaches = FXCollections.observableArrayList(taches.getListeTaches());


      colIdTache.setCellValueFactory(new PropertyValueFactory<Tache, String>("id"));
      colResponsable.setCellValueFactory(
            new Callback<TableColumn.CellDataFeatures<Tache, String>, ObservableValue<String>>()
            {
               @Override
               public ObservableValue<String> call(TableColumn.CellDataFeatures<Tache, String> tache)
               {
                  SimpleStringProperty property = new SimpleStringProperty();
                  property.setValue(tache.getValue().getEmploye().getNom() + " " + tache.getValue().getEmploye().getPrenom());
                  return property;
               }
            });

      colNomTache.setCellValueFactory(new PropertyValueFactory<Tache, String>("nom"));
      colPrioriteTache.setCellValueFactory(new PropertyValueFactory<Tache, Priorite>("priorite"));
      colEtatTache.setCellValueFactory(new PropertyValueFactory<Tache, Etat>("etat"));
      colDureeEstimeeTache.setCellValueFactory(
            new Callback<TableColumn.CellDataFeatures<Tache, String>, ObservableValue<String>>()
            {
               @Override
               public ObservableValue<String> call(TableColumn.CellDataFeatures<Tache, String> tache)
               {
                  SimpleStringProperty property = new SimpleStringProperty();
                  DateFormat dateFormat = new SimpleDateFormat("HH:mm");
                  property.setValue(dateFormat.format(tache.getValue().getDureePlanifiee()));
                  return property;
               }
            });

      colDureeEffectiveTache.setCellValueFactory(
            new Callback<TableColumn.CellDataFeatures<Tache, String>, ObservableValue<String>>()
            {
               @Override
               public ObservableValue<String> call(TableColumn.CellDataFeatures<Tache, String> tache)
               {
                  SimpleStringProperty property = new SimpleStringProperty();
                  DateFormat dateFormat = new SimpleDateFormat("HH:mm");
                  property.setValue(dateFormat.format(tache.getValue().getDureeEffective()));
                  return property;
               }
            });

      tableauTaches.setItems(oListeTaches);
   }

   /**
    * Action lors du double-clique sur une cellule.
    *
    * @param event Événement souris
    */
   @FXML
   public void clickCellule(MouseEvent event)
   {
      if (event.getClickCount() == 2) //Checking double click
      {
         Integer tmp = tableauTaches.getSelectionModel().getSelectedItem().getId();
         Tache tacheCourante = controleur.tacheParId(tmp.toString());
         controleur.setTache(tacheCourante);

         if (tacheCourante.getEmploye().getId() != idEmploye)
         {
            labelErreur.setText("Vous ne pouvez pas travailler sur une tâche qui n'est pas la votre");
            return;
         }

         Parent tacheSelectionee = null;
         FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Vues/TimerTache.fxml")); // Version Jar

         try
         {
            tacheSelectionee = (Parent) fxmlLoader.load(); // Version Jar
         } catch (IOException e)
         {
            Journalisation.getERR().severe("Erreur lors du changement vers la fenêtre Timer ." + e.getMessage());
         }

         Stage sceneActuelle = (Stage) labelNomProjet.getScene().getWindow();
         sceneActuelle.setScene(new Scene(tacheSelectionee));
         sceneActuelle.setResizable(false);
         sceneActuelle.show();

         TimerTacheControleur controleurTimerTacheControleur = (TimerTacheControleur) fxmlLoader.getController();
         controleurTimerTacheControleur.setDonnees(controleur, tacheCourante);
      }
   }

   /**
    * Action du bouton "Ajouter tâche"
    * Changement de vue vers NouvelleTacheControleur.
    */
   @FXML
   public void ajouterTache()
   {
      if (controleur.getModule() == null)
      {
         Journalisation.getERR().severe("Impossible d'ajouter une tâche à un module innexistant");
      }
      else
      {
         Parent nouvelleTache = null;
         FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Vues/NouvelleTache.fxml")); // Version Jar

         try
         {
            nouvelleTache = (Parent) fxmlLoader.load(); // Version Jar
         } catch (IOException e)
         {
            Journalisation.getERR().severe("Erreur lors du changement vers la fenêtre création tâche."
                                                 + e.getMessage());
         }

         Stage sceneActuelle = (Stage) labelNomProjet.getScene().getWindow();
         sceneActuelle.setScene(new Scene(nouvelleTache));
         sceneActuelle.setResizable(false);
         sceneActuelle.show();

         NouvelleTacheControleur controleurCreationTache = (NouvelleTacheControleur) fxmlLoader.getController();
         controleurCreationTache.setDonnees(controleur);
      }
   }

   /**
    * Changement de vue
    * Retour sur la liste des projets de l'employe.
    */
   @FXML
   public void retour()
   {
      controleur.setProjet(null);
      controleur.setModule(null);
      controleur.setTache(null);

      Parent listeProjet = null;
      FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Vues/ListeProjetsEmploye.fxml")); // Version Jar

      try
      {
         listeProjet = (Parent) fxmlLoader.load(); // Version Jar
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Erreur lors du changement vers la fenêtre de liste projet utilisateur."
                                              + e.getMessage());
      }

      Stage sceneActuelle = (Stage) labelNomProjet.getScene().getWindow();
      sceneActuelle.setScene(new Scene(listeProjet));
      sceneActuelle.setResizable(false);
      sceneActuelle.show();

      ListeProjetsEmployeControleur controleurListeProjet = (ListeProjetsEmployeControleur) fxmlLoader.getController();
      controleurListeProjet.setDonnees(controleur);
   }
}
