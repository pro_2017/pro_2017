package ControleursVues;

import Controleur.Controleur;
import Enums.*;
import Objets.*;

import Protocoles.ProtocoleWorksOn;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Contrôleur de la vue MenuAdmin.fxml
 * Ce contrôleur regroupe toutes les actions qu'il est possible de faire sur cette vue.
 */
public class MenuAdminControleur
{
   @FXML
   private TextField
         nomEmploye,
         prenomEmploye,
         loginEmploye,
         mdpEmploye;

   @FXML
   private ComboBox<Role> roleEmploye;

   @FXML
   private Label erreur;

   @FXML
   private TextField
         filtreProjets,
         filtreUtilisateurs;

   @FXML
   private TableView<Employe> tableUtilisateurs;

   @FXML
   private TableView<Projet> tableProjets;

   @FXML
   private TableColumn
         colId,
         colNom,
         colPrenom,
         colUsername,
         colRole;

   @FXML
   private TableColumn
         colIdProjet,
         colNomProjet,
         colDateDebut,
         colDateLimite,
         colStatus;

   // Membre non-FXML
   private Controleur controleur;

   /**
    * Action du bouton "Ajouter"
    * Ajoute un employe dan sla base de données selon les champs renseignés.
    */
   @FXML
   public void boutonAjouterAppuye()
   {
      // Récupération des champs saisi
      String nomSaisi = nomEmploye.getText();
      String prenomSaisi = prenomEmploye.getText();
      String loginSaisi = loginEmploye.getText();
      String mdpSaisi = mdpEmploye.getText();
      Role roleSaisi = roleEmploye.getSelectionModel().getSelectedItem();

      // Si un des champs est vide
      if (nomSaisi.equalsIgnoreCase("") || prenomSaisi.equalsIgnoreCase("") ||
            loginSaisi.equalsIgnoreCase("") || mdpSaisi.equalsIgnoreCase(""))
      {
         erreur.setText("Merci de remplir les champs ci-dessus");
         return;
      }

      // On envoie le nouvel employé
      String reponse = controleur.creerEmploye(nomSaisi, prenomSaisi, loginSaisi, mdpSaisi, roleSaisi);

      if (!reponse.equalsIgnoreCase(ProtocoleWorksOn.RSP_OK))
      {
         erreur.setText(reponse + "\nRappel : le nom d'utilisateur doit être unique.");
         return;
      }

      erreur.setText("Ajout réussi");
      nomEmploye.clear();
      prenomEmploye.clear();
      loginEmploye.clear();
      mdpEmploye.clear();
      roleEmploye.getSelectionModel().selectFirst();

      // On refresh les listes
      setDonnees(controleur);
   }

   @FXML
   public void saisieEnCours()
   {
      erreur.setText("");
   }

   /**
    * Fonction appellée à l'initialisation de la fenêtre
    *
    * @param controleur Le controleur principal passé par la fenetre précédente
    */
   public void setDonnees(Controleur controleur)
   {
      this.controleur = controleur;

      // Récupération des listes sur le serveur
      ListeEmployes listeEmployes = controleur.listeEmployes();
      ListeProjets listeProjets = controleur.listeProjets();

      // Création des observableList grâce aux liste contenues dans nos objets
      ObservableList<Employe> oListeEmployes = FXCollections.observableArrayList(listeEmployes.getListeEmployes());
      ObservableList<Projet> oListeProjets = FXCollections.observableArrayList(listeProjets.getListeProjets());

      // Initialisation des colonnes de la table qui contient les employes
      colId.setCellValueFactory(new PropertyValueFactory<Employe, String>("id"));
      colNom.setCellValueFactory(new PropertyValueFactory<Employe, String>("nom"));
      colPrenom.setCellValueFactory(new PropertyValueFactory<Employe, String>("prenom"));
      colUsername.setCellValueFactory(new PropertyValueFactory<Employe, String>("nomUtilisateur"));
      colRole.setCellValueFactory(new PropertyValueFactory<Employe, Role>("role"));

      // Initialisation des colonnes de la table qui contient les projets
      colIdProjet.setCellValueFactory(new PropertyValueFactory<Projet, String>("id"));
      colNomProjet.setCellValueFactory(new PropertyValueFactory<Projet, String>("nom"));
      colStatus.setCellValueFactory(new PropertyValueFactory<Projet, Etat>("etat"));

      // Pour les dates, on fait une petite classe anonyme pour les faire apparaitre proprement
      colDateDebut.setCellValueFactory(
            new Callback<TableColumn.CellDataFeatures<Projet, String>, ObservableValue<String>>()
            {
               @Override
               public ObservableValue<String> call(TableColumn.CellDataFeatures<Projet, String> projet)
               {
                  SimpleStringProperty property = new SimpleStringProperty();
                  DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                  property.setValue(dateFormat.format(projet.getValue().getDateDepart()));
                  return property;
               }
            });
      colDateLimite.setCellValueFactory(
            new Callback<TableColumn.CellDataFeatures<Projet, String>, ObservableValue<String>>()
            {
               @Override
               public ObservableValue<String> call(TableColumn.CellDataFeatures<Projet, String> projet)
               {
                  SimpleStringProperty property = new SimpleStringProperty();
                  DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                  property.setValue(dateFormat.format(projet.getValue().getDateRendu()));
                  return property;
               }
            });

      // Pour le fonctionnement des filtres vous pouvez allez voir ici : 
      // http://code.makery.ch/blog/javafx-8-tableview-sorting-filtering/
      // On lie le filtre (le champ) à la table et on règle le fonctionnement du filtre
      // Attention lambdas expressions

      // Liste qui contiendra les employes filtrés
      FilteredList<Employe> employesFiltres = new FilteredList<>(oListeEmployes, p -> true);
      filtreUtilisateurs.textProperty().addListener((observable, ancienneValeur, nouvelleValeur) -> {
         employesFiltres.setPredicate(employe -> {
            if (nouvelleValeur == null || nouvelleValeur.isEmpty())
            {
               return true;
            }

            // NouvelleValeur correspond à la string qui est dans le textField
            String filtreLowerCase = nouvelleValeur.toLowerCase();

            // Donc on vérifie et ajoute les champs qu'on veut contenir dans le filtre
            if (employe.getNom().toLowerCase().contains(filtreLowerCase))
            {
               return true;
            }
            else if (employe.getPrenom().toLowerCase().contains(filtreLowerCase))
            {
               return true;
            }
            else if (String.valueOf(employe.getId()).contains(filtreLowerCase))
            {
               return true;
            }
            else if (employe.getNomUtilisateur().toLowerCase().contains(filtreLowerCase))
            {
               return true;
            }
            else if (employe.getRole().toString().toLowerCase().contains(filtreLowerCase))
            {
               return true;
            }

            return false;
         });
      });

      // On crée la liste triées des employes filtrés
      SortedList<Employe> employesFiltresTries = new SortedList<>(employesFiltres);
      // On bind cette liste dans la table
      employesFiltresTries.comparatorProperty().bind(tableUtilisateurs.comparatorProperty());
      tableUtilisateurs.setItems(employesFiltresTries);

      // Pareil qu'avec les employes mais avec les projets
      FilteredList<Projet> projetsFiltres = new FilteredList<>(oListeProjets, p -> true);
      filtreProjets.textProperty().addListener((observable, ancienneValeur, nouvelleValeur) -> {
         projetsFiltres.setPredicate(projet -> {
            if (nouvelleValeur == null || nouvelleValeur.isEmpty())
            {
               return true;
            }

            String filtreLowerCase = nouvelleValeur.toLowerCase();

            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

            if (projet.getNom().toLowerCase().contains(filtreLowerCase))
            {
               return true;
            }
            else if (String.valueOf(projet.getId()).contains(filtreLowerCase))
            {
               return true;
            }
            else if (projet.getEtat().toString().toLowerCase().contains(filtreLowerCase))
            {
               return true;
            }
            else if (dateFormat.format(projet.getDateRendu()).toLowerCase().contains(filtreLowerCase))
            {
               return true;
            }
            else if (dateFormat.format(projet.getDateDepart()).toLowerCase().contains(filtreLowerCase))
            {
               return true;
            }

            return false;
         });
      });

      SortedList<Projet> projetsFiltresTries = new SortedList<>(projetsFiltres);
      projetsFiltresTries.comparatorProperty().bind(tableProjets.comparatorProperty());
      tableProjets.setItems(projetsFiltresTries);

      //Initialisation de la comboBox avec les rôles à choix
      for (Role role : Role.values())
      {
         if (role != Role.ADMINISTRATEUR)
         {
            roleEmploye.getItems().add(role);
         }
      }

      roleEmploye.getSelectionModel().selectFirst();
   }
}
