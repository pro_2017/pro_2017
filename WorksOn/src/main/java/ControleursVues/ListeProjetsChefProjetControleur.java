package ControleursVues;

import Controleur.Controleur;
import Objets.*;
import Enums.Priorite;
import Utilitaires.Journalisation;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.text.SimpleDateFormat;

/**
 * Contrôleur de la vue ListeProjetsChefProjetControleur.fxml
 * Ce contrôleur regroupe toutes les actions qu'il est possible de faire sur cette vue.
 */
public class ListeProjetsChefProjetControleur
{

   @FXML
   private Label lbl_nomEntreprise;
   @FXML
   private Label lbl_nomProjet;
   @FXML
   private Label lbl_nomModule;
   @FXML
   private Label lbl_dateLimite;
   @FXML
   private Label lbl_dateLimiteModule;
   @FXML
   private Label lbl_erreur;
   @FXML
   private TextArea txta_descriProjet;
   @FXML
   private TextArea txta_descriModule;
   @FXML
   private ListView<Projet> lst_projets;
   @FXML
   private ListView<Module> lst_modules;
   @FXML
   private ListView<Employe> lst_utilisateurs;
   @FXML
   private ChoiceBox<Etat> drop_etatProjet;
   @FXML
   private ChoiceBox<Etat> drop_etatModule;
   @FXML
   private ChoiceBox<Priorite> drop_prioriteModule;
   @FXML
   private AnchorPane panneauModule;

   // Membre non FXML
   private Controleur controleur;
   private ListeEtats etatsPossibles;
   private ListeProjets projets;
   private ListeModules modules;
   private ListeEmployes utilisateurs;
   private Projet projetCourant;
   private Module moduleCourant;

   // Données qui seront utilisées par les "view list" et les "choice list".
   private ObservableList<Projet> olst_projets;
   private ObservableList<Module> olst_modules;
   private ObservableList<Employe> olst_employes;
   private ObservableList<Etat> olst_etats;

   /**
    * Méthode appelée lors d'un changement de vue.
    * Initialise les données de la vue.
    *
    * @param controleur Controleur principal communiquant avec le serveur.
    */
   public void setDonnees(Controleur controleur)
   {
      this.controleur = controleur;

      lbl_erreur.setText("");
      lbl_nomEntreprise.setText(controleur.getEntreprise().getNom());

      // Récupération des laliste des états
      etatsPossibles = controleur.listeEtats();
      if (etatsPossibles == null)
      {
         lbl_erreur.setText("Erreur critique : La liste des états est vide.\nVeuillez contacter votre administrateur.");

      }
      else
      {
         // construction des listes d'états possibles
         olst_etats = FXCollections.observableArrayList(etatsPossibles.getListeEtats());
         drop_etatProjet.setItems(olst_etats);
         drop_etatModule.setItems(olst_etats);
      }

      // Construction de la liste des priorites possibles
      drop_prioriteModule.getItems().setAll(Priorite.HAUTE, Priorite.MOYENNE, Priorite.BASSE);

      // Demande de la liste des projets au controleur
      projets = controleur.listeProjetsChef(((Integer) controleur.getEmploye().getId()).toString());
      if (projets == null)
      {
         lbl_erreur.setText("Erreur : La liste de projet est vide");
      }
      else
      {
         // Transformation de la liste pour affichage sous forme de view list
         olst_projets = FXCollections.observableArrayList(projets.getListeProjets());
         lst_projets.setItems(olst_projets);
         lst_projets.getSelectionModel().selectFirst();
         selectionProjet();
      }
   }

   /**
    * Action lorsque qu'un projet est selectionné.
    */
   @FXML
   public void selectionProjet()
   {
      Integer IndexProjet = lst_projets.getSelectionModel().getSelectedIndex();
      lbl_erreur.setText("");

      if (IndexProjet != -1)
      {
         Integer IDProjet = lst_projets.getSelectionModel().getSelectedItem().getId();

         // Récupération du projet.
         projetCourant = controleur.projetParId(IDProjet.toString());

         if (projetCourant != null)
         {

            // Affichage du nom du projet.
            lbl_nomProjet.setText(projetCourant.getNom());

            // Affichage de l'état.
            Etat aAfficher = projetCourant.getEtat();
            Etat oLstCorrespondant = null;
            for (Etat e : olst_etats)
            {
               if (e.getId() == aAfficher.getId())
               {
                  oLstCorrespondant = e;
               }
            }
            drop_etatProjet.getSelectionModel().select(oLstCorrespondant);

            // Affichage de la description
            txta_descriProjet.setText(projetCourant.getDescription());

            // Affichage de la date limite.
            lbl_dateLimite.setText(projetCourant.getDateRendu().toLocalDateTime().toLocalDate().toString());

            // Demande de la liste des modules au controleur
            modules = controleur.listeModulesProjets(IDProjet.toString());

            if (!modules.getListeModules().isEmpty())
            {
               // Affichage des modules.
               panneauModule.setOpacity(1);
               olst_modules = FXCollections.observableArrayList(modules.getListeModules());
               lst_modules.setItems(olst_modules);
               lst_modules.getSelectionModel().selectFirst();
               selectionModule();
            }
            else
            {
               // Liste est détail des modules sont cachés.
               lbl_erreur.setText("Il n'y a pas de module pour ce projet.");
               panneauModule.setOpacity(0);
               olst_modules = FXCollections.observableArrayList(modules.getListeModules());
               lst_modules.setItems(olst_modules);
            }
         }
      }
   }

   /**
    * Action lorsqu'un module est selectionné.
    */
   @FXML
   public void selectionModule()
   {
      Integer IndexModule = lst_modules.getSelectionModel().getSelectedIndex();
      lbl_erreur.setText("");

      if (IndexModule != -1)
      {
         Integer IDModule = lst_modules.getSelectionModel().getSelectedItem().getId();

         // Récupération du module.
         moduleCourant = controleur.moduleParId(IDModule.toString());

         if (moduleCourant != null)
         {
            // Affichage nom et description du module.
            lbl_nomModule.setText(moduleCourant.getNom());
            txta_descriModule.setText(moduleCourant.getDescription());

            // Affichage de l'état.
            Etat aAfficher = moduleCourant.getEtat();
            Etat oLstCorrespondant = null;
            for (Etat e : olst_etats)
            {
               if (e.getId() == aAfficher.getId())
               {
                  oLstCorrespondant = e;
               }
            }
            drop_etatModule.getSelectionModel().select(oLstCorrespondant);

            // Affichage prioritéé et date limite.
            drop_prioriteModule.setValue(moduleCourant.getPriorite());
            lbl_dateLimiteModule.setText(new SimpleDateFormat("yyyy-MM-dd").format(moduleCourant.getDeadline()));

            // Affichage des employés sur le projet
            olst_employes = FXCollections.observableArrayList(moduleCourant.getListeEmployes());
            lst_utilisateurs.setItems(olst_employes);
            lst_utilisateurs.setEditable(false);
         }
      }
   }

   /**
    * Action du bouton "Ajouter projet"
    * Changement de vue vers NouveauProjetControleur.
    */
   @FXML
   public void ajouterProjet()
   {
      Parent nouveauProjet = null;
      FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Vues/NouveauProjet.fxml")); // Version Jar

      try
      {
         nouveauProjet = (Parent) fxmlLoader.load(); // Version Jar
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Erreur lors du changement vers la fenêtre nouveau projet."
                                              + e.getMessage());
      }

      Stage sceneActuelle = (Stage) lbl_nomProjet.getScene().getWindow();
      sceneActuelle.setScene(new Scene(nouveauProjet));
      sceneActuelle.setResizable(false);
      sceneActuelle.show();

      NouveauProjetControleur controleurNouveauProjetControleur = (NouveauProjetControleur) fxmlLoader.getController();
      controleurNouveauProjetControleur.setDonnees(controleur);
   }

   /**
    * Action du bouton "Ajouter module"
    * Changement de vue vers NouveauModuleControleur.
    */
   @FXML
   public void ajouterModule()
   {
      if (projetCourant == null)
      {
         lbl_erreur.setText("Veuillez selectionner un projet avant d'y ajouter un module");
      }
      else
      {
         Parent nouveauModule = null;
         FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Vues/NouveauModule.fxml"));

         try
         {
            nouveauModule = (Parent) fxmlLoader.load(); // Version Jar
         } catch (IOException e)
         {
            Journalisation.getERR().severe("Erreur lors du changement vers la fenêtre nouveau module."
                                                 + e.getMessage());
         }

         Stage sceneActuelle = (Stage) lbl_nomProjet.getScene().getWindow();
         sceneActuelle.setScene(new Scene(nouveauModule));
         sceneActuelle.setResizable(false);
         sceneActuelle.show();

         NouveauModuleControleur controleurNouveauModuleControleur = (NouveauModuleControleur) fxmlLoader.getController();
         controleurNouveauModuleControleur.setDonnees(controleur, projetCourant);
      }
   }

   /**
    * Action du bouton "Ajouter utilisateur(s)"
    * Changement de vue vers AjouterUtilisateursModule.
    */
   @FXML
   void ajouterUtilisateurs()
   {
      Parent usr = null;
      FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Vues/AjoutUtilisateurModule.fxml"));

      try
      {
         usr = (Parent) fxmlLoader.load(); // Version Jar
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Erreur lors du changement vers la fenêtre ajouter utilisateurs au module."
                                              + e.getMessage());
      }

      Stage sceneActuelle = (Stage) lbl_nomProjet.getScene().getWindow();
      sceneActuelle.setScene(new Scene(usr));
      sceneActuelle.setResizable(false);
      sceneActuelle.show();

      AjoutUtilisateurModuleControleur controleurNouveauModule = (AjoutUtilisateurModuleControleur) fxmlLoader.getController();
      controleurNouveauModule.setDonnees(controleur, moduleCourant);
   }

   /**
    * Action du bouton "Mise à jour projet"
    * Réenvoie le projet avec les nouvelles données à la base de données.
    */
   @FXML
   public void miseAJourProjet()
   {
      if (projetCourant != null)
      {
         String err;
         err = controleur.modifierProjet(projetCourant, drop_etatProjet.getSelectionModel().getSelectedItem(),
                                         projetCourant.getDateDepart().toLocalDateTime().toLocalDate(),
                                         txta_descriProjet.getText());
         if (err == null)
         {
            lbl_erreur.setText("Mise à jour du projet réussie.");
         }
         else
         {

            lbl_erreur.setText(err);
         }
      }
   }

   /**
    * Action du bouton "Mise à jour module"
    * Réenvoie le module avec les nouvelles données à la base de données.
    */
   @FXML
   public void miseAjourModule()
   {
      if (moduleCourant != null)
      {
         String err = null;
         controleur.modifierModule(moduleCourant, txta_descriModule.getText(),
                                   drop_etatModule.getSelectionModel().getSelectedItem(),
                                   drop_prioriteModule.getValue());

         if (err == null)
         {
            // lbl_erreur.setTextFill(Color.color(0, 0xC, 0));
            lbl_erreur.setText("Mise à jour du module réussie.");
         }
         else
         {
            lbl_erreur.setText("Erreur lors de la mise à jour du module.");
         }
      }
   }

   /**
    * Action du bouton "Lister tâche"
    * Changement de vue vers ListeTachesChefProjetControleur.
    */
   @FXML
   public void listerTaches()
   {
      Parent taches = null;
      FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Vues/ListeTachesChefProjet.fxml"));

      try
      {
         taches = (Parent) fxmlLoader.load(); // Version Jar
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Erreur lors du changement vers la fenêtre liste taches chef."
                                              + e.getMessage());
      }

      Stage sceneActuelle = (Stage) lbl_nomProjet.getScene().getWindow();
      sceneActuelle.setScene(new Scene(taches));
      sceneActuelle.setResizable(false);
      sceneActuelle.show();

      ListeTachesChefProjetControleur controleurTaches = (ListeTachesChefProjetControleur) fxmlLoader.getController();
      controleur.setProjet(projetCourant);
      controleur.setModule(moduleCourant);
      controleurTaches.setDonnees(controleur);
   }
}
