package ControleursVues;

import Controleur.Controleur;
import Objets.*;
import Protocoles.ProtocoleWorksOn;
import Utilitaires.Journalisation;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.IOException;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;

/**
 * Contrôleur de la vue TimerTache.fxml
 * Ce contrôleur regroupe toutes les actions qu'il est possible de faire sur cette vue.
 */
public class TimerTacheControleur
{
   @FXML
   private Label
         labelNomTache,
         labelDureeEstimee,
         labelDureeEcoulee,
         labelTimer,
         labelPriorite,
         labelErreur;

   @FXML
   private Button
         boutonEnregistrerTimer,
         boutonPauseTimer,
         boutonDemarrerTimer;

   // Membres non-FXML
   private Controleur controleur;
   private Tache tache;
   private Log log;
   private Timeline timeline;
   private int nbHeures;
   private int nbMinutes;
   private int nbSecondes;

   /**
    * Action du bouton "Enregistrer"
    * Calcule le temps passé sur la tache et met à jour le temps effectif. Ouvre la vue de remarques.
    */
   @FXML
   public void enregistrerTimer()
   {
      timeline.stop();

      long debut = log.getDebut().getTime();
      long fin = debut;
      fin += nbSecondes * 1000;
      fin += nbMinutes * 60 * 1000;
      fin += nbHeures * 60 * 60 * 1000;

      log.setFin(new Timestamp(fin));

      afficherRemarques();
   }

   /**
    * Action du bouton "Pause" ou "Play"
    * Met le timer en pause s'il run, le redémarre si il est en pause.
    */
   @FXML
   public void pauseTimer()
   {
      if (timeline.getStatus() == Animation.Status.RUNNING)
      {
         timeline.pause();
         boutonPauseTimer.setText("Play");
      }
      else
      {
         timeline.play();
         boutonPauseTimer.setText("Pause");
      }
   }

   /**
    * Action du bouton "Démarrer"
    * Commence le décompte du temps.
    */
   @FXML
   public void demarrerTimer()
   {
      // On ferme la connexion, l'utilisateur a commencé à travailler
      if (!controleur.fermerConnexion().equalsIgnoreCase(ProtocoleWorksOn.RSP_OK))
      {
         labelErreur.setText("Erreur lors de la fermeture du socket");
         return;
      }

      boutonPauseTimer.setDisable(false);
      boutonDemarrerTimer.setDisable(true);
      boutonEnregistrerTimer.setDisable(false);

      // Mettre le début dans le log
      log.setDebut(new Timestamp(Calendar.getInstance().getTime().getTime()));

      // Lancer le timer
      creationTimeLine();
      timeline.playFromStart();
   }

   /**
    * Méthode appelée lors d'un changement de vue.
    * Initialise les données de la vue.
    *
    * @param controleur Controleur principal communiquant avec le serveur.
    * @param tache      Tache actuellement modifiée.
    */
   public void setDonnees(Controleur controleur, Tache tache)
   {
      this.controleur = controleur;
      this.tache = tache;
      this.nbHeures = 0;
      this.nbMinutes = 0;
      this.nbSecondes = 0;
      this.log = new Log();

      // On set les champs avec la tâche qu'on a recue
      labelErreur.setText("");

      if (tache != null)
      {
         labelDureeEcoulee.setText(tache.getDureeEffective().toString());
         labelDureeEstimee.setText(tache.getDureePlanifiee().toString());
         labelNomTache.setText(tache.getNom());
         labelPriorite.setText(tache.getPriorite().toString());
         log.setTache(tache);
      }
      else
      {
         labelErreur.setText("La tâche reçue n'est pas une tâche valide");
         boutonDemarrerTimer.setDisable(true);
      }

      // Tant que le timer n'a pas démarrer, on ne peut pas enregistrer ni pause
      boutonEnregistrerTimer.setDisable(true);
      boutonPauseTimer.setDisable(true);
   }

   /**
    * Changement de vue
    * Ouvre la vue d'enregistrement des remarques.
    */
   private void afficherRemarques()
   {
      Parent fenetreRemarques = null;
      FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Vues/TacheRemarques.fxml")); // Version Jar

      try
      {
         fenetreRemarques = (Parent) fxmlLoader.load(); // Version Jar
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Erreur lors du changement vers la fenêtre remarques tâche."
                                              + e.getMessage());
      }

      Stage sceneActuelle = (Stage) labelDureeEcoulee.getScene().getWindow();
      sceneActuelle.setScene(new Scene(fenetreRemarques));
      sceneActuelle.setResizable(false);
      sceneActuelle.show();

      TacheRemarquesControleur controleurFenetreRemarques = (TacheRemarquesControleur) fxmlLoader.getController();
      controleurFenetreRemarques.setDonnees(controleur, log);
   }

   /**
    * Méthode permettant de créer une timeline pour gérer le temps du compteur. Cette timeline permet d'avoir un thread
    * parallèle dédié au timer qui fait des actions en fonction de certains évenements (en l'occurance ici le passage
    * d'une seconde).
    */
   private void creationTimeLine()
   {
      // Plus d'information sur la timeLine ici.
      // http://asgteach.com/2011/10/javafx-animation-and-binding-simple-countdown-timer-2/
      timeline = new Timeline(
            new KeyFrame(
                  Duration.seconds(0), // Si une une seconde est passée pour la timeline
                  actionEvent ->
                  {
                     nbSecondes++;
                     if (nbSecondes == 60)
                     {
                        nbSecondes = 0;
                        nbMinutes++;

                        if (nbMinutes == 60)
                        {
                           nbMinutes = 0;
                           nbHeures++;
                        }
                     }
                     labelTimer.setText(String.format("%02d", nbHeures) + ":" +
                                              String.format("%02d", nbMinutes) + ":" + String.format("%02d", nbSecondes));
                  }
            ),
            new KeyFrame(Duration.seconds(1))
      );

      // On ne sait pas combien de temps la timeline doit continuer donc on set le nombre de cycle à indéfini
      timeline.setCycleCount(Animation.INDEFINITE);
   }
}
