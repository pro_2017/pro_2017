package ControleursVues;

import Controleur.Controleur;
import Protocoles.ProtocoleWorksOn;
import Objets.Etat;
import Utilitaires.Journalisation;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.time.LocalDate;

/**
 * Contrôleur de la vue NouveauProjet.fxml
 * Ce contrôleur regroupe toutes les actions qu'il est possible de faire sur cette vue.
 */
public class NouveauProjetControleur
{
   @FXML
   private TextField txt_nomNouveauProjet;
   @FXML
   private DatePicker dtp_dateNouveauProjet;
   @FXML
   private TextField txta_descriNouveauProjet;
   @FXML
   private Label lbl_err;

   // Membres non-FXML
   private Controleur controleur;

   /**
    * Méthode appelée lors d'un changement de vue.
    * Initialise les données de la vue.
    *
    * @param c Controleur principal communiquant avec le serveur.
    */
   public void setDonnees(Controleur c)
   {
      this.controleur = c;
      lbl_err.setText("");
   }

   /**
    * Action du bouton "Créer projet"
    * Ajoute le nouveau projet dans la base de donnée.
    */
   @FXML
   public void creerNouveauProjet()
   {
      String err;

      if (txt_nomNouveauProjet.getText().equalsIgnoreCase("") || dtp_dateNouveauProjet.getValue() == null)
      {
         lbl_err.setText("Merci de remplir les champs nom et date ci-dessus");
         return;
      }

      LocalDate dateFin = dtp_dateNouveauProjet.getValue();
      err = controleur.creerProjet(txt_nomNouveauProjet.getText(), new Etat(1, "En cours"),
                                   txta_descriNouveauProjet.getText(), dateFin);
      if (!err.equals(ProtocoleWorksOn.RSP_OK))
      {
         lbl_err.setText(err);
      }
      else
      {
         retourListeProjetChefProjet();
      }
   }

   /**
    * Action du bouton "Annuler"
    * Retour sur la liste des projets du chef de projet.
    */
   @FXML
   public void annulerNouveauProjet()
   {
      retourListeProjetChefProjet();
   }


   @FXML
   public void saisieEnCours()
   {
      lbl_err.setText("");
   }

   /**
    * Changement de vue
    * Retour sur la liste des projets du chef de projet.
    */
   public void retourListeProjetChefProjet()
   {
      Parent chef = null;
      FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Vues/ListeProjetsChefProjet.fxml")); // Version Jar

      try
      {
         chef = (Parent) fxmlLoader.load(); // Version Jar
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Erreur lors du changement vers la fenêtre chef depuis nouveau projet: "
                                              + e.getMessage());
      }

      Stage sceneActuelle = (Stage) txt_nomNouveauProjet.getScene().getWindow();
      sceneActuelle.setScene(new Scene(chef));
      sceneActuelle.setResizable(false);
      sceneActuelle.show();

      ListeProjetsChefProjetControleur controleurChef = (ListeProjetsChefProjetControleur) fxmlLoader.getController();
      controleurChef.setDonnees(controleur);
   }
}
