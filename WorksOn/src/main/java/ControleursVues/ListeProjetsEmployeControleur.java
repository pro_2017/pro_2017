package ControleursVues;

import Controleur.Controleur;
import Enums.Priorite;
import Objets.*;
import Utilitaires.Journalisation;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.*;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Contrôleur de la vue ListeProjetsEmploye.fxml
 * Ce contrôleur regroupe toutes les actions qu'il est possible de faire sur cette vue.
 */
public class ListeProjetsEmployeControleur
{
   @FXML
   public TextField textProjetDescription;

   @FXML
   public TableView<Module> tableauModule;

   @FXML
   public ListView listeProjets;

   @FXML
   public SplitPane
         splitPane1,
         splitPane2,
         splitPane3;

   @FXML
   private Label
         labelDebutProjet,
         labelFinProjet,
         labelNomProjet,
         labelChefProjet,
         labelErreur;

   @FXML
   private TableColumn
         colIdModule,
         colNomModule,
         colPrioriteModule,
         colEtatModule,
         colDateFinModule;

   // Membre non-FXML
   private Controleur controleur;
   private ListeProjets projets;
   private Projet projetCourant;
   private Module moduleCourant;

   /**
    * Méthode appelée lors d'un changement de vue.
    * Initialise les données de la vue.
    *
    * @param controleur Controleur principal communiquant avec le serveur.
    */
   public void setDonnees(Controleur controleur)
   {
      this.controleur = controleur;
      controleur.listeEtats();

      labelErreur.setText("");

      // Demande de la liste des projets au controleur
      projets = controleur.listeProjetsUtilisateur(((Integer) controleur.getEmploye().getId()).toString());

      if (projets == null)
      {
         Journalisation.getERR().severe("Pas de projet");
      }
      else
      {
         // Transformation de la liste pour affichage sous forme de view list
         ObservableList<Projet> oListeProjets = FXCollections.observableArrayList(projets.getListeProjets());
         listeProjets.setItems(oListeProjets);
         listeProjets.getSelectionModel().selectFirst();
         selectionProjet();
      }
   }

   /**
    * Action lorsque qu'un projet est selectionné.
    */
   @FXML
   public void selectionProjet()
   {
      Projet projet = (Projet) listeProjets.getSelectionModel().getSelectedItem();
      Integer IdProjet = projet.getId();

      controleur.setProjet(projet);
      projetCourant = controleur.projetParId(IdProjet.toString());
      labelNomProjet.setText(projetCourant.getNom());
      textProjetDescription.setText(projetCourant.getDescription());
      labelFinProjet.setText(new SimpleDateFormat("yyyy-MM-dd").format(projetCourant.getDateRendu()));
      labelDebutProjet.setText(new SimpleDateFormat("yyyy-MM-dd").format(projetCourant.getDateDepart()));
      labelChefProjet.setText(projetCourant.getChefProjet().getPrenom() + " " + projetCourant.getChefProjet().getNom());

      // Récupération des listes sur le serveur
      ListeModules listeModules = controleur.listeModulesProjets(IdProjet.toString());
      // Création des observableList grâce aux liste contenues dans nos objets
      ObservableList<Module> oListeModules = FXCollections.observableArrayList(listeModules.getListeModules());

      colIdModule.setCellValueFactory(new PropertyValueFactory<Module, String>("id"));
      colNomModule.setCellValueFactory(new PropertyValueFactory<Module, String>("nom"));
      colPrioriteModule.setCellValueFactory(new PropertyValueFactory<Module, Priorite>("priorite"));
      colEtatModule.setCellValueFactory(new PropertyValueFactory<Module, Etat>("etat"));
      colDateFinModule.setCellValueFactory(
            new Callback<TableColumn.CellDataFeatures<Module, String>, ObservableValue<String>>()
            {
               @Override
               public ObservableValue<String> call(TableColumn.CellDataFeatures<Module, String> module)
               {
                  SimpleStringProperty property = new SimpleStringProperty();
                  DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                  property.setValue(dateFormat.format(module.getValue().getDeadline()));
                  return property;
               }
            });

      //affiche les infos sur un module
      tableauModule.setItems(oListeModules);
   }

   /**
    * Action lors du double-clique sur une cellule.
    *
    * @param event Événement souris
    */
   @FXML
   public void clickCellule(MouseEvent event)
   {
      labelErreur.setText("");

      if (event.getClickCount() == 2) //Checking double click
      {
         Integer tmp = tableauModule.getSelectionModel().getSelectedItem().getId();
         moduleCourant = controleur.moduleParId(tmp.toString());

         ListeEmployes listeEmployesModuleCourant = controleur.listeUtilisateursModule(Integer.toString(moduleCourant.getId()));
         boolean employeTravailleModule = false;

         for (Employe employe : listeEmployesModuleCourant.getListeEmployes())
         {
            if (controleur.getEmploye().getId() == employe.getId())
            {
               employeTravailleModule = true;
               break;
            }
         }

         if (!employeTravailleModule)
         {
            labelErreur.setText("Vous ne travaillez pas sur ce module");
            return;
         }

         controleur.setModule(moduleCourant);

         Parent moduleSelectione = null;
         FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Vues/ListeTachesEmploye.fxml")); // Version Jar

         try
         {
            moduleSelectione = (Parent) fxmlLoader.load(); // Version Jar
         } catch (IOException e)
         {
            Journalisation.getERR().severe("Erreur lors du changement vers la fenêtre liste tâches employe."
                                                 + e.getMessage());
         }

         Stage sceneActuelle = (Stage) labelNomProjet.getScene().getWindow();
         sceneActuelle.setScene(new Scene(moduleSelectione));
         sceneActuelle.setResizable(false);
         sceneActuelle.show();

         ListeTachesEmployeControleur controleurListeTache = (ListeTachesEmployeControleur) fxmlLoader.getController();
         controleurListeTache.setDonnees(controleur);
      }
   }
}
