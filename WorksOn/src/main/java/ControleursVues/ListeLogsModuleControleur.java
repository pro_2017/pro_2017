package ControleursVues;

import Controleur.Controleur;
import Objets.*;
import Utilitaires.Journalisation;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.IOException;
import java.text.SimpleDateFormat;

/**
 * Contrôleur de la vue ListeLogsModule.fxml
 * Ce contrôleur regroupe toutes les actions qu'il est possible de faire sur cette vue.
 */
public class ListeLogsModuleControleur
{
   @FXML
   public Button boutonRetour;

   @FXML
   public TableView<Log> tableauLogs;

   @FXML
   private TableColumn
         colIdTache,
         coloneHeureDebut,
         coloneHeureFin,
         coloneRemarques;
   @FXML
   private Label
         labelNomProjet,
         labelNomModule,
         labelHeureEffectuees,
         labelEtatTache,
         labelPrioriteTache,
         labelNomTache,
         labelErreur,
         labelResponsableTache;

   // Membres non-FXML
   private Controleur controleur;
   private ListeLog logs;

   /**
    * Méthode appelée lors d'un changement de vue.
    * Initialise les données de la vue.
    *
    * @param controleur Controleur principal communiquant avec le serveur.
    */
   public void setDonnees(Controleur controleur)
   {
      Integer idTache;
      this.controleur = controleur;

      idTache = controleur.getTache().getId();
      System.out.println(idTache);

      labelNomProjet.setText(controleur.getProjet().getNom());
      labelNomTache.setText(controleur.getTache().getNom());
      labelNomModule.setText(controleur.getModule().getNom());
      labelEtatTache.setText(controleur.getTache().getEtat().getNom());
      labelResponsableTache.setText(controleur.getTache().getEmploye().getNom() + " " + controleur.getTache().getEmploye().getPrenom());
      labelPrioriteTache.setText(controleur.getTache().getPriorite().toString());
      labelHeureEffectuees.setText(new SimpleDateFormat("HH:mm").format(controleur.getTache().getDureeEffective()));
      labelErreur.setText("");

      // Demande de la liste des projets au controleur
      logs = controleur.listeLogsTache(idTache.toString());

      ObservableList<Log> oListeLog = FXCollections.observableArrayList(logs.getListeLog());

      colIdTache.setCellValueFactory(new PropertyValueFactory<Log, String>("id"));
      coloneRemarques.setCellValueFactory(new PropertyValueFactory<Log, String>("remarques"));
      coloneHeureDebut.setCellValueFactory(
            new Callback<TableColumn.CellDataFeatures<Log, String>, ObservableValue<String>>()
            {
               @Override
               public ObservableValue<String> call(TableColumn.CellDataFeatures<Log, String> log)
               {
                  SimpleStringProperty property = new SimpleStringProperty();
                  SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                  property.setValue(dateFormat.format(log.getValue().getDebut()));
                  return property;
               }
            });

      coloneHeureFin.setCellValueFactory(
            new Callback<TableColumn.CellDataFeatures<Log, String>, ObservableValue<String>>()
            {
               @Override
               public ObservableValue<String> call(TableColumn.CellDataFeatures<Log, String> log)
               {
                  SimpleStringProperty property = new SimpleStringProperty();
                  SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                  property.setValue(dateFormat.format(log.getValue().getFin()));
                  return property;
               }
            });

      tableauLogs.setItems(oListeLog);
   }

   /**
    * Changement de vue
    * Retour sur la liste des taches du chef de projet.
    */
   @FXML
   public void retour()
   {
      controleur.setTache(null);

      Parent listeProjet = null;
      FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Vues/ListeTachesChefProjet.fxml")); // Version Jar

      try
      {
         listeProjet = (Parent) fxmlLoader.load(); // Version Jar
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Erreur lors du changement vers la fenêtre de liste projet utilisateur."
                                              + e.getMessage());
      }

      Stage sceneActuelle = (Stage) labelNomProjet.getScene().getWindow();
      sceneActuelle.setScene(new Scene(listeProjet));
      sceneActuelle.setResizable(false);
      sceneActuelle.show();

      ListeTachesChefProjetControleur controleurListeTaches = (ListeTachesChefProjetControleur) fxmlLoader.getController();
      controleurListeTaches.setDonnees(controleur);
   }
}
