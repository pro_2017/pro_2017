package ControleursVues;

import Controleur.Controleur;
import Enums.Priorite;
import Objets.Etat;
import Objets.ListeTaches;
import Objets.Tache;
import Utilitaires.Journalisation;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Contrôleur de la vue ListeTachesChefProjet.fxml
 * Ce contrôleur regroupe toutes les actions qu'il est possible de faire sur cette vue.
 */
public class ListeTachesChefProjetControleur
{
   @FXML
   public TableView<Tache> tableauTaches;

   @FXML
   private TableColumn
         colIdTache,
         colNomTache,
         colPrioriteTache,
         colEtatTache,
         colDureeEstimee,
         colDureeEffective,
         colResponsableTache;

   @FXML
   private Label
         labelNomProjet,
         labelNomModule,
         labelManager,
         labelEtat,
         labelPriorite,
         labelDateRendu;

   // Membres non-FXML
   private Controleur controleur;
   private ListeTaches taches;
   private Integer idEmploye;
   private Integer idModule;

   /**
    * Méthode appelée lors d'un changement de vue.
    * Initialise les données de la vue.
    *
    * @param controleur Controleur principal communiquant avec le serveur.
    */
   public void setDonnees(Controleur controleur)
   {

      this.controleur = controleur;

      idEmploye = controleur.getEmploye().getId();
      idModule = controleur.getModule().getId();

      labelNomProjet.setText(controleur.getProjet().getNom());
      labelManager.setText(controleur.getProjet().getChefProjet().getPrenom() + " " + controleur.getProjet().getChefProjet().getNom());
      labelNomModule.setText(controleur.getModule().getNom());
      labelEtat.setText(controleur.getModule().getEtat().getNom());
      labelPriorite.setText(controleur.getModule().getPriorite().toString());
      labelDateRendu.setText(new SimpleDateFormat("yyyy-MM-dd").format(controleur.getModule().getDeadline()));

      // Demande de la liste des projets au controleur
      taches = controleur.listeTachesModule(idModule.toString());
      ObservableList<Tache> oListeTaches = FXCollections.observableArrayList(taches.getListeTaches());

      colIdTache.setCellValueFactory(new PropertyValueFactory<Tache, String>("id"));
      colNomTache.setCellValueFactory(new PropertyValueFactory<Tache, String>("nom"));
      colPrioriteTache.setCellValueFactory(new PropertyValueFactory<Tache, Priorite>("priorite"));
      colEtatTache.setCellValueFactory(new PropertyValueFactory<Tache, Etat>("etat"));
      colDureeEstimee.setCellValueFactory(
            new Callback<TableColumn.CellDataFeatures<Tache, String>, ObservableValue<String>>()
            {
               @Override
               public ObservableValue<String> call(TableColumn.CellDataFeatures<Tache, String> tache)
               {
                  SimpleStringProperty property = new SimpleStringProperty();
                  DateFormat dateFormat = new SimpleDateFormat("HH:mm");
                  property.setValue(dateFormat.format(tache.getValue().getDureePlanifiee()));
                  return property;
               }
            });

      colDureeEffective.setCellValueFactory(
            new Callback<TableColumn.CellDataFeatures<Tache, String>, ObservableValue<String>>()
            {
               @Override
               public ObservableValue<String> call(TableColumn.CellDataFeatures<Tache, String> tache)
               {
                  SimpleStringProperty property = new SimpleStringProperty();
                  DateFormat dateFormat = new SimpleDateFormat("HH:mm");
                  property.setValue(dateFormat.format(tache.getValue().getDureeEffective()));
                  return property;
               }
            });

      colResponsableTache.setCellValueFactory(
            new Callback<TableColumn.CellDataFeatures<Tache, String>, ObservableValue<String>>()
            {
               @Override
               public ObservableValue<String> call(TableColumn.CellDataFeatures<Tache, String> tache)
               {
                  SimpleStringProperty property = new SimpleStringProperty();
                  property.setValue(tache.getValue().getEmploye().getPrenom() + " " + tache.getValue().getEmploye().getNom());
                  return property;
               }
            });

      colIdTache.setSortType(TableColumn.SortType.DESCENDING);
      tableauTaches.getSortOrder().add(colIdTache);
      tableauTaches.setItems(oListeTaches);
   }

   /**
    * Changement de vue
    * Retour sur la liste des projets du chef de projet.
    */
   public void retour()
   {
      controleur.setModule(null);
      controleur.setProjet(null);

      Parent listeProjets = null;
      FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Vues/ListeProjetsChefProjet.fxml")); // Version Jar

      try
      {
         listeProjets = (Parent) fxmlLoader.load(); // Version Jar
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Erreur lors du changement vers la fenêtre liste projets chef projet." + e.getMessage());
      }

      Stage sceneActuelle = (Stage) labelEtat.getScene().getWindow();
      sceneActuelle.setScene(new Scene(listeProjets));
      sceneActuelle.setResizable(false);
      sceneActuelle.show();

      ListeProjetsChefProjetControleur controleurListeProjets = (ListeProjetsChefProjetControleur) fxmlLoader.getController();
      controleurListeProjets.setDonnees(controleur);
   }

   /**
    * Action lors du double-clique sur une cellule.
    *
    * @param event Événement souris
    */
   public void clickCellule(MouseEvent event)
   {
      if (event.getClickCount() == 2) //Checking double click
      {
         Integer tmp = tableauTaches.getSelectionModel().getSelectedItem().getId();
         Tache tacheCourante = controleur.tacheParId(tmp.toString());
         controleur.setTache(tacheCourante);

         Parent tacheSelectionee = null;
         FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Vues/ListeLogsModule.fxml")); // Version Jar

         try
         {
            tacheSelectionee = (Parent) fxmlLoader.load(); // Version Jar
         } catch (IOException e)
         {
            Journalisation.getERR().severe("Erreur lors du changement vers la liste des logs. " + e.getMessage());
         }

         Stage sceneActuelle = (Stage) labelNomProjet.getScene().getWindow();
         sceneActuelle.setScene(new Scene(tacheSelectionee));
         sceneActuelle.setResizable(false);
         sceneActuelle.show();

         ListeLogsModuleControleur controleurListeLogs = (ListeLogsModuleControleur) fxmlLoader.getController();
         controleurListeLogs.setDonnees(controleur);
      }
   }
}
