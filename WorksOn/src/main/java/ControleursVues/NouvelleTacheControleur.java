package ControleursVues;

import Controleur.Controleur;
import Enums.*;
import Objets.*;
import Protocoles.ProtocoleWorksOn;
import Utilitaires.Journalisation;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Contrôleur de la vue NouvelleTache.fxml
 * Ce contrôleur regroupe toutes les actions qu'il est possible de faire sur cette vue.
 */
public class NouvelleTacheControleur
{
   @FXML
   private Button
         boutonAjouter,
         boutonAnnuler;

   @FXML
   private Label labelErreur;

   @FXML
   private TextField
         texteTache,
         texteHeures,
         texteMinutes;

   @FXML
   private ChoiceBox<Priorite> boxPriorite;

   @FXML
   private ChoiceBox<Etat> boxEtat;

   // Champs non-FXML
   private Controleur controleur;
   private ObservableList<Priorite> oListPrio;
   private ObservableList<Etat> oListEtat;

   /**
    * Action du bouton "Ajouter"
    * Vérification des champs et ajout dans la base de données de la nouvelle tâche.
    */
   @FXML
   public void ajouterTache()
   {
      String nomSaisi = texteTache.getText();
      String heuresSaisie = texteHeures.getText();
      String minutesSaisie = texteMinutes.getText();
      Priorite prioriteChoisie = boxPriorite.getValue();
      Etat etatChoisi = boxEtat.getValue();

      if (nomSaisi.equalsIgnoreCase("") || heuresSaisie.equalsIgnoreCase("") ||
            minutesSaisie.equalsIgnoreCase(""))
      {
         labelErreur.setText("Merci de remplir les champs ci-dessus");
         return;
      }

      String reponse = controleur.creerTache(nomSaisi, prioriteChoisie, heuresSaisie, minutesSaisie, etatChoisi,
                                             controleur.getModule());

      if (reponse.equalsIgnoreCase(ProtocoleWorksOn.RSP_OK))
      {
         afficherListeTaches();
      }
      else
      {
         labelErreur.setText(reponse);
      }
   }

   /**
    * Action du bouton "Annuler"
    * Retour sur la liste des tâches.
    */
   @FXML
   public void annuler()
   {
      afficherListeTaches();
   }

   @FXML
   public void saisieEnCours()
   {
      labelErreur.setText("");
   }

   /**
    * Méthode appelée lors d'un changement de vue.
    * Initialise les données de la vue.
    *
    * @param controleur Controleur principal communiquant avec le serveur.
    */
   public void setDonnees(Controleur controleur)
   {
      this.controleur = controleur;
      labelErreur.setText("");

      oListPrio = FXCollections.observableArrayList(Priorite.values());
      boxPriorite.setItems(oListPrio);
      boxPriorite.getSelectionModel().selectFirst();

      oListEtat = FXCollections.observableArrayList(controleur.getListeEtats().getListeEtats());
      boxEtat.setItems(oListEtat);
      boxEtat.getSelectionModel().selectFirst();

      // Ajout du filtres sur les champs minutes et heures. La méthode handle du filtre est appellée à chaque fois
      // que l'évenement KEY_TYPED est reçu
      texteHeures.addEventFilter(KeyEvent.KEY_TYPED, validationTemps(2));
      texteMinutes.addEventFilter(KeyEvent.KEY_TYPED, validationTemps(2));
      texteMinutes.setPromptText("00");
      texteHeures.setPromptText("00");
   }

   /**
    * Méthode permettant de faire un filtre sur les champs de saisie du temps.
    *
    * @param nombreMaxDigit Le nombre maximum de digit que peuvent posséder les champs heures / minutes
    * @return Le eventHandler de la saisie si celle-ci correspond aux exigences du filtre
    */
   private EventHandler validationTemps(int nombreMaxDigit)
   {
      return new EventHandler<KeyEvent>()
      {
         @Override
         public void handle(KeyEvent event)
         {
            TextField fieldModifie = (TextField) event.getSource();

            // Si on est en train de taper plus du nombre max de digit, l'évenement est dropé
            if (fieldModifie.getText().length() >= nombreMaxDigit)
            {
               event.consume();
            }

            try
            {
               Integer.parseInt(event.getCharacter());
            } catch (NumberFormatException e) // Si la saisie n'est pas transformable en nombre, l'évenement est dropé
            {
               event.consume();
            }
         }
      };
   }

   /**
    * Changement de vue
    * Retour sur la liste des taches
    */
   private void afficherListeTaches()
   {
      Parent listeTaches = null;
      FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Vues/ListeTachesEmploye.fxml")); // Version Jar

      try
      {
         listeTaches = (Parent) fxmlLoader.load(); // Version Jar
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Erreur lors du changement vers la fenêtre de la liste des tâches."
                                              + e.getMessage());
      }

      Stage sceneActuelle = (Stage) boutonAjouter.getScene().getWindow();
      sceneActuelle.setScene(new Scene(listeTaches));
      sceneActuelle.setResizable(false);
      sceneActuelle.show();

      ListeTachesEmployeControleur controleurListeTache = (ListeTachesEmployeControleur) fxmlLoader.getController();
      controleurListeTache.setDonnees(controleur);
   }
}
