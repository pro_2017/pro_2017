package ControleursVues;

import Controleur.Controleur;
import Enums.Priorite;
import Objets.Employe;
import Objets.Projet;
import Protocoles.ProtocoleWorksOn;
import Utilitaires.Journalisation;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Contrôleur de la vue NouveauModule.fxml
 * Ce contrôleur regroupe toutes les actions qu'il est possible de faire sur cette vue.
 */
public class NouveauModuleControleur
{
   @FXML
   private Label lbl_nomProjetNouveauModule;
   @FXML
   private TextField txt_nomNouveauModule;
   @FXML
   private DatePicker dtp_dateNouveauModule;
   @FXML
   private TextField txta_descriNouveauModule;
   @FXML
   private ChoiceBox drop_prioNouveauModule;
   @FXML
   private Label lbl_err;

   // Membres non-FXML
   private Controleur controleur;
   private Projet projetCourant;
   private ObservableList<Priorite> olst_prio;

   /**
    * Méthode appelée lors d'un changement de vue.
    * Initialise les données de la vue.
    *
    * @param controleur Controleur principal communiquant avec le serveur.
    * @param projet     Projet actuellement modifié auquel sera ajouté le module.
    */
   public void setDonnees(Controleur controleur, Projet projet)
   {
      this.controleur = controleur;
      projetCourant = projet;

      olst_prio = FXCollections.observableArrayList(Priorite.values());
      drop_prioNouveauModule.setItems(olst_prio);
      drop_prioNouveauModule.getSelectionModel().selectFirst();

      lbl_nomProjetNouveauModule.setText(projetCourant.getNom());
      lbl_err.setText("");
   }

   /**
    * Action du bouton "Créer module"
    * Ajoute le nouveau module dans la base de donnée.
    */
   @FXML
   public void creerNouveauModule()
   {
      if (txt_nomNouveauModule.getText().equalsIgnoreCase("") || dtp_dateNouveauModule.getValue() == null)
      {
         lbl_err.setText("Merci de remplir les champs nom et date ci-dessus");
         return;
      }

      List<Employe> employes = new ArrayList<>();
      employes.add(controleur.getEmploye());
      String err;
      err = controleur.creerModule(txt_nomNouveauModule.getText(), dtp_dateNouveauModule.getValue(),
                                   Priorite.valueOf(drop_prioNouveauModule.getValue().toString()),
                                   controleur.getListeEtats().getListeEtats().get(0),
                                   txta_descriNouveauModule.getText(), projetCourant, employes);

      if (!err.equals(ProtocoleWorksOn.RSP_OK))
      {
         lbl_err.setText(err);
      }
      else
      {
         retourListeProjetChefProjet();
      }
   }

   /**
    * Action du bouton "Annuler"
    * Retour sur la liste des projets du chef de projet.
    */
   @FXML
   public void annulerNouveauModule()
   {
      retourListeProjetChefProjet();
   }

   @FXML
   public void saisieEnCours()
   {
      lbl_err.setText("");
   }

   /**
    * Changement de vue
    * Retour sur la liste des projets du chef de projet.
    */
   public void retourListeProjetChefProjet()
   {
      Parent chef = null;
      FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Vues/ListeProjetsChefProjet.fxml")); // Version Jar

      try
      {
         chef = (Parent) fxmlLoader.load(); // Version Jar
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Erreur lors du changement vers la fenêtre liste projets chef projet : "
                                              + e.getMessage());
      }

      Stage sceneActuelle = (Stage) lbl_nomProjetNouveauModule.getScene().getWindow();
      sceneActuelle.setScene(new Scene(chef));
      sceneActuelle.setResizable(false);
      sceneActuelle.show();

      ListeProjetsChefProjetControleur controleurChef = (ListeProjetsChefProjetControleur) fxmlLoader.getController();
      controleurChef.setDonnees(controleur);
   }

   public void ajouterUtilisateursModule()
   {
      Parent ajoutUser = null;
      FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Vues/AjoutUtilisateurModule.fxml")); // Version Jar

      try
      {
         ajoutUser = (Parent) fxmlLoader.load(); // Version Jar
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Erreur lors du changement vers la fenêtre ajout utilisateur module : "
                                              + e.getMessage());
      }

      Stage sceneActuelle = (Stage) lbl_nomProjetNouveauModule.getScene().getWindow();
      sceneActuelle.setScene(new Scene(ajoutUser));
      sceneActuelle.setResizable(false);
      sceneActuelle.show();

      AjoutUtilisateurModuleControleur controleurAjoutusr = (AjoutUtilisateurModuleControleur) fxmlLoader.getController();
      controleurAjoutusr.setDonnees(controleur, controleur.getModule());
   }
}
