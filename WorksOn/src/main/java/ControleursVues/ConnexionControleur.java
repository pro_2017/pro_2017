package ControleursVues;

import Controleur.Controleur;
import Utilitaires.Journalisation;

import java.io.IOException;

import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class ConnexionControleur
{

   @FXML
   private ProgressIndicator pending;

   @FXML
   private AnchorPane anchorpane_connexion;

   @FXML
   private TextField username;

   @FXML
   private PasswordField motDePasse;

   @FXML
   private Label erreur;

   // Membre non FXML
   private Controleur controleur;
   private boolean estUneReconnexion = false;

   @FXML
   public void boutonConnexionAppuye(ActionEvent actionEvent) throws IOException
   {
      String motDePasseSaisi = motDePasse.getText();
      String usernameSaisi = username.getText();

      // Si un des champs n'est pas rempli
      if (motDePasseSaisi.equalsIgnoreCase("") || usernameSaisi.equalsIgnoreCase(""))
      {
         erreur.setText("Merci de remplir les champs ci-dessus");
      }
      else
      {
         if (!estUneReconnexion)
         {
            controleur = new Controleur();
         }

         anchorpane_connexion.setOpacity(0.2);
         pending.setVisible(true);

         // Pour comprendre cette partie -> http://docs.oracle.com/javafx/2/api/javafx/concurrent/Task.html
         // Tache qui lance la requete en direction du serveur en parallèle de la fenetre qui met chargement
         Task<String> verificationCredentials = new Task<String>()
         {
            @Override
            public String call() throws IOException
            {
               String reponseAuthentification = controleur.authentification(usernameSaisi, motDePasseSaisi);
               return reponseAuthentification;
            }
         };

         // Quand la tache à terminer on affiche la bonne fenêtre selon le role
         verificationCredentials.setOnSucceeded(new EventHandler<WorkerStateEvent>()
         {
            @Override
            public void handle(WorkerStateEvent event)
            {
               anchorpane_connexion.setOpacity(1);
               pending.setVisible(false);

               // Utilisez le nom de la task .getValue() pour récupérer la valeur de retour de la task
               if (verificationCredentials.getValue() == null)
               {
                  if (!estUneReconnexion)
                  {
                     switch (controleur.getEmploye().getRole())
                     {
                        case ADMINISTRATEUR:
                           afficherFenetreAdmin();
                           break;

                        case CHEF_PROJET:
                           afficherFenetreChef();
                           break;

                        case UTILISATEUR:
                           afficherFenetreUtilisateur();
                           break;
                     }
                  }
                  else
                  {
                     afficherLogReconnexion();
                  }
               }
               else
               {
                  erreur.setText("ERREUR : " + verificationCredentials.getValue());
                  if (!verificationCredentials.getValue().equalsIgnoreCase("le serveur est introuvable"))
                  {
                     Journalisation.getLOG().severe(verificationCredentials.getValue());
                  }
               }
            }
         });

         new Thread(verificationCredentials).start();
      }
   }

   @FXML
   public void saisieEnCours()
   {
      erreur.setText("");
   }

   public void setDonnees(Controleur controleur)
   {
      this.controleur = controleur;
      estUneReconnexion = true;
   }

   /**
    * Méthode changeant de fenetre pour la fenetre de l'administrateur
    */
   private void afficherFenetreAdmin()
   {
      Parent admin = null;
      FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Vues/MenuAdmin.fxml")); // Version Jar
      //FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../Vues/MenuAdmin.fxml")); // Version IntelliJ

      try
      {
         admin = (Parent) fxmlLoader.load(); // Version Jar
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Erreur lors du changement vers la fenêtre admin : " + e.getMessage());
      }

      Stage sceneActuelle = (Stage) motDePasse.getScene().getWindow();
      sceneActuelle.setScene(new Scene(admin));
      sceneActuelle.setResizable(false);
      sceneActuelle.show();

      // Récupération du constructeur et appelle de sa fonction d'initialisation
      MenuAdminControleur controleurAdmin = (MenuAdminControleur) fxmlLoader.getController();
      controleurAdmin.setDonnees(controleur);
   }

   /**
    * Méthode changeant de fenetre pour la fenetre du chef de projet
    */
   private void afficherFenetreChef()
   {
      Parent chef = null;
      FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Vues/ListeProjetsChefProjet.fxml")); // Version Jar
      //FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../Vues/MenuAdmin.fxml")); // Version IntelliJ

      try
      {
         chef = (Parent) fxmlLoader.load(); // Version Jar
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Erreur lors du changement vers la fenêtre chef : " + e.getMessage());
      }

      Stage sceneActuelle = (Stage) motDePasse.getScene().getWindow();
      sceneActuelle.setScene(new Scene(chef));
      sceneActuelle.setResizable(false);
      sceneActuelle.show();

      // Récupération du constructeur et appelle de sa fonction d'initialisation
      ListeProjetsChefProjetControleur controleurChef = (ListeProjetsChefProjetControleur) fxmlLoader.getController();
      controleurChef.setDonnees(controleur);
   }

   /**
    * Méthode changeant de fenetre pour la fenetre de l'utilisateur
    */
   private void afficherFenetreUtilisateur()
   {
      Parent user = null;
      FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Vues/ListeProjetsEmploye.fxml")); // Version Jar
      //FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../Vues/MenuAdmin.fxml")); // Version IntelliJ

      try
      {
         user = (Parent) fxmlLoader.load(); // Version Jar
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Erreur lors du changement vers la fenêtre utilisateur : " + e.getMessage());
      }

      Stage sceneActuelle = (Stage) motDePasse.getScene().getWindow();
      sceneActuelle.setScene(new Scene(user));
      sceneActuelle.setResizable(false);
      sceneActuelle.show();

      // Récupération du constructeur et appelle de sa fonction d'initialisation
      ListeProjetsEmployeControleur controleurUtilisateur = (ListeProjetsEmployeControleur) fxmlLoader.getController();
      controleurUtilisateur.setDonnees(controleur);
   }

   private void afficherLogReconnexion()
   {
      Parent remarqueLog = null;
      FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Vues/TacheRemarques.fxml")); // Version Jar

      try
      {
         remarqueLog = (Parent) fxmlLoader.load(); // Version Jar
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Erreur lors du changement vers le log: " + e.getMessage());
      }

      Stage sceneActuelle = (Stage) motDePasse.getScene().getWindow();
      sceneActuelle.setScene(new Scene(remarqueLog));
      sceneActuelle.setResizable(false);
      sceneActuelle.show();

      // Récupération du constructeur et appelle de sa fonction d'initialisation
      TacheRemarquesControleur controleurRemarque = (TacheRemarquesControleur) fxmlLoader.getController();
      controleurRemarque.setDonnees(controleur, controleur.getLog());
   }
}
