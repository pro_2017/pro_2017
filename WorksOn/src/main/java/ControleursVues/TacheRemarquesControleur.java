package ControleursVues;

import Controleur.Controleur;
import Objets.Etat;
import Objets.Log;
import Protocoles.ProtocoleWorksOn;
import Utilitaires.Journalisation;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Contrôleur de la vue TacheRemarque.fxml
 * Ce contrôleur regroupe toutes les actions qu'il est possible de faire sur cette vue.
 */
public class TacheRemarquesControleur
{
   @FXML
   private ChoiceBox<Etat> listeEtatTache;

   @FXML
   private TextArea fieldRemarque;

   @FXML
   private Button boutonValider;

   @FXML
   private Label labelErreur;

   // Champs non-FXML
   Controleur controleur;
   Log log;
   ObservableList<Etat> oListeEtats;

   /**
    * Action du bouton "Valider"
    * Vérifie l'authentification puis ajoute la remarque dans la base de données.
    */
   @FXML
   public void valider()
   {
      if (!controleur.authentification().equals(ProtocoleWorksOn.RSP_OK))
      {
         afficherReConnexion();
         return;
      }

      String remarqueSaisie = fieldRemarque.getText();
      Etat etatChoisi = listeEtatTache.getValue();

      if (etatChoisi.getId() != log.getTache().getEtat().getId())
      {
         String reponseTacheModifiee = controleur.modifierTache(log.getTache(), etatChoisi);

         if (!reponseTacheModifiee.equalsIgnoreCase(ProtocoleWorksOn.RSP_OK))
         {
            Journalisation.getERR().severe("Echec mise a jour tache");
            labelErreur.setText(reponseTacheModifiee);
            return;
         }
      }

      String reponse = controleur.creerLog(remarqueSaisie, log.getDebut(), log.getFin(), log.getTache());

      if (reponse != null && reponse.equalsIgnoreCase(ProtocoleWorksOn.RSP_OK))
      {
         afficherListeTaches();
      }
      else
      {
         if (reponse != null)
         {
            labelErreur.setText(reponse);
         }
         else
         {
            labelErreur.setText("Erreur lors de la création du log.");
         }
      }
   }

   /**
    * Méthode appelée lors d'un changement de vue.
    * Initialise les données de la vue.
    *
    * @param controleur Controleur principal communiquant avec le serveur.
    * @param log        Log actuellement modifié.
    */
   public void setDonnees(Controleur controleur, Log log)
   {
      this.controleur = controleur;
      this.log = log;

      labelErreur.setText("");

      oListeEtats = FXCollections.observableArrayList(controleur.getListeEtats().getListeEtats());
      listeEtatTache.setItems(oListeEtats);

      Etat aAfficher = log.getTache().getEtat();
      Etat oLstCorrespondant = null;
      for (Etat e : oListeEtats)
      {
         if (e.getId() == aAfficher.getId())
         {
            oLstCorrespondant = e;
         }
      }

      listeEtatTache.getSelectionModel().select(oLstCorrespondant);
   }

   /**
    * Changement de vue
    * Retour sur la liste des tâches
    */
   private void afficherListeTaches()
   {
      Parent listeTaches = null;
      FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Vues/ListeTachesEmploye.fxml")); // Version Jar

      try
      {
         listeTaches = (Parent) fxmlLoader.load(); // Version Jar
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Erreur lors du changement vers la fenêtre liste des tâches."
                                              + e.getMessage());
      }

      Stage sceneActuelle = (Stage) boutonValider.getScene().getWindow();
      sceneActuelle.setScene(new Scene(listeTaches));
      sceneActuelle.setResizable(false);
      sceneActuelle.show();

      ListeTachesEmployeControleur controleurFenetreListeTaches = (ListeTachesEmployeControleur) fxmlLoader.getController();
      controleurFenetreListeTaches.setDonnees(controleur);
   }

   /**
    * Changement de vue
    * Affiche la vue de reconnexion.
    */
   private void afficherReConnexion()
   {
      controleur.setProjet(log.getTache().getModule().getProjet());
      controleur.setModule(log.getTache().getModule());
      controleur.setTache(log.getTache());
      controleur.setLog(log);

      Parent reconnexion = null;
      FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Vues/Connexion.fxml")); // Version Jar

      try
      {
         reconnexion = (Parent) fxmlLoader.load(); // Version Jar
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Erreur lors du changement vers la fenêtre de reconnection."
                                              + e.getMessage());
      }

      Stage sceneActuelle = (Stage) boutonValider.getScene().getWindow();
      sceneActuelle.setScene(new Scene(reconnexion));
      sceneActuelle.setResizable(false);
      sceneActuelle.show();

      ConnexionControleur controleurConnexion = (ConnexionControleur) fxmlLoader.getController();
      controleurConnexion.setDonnees(controleur);
   }
}
