package ControleursVues;

import Controleur.Controleur;
import Enums.Role;
import Objets.Employe;
import Objets.Module;
import Protocoles.ProtocoleWorksOn;
import Utilitaires.Journalisation;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Contrôleur de la vue AjouterUtilisateursModule.fxml
 * Ce contrôleur regroupe toutes les actions qu'il est possible de faire sur cette vue.
 */
public class AjoutUtilisateurModuleControleur
{
   @FXML
   private Label lbl_nomModuleAjoutUtilisateurs;
   @FXML
   private Label lbl_err;
   @FXML
   private ListView<Employe> lst_utilisateurs;

   // Membres non-FXML
   private Controleur controleur;
   private Module module;

   // Liste d'observable pour converstion en ListView
   private ObservableList<Employe> olst_employes;

   /**
    * Méthode appelée lors d'un changement de vue.
    * Initialise les données de la vue.
    *
    * @param c Controleur principale communiquant avec le serveur.
    * @param m Module actuellement modifié.
    */
   public void setDonnees(Controleur c, Module m)
   {
      controleur = c;
      module = m;

      lbl_nomModuleAjoutUtilisateurs.setText(module.getNom());
      lbl_err.setText("");

      // Liste des utilisateurs (sans les chef de projet et sans les administrateurs)
      // Sans les utilisateurs déjà sur le module
      ArrayList<Employe> utilisateurs = new ArrayList<Employe>();

      boolean surLeModule = false;

      for (Employe employe : controleur.listeEmployes().getListeEmployes())
      {
         if (employe.getRole().equals(Role.UTILISATEUR))
         {
            // Vérification que l'employé n'est pas déjà sur le module.
            surLeModule = false;
            for (Module moduleEmploye : employe.getListeDeModules())
            {
               if (moduleEmploye.getId() == module.getId())
               {
                  surLeModule = true;
                  break;
               }
            }

            if (!surLeModule)
            {
               utilisateurs.add(employe);
            }
         }
      }
      // Transformation en liste d'observable pour peupler la ListView
      olst_employes = FXCollections.observableArrayList(utilisateurs);
      lst_utilisateurs.setItems(olst_employes);
      lst_utilisateurs.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

      // Selection des employés travaillant déjà sur le module
      Employe oLstCorrespondant = null;
      for (Employe utilisateur : module.getListeEmployes())
      {
         for (Employe e : olst_employes)
         {
            if (e.getId() == utilisateur.getId() && e.getRole().equals(Role.UTILISATEUR))
            {
               oLstCorrespondant = e;
            }
         }
         lst_utilisateurs.getSelectionModel().select(oLstCorrespondant);
      }
   }

   /**
    * Action du bouton "Ajouter utilisateur(s)"
    * Les employés selectionnés dans la ListView seront ajouter au module en court.
    */
   public void ajouterUtilisateursAuModule()
   {
      String err = "";
      for (Employe utilisateur : lst_utilisateurs.getSelectionModel().getSelectedItems())
      {
         err = controleur.modifierEmploye(utilisateur, module);

         if (!err.equals(ProtocoleWorksOn.RSP_OK))
         {
            lbl_err.setText(err + "\nLe reste des modifications n'a pas été pris en compte.");
            break;
         }
      }
      if (err.equals(ProtocoleWorksOn.RSP_OK))
      {
         retourListeProjetChefProjet();
      }
   }

   /**
    * Action du bouton "Annuler"
    * Retour sur la liste des projets du chef de projet.
    */
   public void annulerAjoutUtilisateurs()
   {
      retourListeProjetChefProjet();
   }

   /**
    * Changement de vue
    * Retour sur la liste des projets du chef de projet.
    */
   public void retourListeProjetChefProjet()
   {
      Parent chef = null;
      FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Vues/ListeProjetsChefProjet.fxml")); // Version Jar

      try
      {
         chef = (Parent) fxmlLoader.load(); // Version Jar
      } catch (IOException e)
      {
         Journalisation.getERR().severe("Erreur lors du changement vers la fenêtre chef depuis ajout utilisateurs module: "
                                              + e.getMessage());
      }

      Stage sceneActuelle = (Stage) lst_utilisateurs.getScene().getWindow();
      sceneActuelle.setScene(new Scene(chef));
      sceneActuelle.setResizable(false);
      sceneActuelle.show();

      ListeProjetsChefProjetControleur controleurChef = (ListeProjetsChefProjetControleur) fxmlLoader.getController();
      controleurChef.setDonnees(controleur);
   }
}
