import Protocoles.ProtocoleWorksOn;
import Utilitaires.Journalisation;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.File;

/**
 * Point d'entree du programme
 */
public class WorksOn extends Application
{
   @Override
   public void start(Stage primaryStage) throws Exception
   {
      Parent login = FXMLLoader.load(getClass().getClassLoader().getResource("Vues/Connexion.fxml"));
      primaryStage.setTitle("WorksOn");
      primaryStage.setResizable(false);
      primaryStage.getIcons().add(new Image("Ressources/miniLogo.png"));
      primaryStage.setScene(new Scene(login));
      primaryStage.show();
   }

   public static void main(String[] args)
   {
      if (!new File(ProtocoleWorksOn.CHEMIN_CERT).exists())
      {
         Journalisation.getERR().severe("Le certificat SSL doit être au même niveau que le .jar");
         System.exit(0);
      }
      else
      {
         System.setProperty("javax.net.ssl.trustStore", ProtocoleWorksOn.CHEMIN_CERT);
         System.setProperty("javax.net.ssl.trustStorePassword", ProtocoleWorksOn.MDP_CERT);
      }

      launch(args);
   }
}