package Objets;

import Enums.Priorite;

import java.sql.Time;

/**
 * Classe représentant une Tache.
 */
public class Tache
{
   private int id;
   private String nom;
   private Priorite priorite;
   private Time dureePlanifiee;
   private Time dureeEffective;
   private Etat etat;
   private Module module;
   private Employe employe;

   /**
    * Constructeur par défaut
    */
   public Tache()
   {
   }

   /**
    * Constructeur utilisé par Hibernate (entier).
    *
    * @param id             L'id de la tâche récupérée
    * @param nom            Le nom de la tâche récupérée
    * @param priorite       La priorité de la tâche récupérée
    * @param dureePlanifiee La durée planifiée de la tâche récupérée
    * @param dureeEffective La durée déjà effectué sur la tâche récupérée
    * @param etat           L'état de la tâche récupérée
    * @param module         Le module qui contient la tâche récupérée
    * @param employe        L'employé à l'origine de la tâche récupérée
    */
   public Tache(int id, String nom, Priorite priorite, Time dureePlanifiee, Time dureeEffective, Etat etat, Module module, Employe employe)
   {
      this.id = id;
      this.nom = nom;
      this.priorite = priorite;
      this.dureePlanifiee = dureePlanifiee;
      this.dureeEffective = dureeEffective;
      this.etat = etat;
      this.module = module;
      this.employe = employe;
   }

   /**
    * Constructeur utilisé lors de la création d'une tâche par un client
    *
    * @param nom            Le nom de la nouvelle tâche
    * @param priorite       La priorité de la nouvelle tâche
    * @param dureePlanifiee La durée planifiée de la nouvelle tâche
    * @param etat           L'état de la nouvelle tâche
    * @param module         Le module qui contient la nouvelle tâche
    * @param employe        L'employé à l'origine de la nouvelle tâche
    */
   public Tache(String nom, Priorite priorite, Time dureePlanifiee, Etat etat, Module module, Employe employe)
   {

      this.nom = nom;
      this.priorite = priorite;
      this.dureePlanifiee = dureePlanifiee;
      this.dureeEffective = new Time(-3600000);
      this.etat = etat;
      this.module = module;
      this.employe = employe;
   }

   // GETTERS & SETTERS

   public int getId()
   {
      return id;
   }

   public void setId(int id)
   {
      this.id = id;
   }

   public String getNom()
   {
      return nom;
   }

   public void setNom(String nom)
   {
      this.nom = nom;
   }

   public Priorite getPriorite()
   {
      return priorite;
   }

   public void setPriorite(Priorite priorite)
   {
      this.priorite = priorite;
   }

   public Time getDureePlanifiee()
   {
      return dureePlanifiee;
   }

   public void setDureePlanifiee(Time dureePlanifiee)
   {
      this.dureePlanifiee = dureePlanifiee;
   }

   public Time getDureeEffective()
   {
      return dureeEffective;
   }

   public void setDureeEffective(Time dureeEffective)
   {
      this.dureeEffective = dureeEffective;
   }

   public Employe getEmploye()
   {
      return employe;
   }

   public void setEmploye(Employe employe)
   {
      this.employe = employe;
   }

   public Etat getEtat()
   {
      return etat;
   }

   public void setEtat(Etat etat)
   {
      this.etat = etat;
   }

   public Module getModule()
   {
      return module;
   }

   public void setModule(Module module)
   {
      this.module = module;
   }
}
