package Objets;

/**
 * Classe représentant une entreprise
 */
public class Entreprise
{
   private int idEntreprise;
   private String nom;
   private String telephone;
   private String adresse;

   /**
    * Constructeur par défaut
    */
   public Entreprise()
   {
   }

   /**
    * Constructeur utilisé par Hibernate (entier).
    *
    * @param idEntreprise L'id de l'entreprise récupérée
    * @param nom          Le nom de l'entreprise récupérée
    * @param telephone    Le téléphone de l'entreprise récupérée
    * @param adresse      L'adresse de l'entreprise récupérée
    */
   public Entreprise(int idEntreprise, String nom, String telephone, String adresse)
   {
      this(nom, telephone, adresse);
      this.idEntreprise = idEntreprise;
   }

   /**
    * Constructeur utilisé par Hibernate dans certains cas
    *
    * @param nom       Le nom de l'entreprise
    * @param telephone Le téléphone de l'entreprise
    * @param adresse   L'adresse de l'entreprise
    */
   public Entreprise(String nom, String telephone, String adresse)
   {
      this.nom = nom;
      this.telephone = telephone;
      this.adresse = adresse;
   }

   // GETTERS & SETTERS

   public int getIdEntreprise()
   {
      return idEntreprise;
   }

   public void setIdEntreprise(int id)
   {
      this.idEntreprise = id;
   }

   public String getNom()
   {
      return nom;
   }

   public void setNom(String nom)
   {
      this.nom = nom;
   }

   public String getTelephone()
   {
      return telephone;
   }

   public void setTelephone(String telephone)
   {
      this.telephone = telephone;
   }

   public String getAdresse()
   {
      return adresse;
   }

   public void setAdresse(String adresse)
   {
      this.adresse = adresse;
   }
}
