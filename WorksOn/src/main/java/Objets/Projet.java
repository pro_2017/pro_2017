package Objets;

import java.sql.Timestamp;

/**
 * Classe représentant un projet
 */
public class Projet
{
   private int id;
   private String nom;
   private Etat etat;
   private String description;
   private Employe chefProjet;
   private Timestamp dateDepart;
   private Timestamp dateRendu;

   /**
    * Constructeur par défaut
    */
   public Projet()
   {
      dateDepart = new Timestamp(System.currentTimeMillis());
   }

   /**
    * Constructeur utilisé par Hibernate (entier).
    *
    * @param id          L'id du projet récupéré
    * @param nom         Le nom du projet récupéré
    * @param etat        L'état du projet récupéré
    * @param description La description du projet récupéré
    * @param chefProjet  Le chef de projet du projet récupéré
    * @param dateRendu   La date de rendu du projet récupéré
    */
   public Projet(int id, String nom, Etat etat, String description, Employe chefProjet, Timestamp dateRendu)
   {
      this(nom, etat, description, chefProjet, dateRendu);
      this.id = id;
   }

   /**
    * Constructeur utilisé lors de la création d'un projet par un client.
    *
    * @param nom         Le nom du nouveau projet
    * @param etat        L'état du nouveau projet
    * @param description La description du nouveau projet
    * @param chefProjet  Le chef de projet du nouveau projet
    * @param dateRendu   La date de rendu du nouveau projet
    */
   public Projet(String nom, Etat etat, String description, Employe chefProjet, Timestamp dateRendu)
   {
      this.nom = nom;
      this.etat = etat;
      this.description = description;
      this.chefProjet = chefProjet;
      this.dateRendu = dateRendu;
   }

   // GETTERS & SETTERS

   public int getId()
   {
      return id;
   }

   public void setId(int id)
   {
      this.id = id;
   }

   public String getNom()
   {
      return nom;
   }

   public void setNom(String nom)
   {
      this.nom = nom;
   }

   public Etat getEtat()
   {
      return etat;
   }

   public void setEtat(Etat etat)
   {
      this.etat = etat;
   }

   public String getDescription()
   {
      return description;
   }

   public void setDescription(String description)
   {
      this.description = description;
   }

   public Employe getChefProjet()
   {
      return chefProjet;
   }

   public void setChefProjet(Employe chefProjet)
   {
      this.chefProjet = chefProjet;
   }

   public Timestamp getDateDepart()
   {
      return dateDepart;
   }

   public void setDateDepart(Timestamp dateDepart)
   {
      this.dateDepart = dateDepart;
   }

   public Timestamp getDateRendu()
   {
      return dateRendu;
   }

   public void setDateRendu(Timestamp dateRendu)
   {
      this.dateRendu = dateRendu;
   }

   @Override
   public String toString()
   {
      return nom;
   }
}
