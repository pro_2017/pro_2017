package Objets;

import Enums.Role;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe représentant un employe.
 */
public class Employe
{
   private int id;
   private String nom;
   private String prenom;
   private String nomUtilisateur;
   private String motDePasse;
   private String sel;
   private Role role;
   private Entreprise entreprise;
   private List<Module> listeDeModules = new ArrayList<Module>();

   /**
    * Constructeur par défaut
    */
   public Employe()
   {
   }

   /**
    * Constructeur pour hibernate (entier). (A noter que pour des raisons de sécurité évidentes le sel et le hash ne
    * sont setter à null avant l'envoi au client).
    *
    * @param id             L'id de l'employe récupéré
    * @param nom            Le nom de l'employe récupéré
    * @param prenom         Le prenom de l'employe récupéré
    * @param nomUtilisateur Le username de l'employe récupéré
    * @param role           Le role de l'employe récupéré
    * @param entreprise     L'entreprise de l'employe récupéré
    * @param listeDeModules La liste des modules sur lesquels travaille l'employe récupéré
    * @param motDePasse     Le mot de passe hashé
    * @param sel            Le sel du mot de passe
    */
   public Employe(int id, String nom, String prenom, String nomUtilisateur, Role role, Entreprise entreprise, List<Module> listeDeModules, String motDePasse, String sel)
   {
      this(nom, prenom, nomUtilisateur, role, entreprise, listeDeModules, motDePasse);
      this.id = id;
      this.sel = sel;
   }

   /**
    * Constructeur utilisé à la création d'un utilisateur par l'admin
    *
    * @param nom            Le nom de l'employe
    * @param prenom         Le prenom de l'employe
    * @param nomUtilisateur Le username de l'employe
    * @param role           Le role de l'employe
    * @param entreprise     L'entreprise de l'employe
    * @param listeDeModules La liste des modules associés à l'employe
    * @param motDePasse     Le mot de passe en clair
    */
   public Employe(String nom, String prenom, String nomUtilisateur, Role role, Entreprise entreprise, List<Module> listeDeModules, String motDePasse)
   {
      this.nom = nom;
      this.prenom = prenom;
      this.nomUtilisateur = nomUtilisateur;
      this.role = role;
      this.entreprise = entreprise;
      this.listeDeModules = listeDeModules;
      this.motDePasse = motDePasse;
   }

   /**
    * Constructeur de copie, peut-être utilisé par Hibernate dans certaines conditions.
    *
    * @param employe L'employe de base
    */
   public Employe(Employe employe)
   {
      this.nom = employe.nom;
      this.prenom = employe.prenom;
      this.nomUtilisateur = employe.nomUtilisateur;
      this.role = employe.role;
      this.entreprise = employe.entreprise;
      this.listeDeModules = employe.listeDeModules;
      this.motDePasse = employe.getMotDePasse();
      this.sel = employe.getSel();
   }

   // GETTERS & SETTERS

   public int getId()
   {
      return id;
   }

   public void setId(int id)
   {
      this.id = id;
   }

   public String getNom()
   {
      return nom;
   }

   public void setNom(String nom)
   {
      this.nom = nom;
   }

   public String getPrenom()
   {
      return prenom;
   }

   public void setPrenom(String prenom)
   {
      this.prenom = prenom;
   }

   public String getNomUtilisateur()
   {
      return nomUtilisateur;
   }

   public void setNomUtilisateur(String nomUtilisateur)
   {
      this.nomUtilisateur = nomUtilisateur;
   }

   public Role getRole()
   {
      return role;
   }

   public void setRole(Role role)
   {
      this.role = role;
   }

   public Entreprise getEntreprise()
   {
      return entreprise;
   }

   public void setEntreprise(Entreprise entreprise)
   {
      this.entreprise = entreprise;
   }

   public String getMotDePasse()
   {
      return motDePasse;
   }

   public void setMotDePasse(String motDePasse)
   {
      this.motDePasse = motDePasse;
   }

   public String getSel()
   {
      return sel;
   }

   public void setSel(String sel)
   {
      this.sel = sel;
   }

   public List<Module> getListeDeModules()
   {
      return listeDeModules;
   }

   public void setListeDeModules(List<Module> listeDeModules)
   {
      this.listeDeModules = listeDeModules;
   }

   @Override
   public String toString()
   {
      return nom + " " + prenom;
   }
}
