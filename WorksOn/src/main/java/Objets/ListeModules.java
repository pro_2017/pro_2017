package Objets;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe représentant une liste de modules.
 * Sa seule utilité est de "wrapper" une ArrayList pour faciliter la sérialisation.
 */
public class ListeModules
{
   private List<Module> listeModules;

   /**
    * Constructeur par défaut
    */
   public ListeModules()
   {
      listeModules = new ArrayList<Module>();
   }

   /**
    * Constructeur de base
    *
    * @param liste L'arrayList de modules à "wrapper"
    */
   public ListeModules(List<Module> liste)
   {
      this();
      listeModules.addAll(liste);
   }

   /**
    * Méthode permettant de récupérer la liste sous forme normale
    *
    * @return L'arrayList de modules
    */
   public List<Module> getListeModules()
   {
      return listeModules;
   }
}
