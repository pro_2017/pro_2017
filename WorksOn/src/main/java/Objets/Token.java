package Objets;

import java.sql.Timestamp;

/**
 * Classe représentant un Token de connexion
 */
public class Token
{
   private int id;
   private String token;
   private Timestamp dateGeneration;
   private Employe employe;

   /**
    * Constructeur par défaut
    */
   public Token()
   {
      token = "invalide";
   }

   /**
    * Constructeur utilisé par Hibernate (entier).
    *
    * @param id             L'id du token récupéré
    * @param token          La string représentant le token récupéré
    * @param dateGeneration La date de génération du token récupéré
    * @param employe        L'employé possédant le token du token récupéré
    */
   public Token(int id, String token, Timestamp dateGeneration, Employe employe)
   {
      this(token, dateGeneration, employe);
      this.id = id;
   }

   /**
    * Constructeur utilisé lors de la création du token par le serveur
    *
    * @param token          La string représentant le nouveau token
    * @param dateGeneration La date de génération du nouveau token
    * @param employe        L'employé possédant le token du token récupéré
    */
   public Token(String token, Timestamp dateGeneration, Employe employe)
   {
      this.token = token;
      this.dateGeneration = dateGeneration;
      this.employe = employe;
   }

   /**
    * Constructeur utilisé lors de la création du token par le serveur
    *
    * @param token          La string représentant le nouveau token
    * @param dateGeneration La date de génération du nouveau token
    */
   public Token(String token, Timestamp dateGeneration)
   {
      this.token = token;
      this.dateGeneration = dateGeneration;
   }

   // GETTERS & SETTERS

   public void setToken(String token)
   {
      this.token = token;
   }

   public Timestamp getDateGeneration()
   {
      return dateGeneration;
   }

   public void setDateGeneration(Timestamp dateGeneration)
   {
      this.dateGeneration = dateGeneration;
   }

   public String getToken()
   {
      return token;
   }

   public int getId()
   {
      return id;
   }

   public void setId(int id)
   {
      this.id = id;
   }

   public Employe getEmploye()
   {
      return employe;
   }

   public void setEmploye(Employe employe)
   {
      this.employe = employe;
   }
}
