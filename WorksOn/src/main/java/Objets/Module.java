package Objets;

import Enums.Priorite;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe représentant un module
 */
public class Module
{
   private int id;
   private String nom;
   private Timestamp deadline;
   private Priorite priorite;
   private Etat etat;
   private String description;
   private Projet projet;
   private List<Employe> listeEmployes = new ArrayList<Employe>();
   ;

   /**
    * Constructeur par défaut
    */
   public Module()
   {
   }

   /**
    * Constructeur utilisé par Hibernate (entier).
    *
    * @param id            L'id du module récupéré
    * @param nom           Le nom du module récupéré
    * @param deadline      La deadline du module récupéré
    * @param priorite      La priorité du module récupéré
    * @param etat          L'état du module récupéré
    * @param description   La description du module récupéré
    * @param projet        Le projet qui contient le module récupéré
    * @param listeEmployes La liste des employés travaillants sur le module récupéré
    */
   public Module(int id, String nom, Timestamp deadline, Priorite priorite, Etat etat, String description, Projet projet, List<Employe> listeEmployes)
   {
      this(nom, deadline, priorite, etat, description, projet, listeEmployes);
      this.id = id;
   }

   /**
    * Constructeur utilisé lors de la création d'un module par un client.
    *
    * @param nom           Le nom du nouveau module
    * @param deadline      La deadline du nouveau module
    * @param priorite      La priorité du nouveau module
    * @param etat          L'état du nouveau module
    * @param description   La description du nouveau module
    * @param projet        Le projet qui contient le nouveau module
    * @param listeEmployes La liste des employés travaillants sur le nouveau module
    */
   public Module(String nom, Timestamp deadline, Priorite priorite, Etat etat, String description, Projet projet, List<Employe> listeEmployes)
   {
      this.nom = nom;
      this.deadline = deadline;
      this.priorite = priorite;
      this.etat = etat;
      this.description = description;
      this.projet = projet;
      this.listeEmployes = listeEmployes;
   }

   // GETTERS & SETTERS

   public int getId()
   {
      return id;
   }

   public void setId(int id)
   {
      this.id = id;
   }

   public String getNom()
   {
      return nom;
   }

   public void setNom(String nom)
   {
      this.nom = nom;
   }

   public Timestamp getDeadline()
   {
      return deadline;
   }

   public void setDeadline(Timestamp deadline)
   {
      this.deadline = deadline;
   }

   public Priorite getPriorite()
   {
      return priorite;
   }

   public void setPriorite(Priorite priorite)
   {
      this.priorite = priorite;
   }

   public Etat getEtat()
   {
      return etat;
   }

   public void setEtat(Etat etat)
   {
      this.etat = etat;
   }

   public String getDescription()
   {
      return description;
   }

   public void setDescription(String description)
   {
      this.description = description;
   }

   public Projet getProjet()
   {
      return projet;
   }

   public void setProjet(Projet projet)
   {
      this.projet = projet;
   }

   public List<Employe> getListeEmployes()
   {
      return listeEmployes;
   }

   public void setListeEmployes(List<Employe> listeEmployes)
   {
      this.listeEmployes = listeEmployes;
   }

   @Override
   public String toString()
   {
      return nom;
   }
}
