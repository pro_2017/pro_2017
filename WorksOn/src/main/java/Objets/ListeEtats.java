package Objets;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe représentant une liste d'utilisateurs.
 * Sa seule utilité est de "wrapper" une ArrayList pour faciliter la sérialisation.
 */
public class ListeEtats
{
   private List<Etat> listeEtats;

   /**
    * Constructeur par défaut
    */
   public ListeEtats()
   {
      listeEtats = new ArrayList<Etat>();
   }

   /**
    * Constructeur de base
    *
    * @param liste L'arrayList d'états à "wrapper"
    */
   public ListeEtats(List<Etat> liste)
   {
      this();
      listeEtats.addAll(liste);
   }

   /**
    * Méthode permettant de récupérer la liste sous forme normale
    *
    * @return L'arrayList d'états
    */
   public List<Etat> getListeEtats()
   {
      return listeEtats;
   }
}
