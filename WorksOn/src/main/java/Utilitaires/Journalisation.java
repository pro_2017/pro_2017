package Utilitaires;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe permettant de logger les erreurs qui arrivent dans le serveur afin de pouvoir vérifier / débugger les problèmes
 * <p>
 * Pour utiliser le logger, faire appel a une des fonctions suivantes :
 * <p>
 * Logger.getLOG.severe("string");
 * Logger.getLOG.warning("string");
 * Logger.getLOG.fine("string");
 * Logger.getLOG.finer("string");
 * Logger.getLOG.finest("string");
 */
public class Journalisation
{
   // Le journal de log pour les erreurs
   final private static java.util.logging.Logger ERR = java.util.logging.Logger.getLogger("Erreurs Serveur");

   // Le journal de log pour les connexions au serveur et les démarrages du serveur
   final private static java.util.logging.Logger LOG = java.util.logging.Logger.getLogger("Connexions Serveur");

   static
   {
      try
      {
         // Indique qu'on veut logger touts les niveaux
         LOG.setLevel(Level.FINEST);
         ERR.setLevel(Level.WARNING);

         // Indique dans quel fichier inscrire les entrées
         LOG.addHandler(new FileHandler("connexions.log", true));
         ERR.addHandler(new FileHandler("erreurs.log", true));
      } catch (IOException e)
      {
         e.printStackTrace();
      }
   }

   public static Logger getLOG()
   {
      return LOG;
   }

   public static Logger getERR()
   {
      return ERR;
   }
}
