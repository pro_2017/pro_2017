package Controleur;

import Client.SocketClient;
import Objets.*;
import Enums.*;
import Protocoles.ProtocoleWorksOn;
import Protocoles.ReponseConnexion;
import Utilitaires.Journalisation;

import java.io.IOException;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe regroupant tous les services qu'il est possible de demdander au controleur pour les vues. Ce controleur a pour
 * seul objectif de vérifié la validité des champs entrés par l'utilisateur et d'ensuite appelé les méthodes du
 * socketClient qui communiquera avec le serveur.
 */
public class Controleur
{
   private SocketClient socketClient; // Le socketClient responsable de la communication avec le serveur
   private Entreprise entreprise;     // L'entreprise du client connecté
   private Employe employe;           // L'employe représentant le client connecté
   private Projet projet;             // Le projet dans lequel le client fait des manipulations à un moment donné
   private Module module;             // Le module dans lequel le client fait des manipulations à un moment donné
   private Tache tache;               // La tache dans laquelle le client fait des manipulations à un moment donné
   private Log log;                   // Le log dans lequel le client fait des manipulations à un moment donné
   private ListeEtats listeEtats;     // La liste de tous les états disponible pour les projet et les taches

   /**
    * Constructeur du controleur on crée juste le socketClient les autres champs sont initialisés en temps voulu grâce
    * à des setters ou directement pendant l'exécution de certaines méthodes.
    */
   public Controleur()
   {
      socketClient = new SocketClient();
   }

   /**
    * Méthode permettant de récupérer dans la base de données la liste de tous les projets d'une entreprise. Cette
    * méthode n'est sensée être appelée que par un administrateur.
    *
    * @return La liste de tous les projets d'une entreprise
    */
   public ListeProjets listeProjets()
   {
      return socketClient.listeProjets();
   }

   /**
    * Méthode permettant de récupérer dans la base de données la liste des projets sur lesquels travaille un
    * utilisateur.
    *
    * @param idUtilisateur L'id de l'utilisateur dont on veut récupérer les projets
    * @return La liste des projets sur lesquels travaille un utilisateur
    */
   public ListeProjets listeProjetsUtilisateur(String idUtilisateur)
   {
      if (!estUnEntierValide(idUtilisateur))
      {
         return null;
      }

      return socketClient.listeProjetsUtilisateur(idUtilisateur);
   }

   /**
    * Méthode permettant de récupérer dans la base de données la liste des projets dont un chef est le responsable.
    * Cette méthode n'est sensé être appelée que par un chef de projet.
    *
    * @param idChef L'id du chef dont on veut récupérer les projets
    * @return La liste des projets gérés par ce chef
    */
   public ListeProjets listeProjetsChef(String idChef)
   {
      if (!estUnEntierValide(idChef))
      {
         return null;
      }

      return socketClient.listeProjetsChef(idChef);
   }

   /**
    * Méthode permettant de récupérer dans la base de données la liste des modules qui composent un projet donné.
    *
    * @param idProjet L'id du projet dont on veut récupérer les modules
    * @return La liste des modules composants le projet
    */
   public ListeModules listeModulesProjets(String idProjet)
   {
      if (!estUnEntierValide(idProjet))
      {
         return null;
      }

      return socketClient.listeModulesProjets(idProjet);
   }

   /**
    * Méthode permettant de récupérer dans la base de données la liste des taches qui appartiennent à un module et qui
    * sont celles de l'utilisateur.
    *
    * @param idUtilisateur L'id de l'utilisateur qui veut connaitre ses tâches
    * @param idModule      L'id du module dans laquelle chercher les tâches
    * @return La liste des tâches du module qui sont celles de l'utilisateur
    */
   public ListeTaches listeTachesUtilisateurSurModule(String idUtilisateur, String idModule)
   {
      if (!estUnEntierValide(idUtilisateur) || !estUnEntierValide(idModule))
      {
         return null;
      }

      return socketClient.listeTachesUtilisateurSurModule(idUtilisateur, idModule);
   }

   /**
    * Méthode permettant de récupérer dans la base de données la liste des taches d'un module (quelque soit
    * l'utilisateur).
    *
    * @param idModule L'id du module dont on veut récupérer les taches
    * @return La liste des taches du module
    */
   public ListeTaches listeTachesModule(String idModule)
   {
      if (!estUnEntierValide(idModule))
      {
         return null;
      }

      return socketClient.listeTachesModule(idModule);
   }

   /**
    * Méthode permettant de récupérer dans la base de données la liste des utilisateurs travaillants sur un module
    * donné. Cette méthode n'est sensée être appellée que par un chef de projet.
    *
    * @param idModule Le module dont on veut connaitre les différents employés asscociés
    * @return La liste des employés travaillants sur le module
    */
   public ListeEmployes listeUtilisateursModule(String idModule)
   {
      if (!estUnEntierValide(idModule))
      {
         return null;
      }

      return socketClient.listeUtilisateursModule(idModule);

   }

    /*
    public ListeEmployes listeUtilisateursProjet(String idProjet)
    {
        if(!estUnEntierValide(idProjet))
        {
            return null;
        }

        return socketClient.listeUtilisateursProjet(idProjet);
    }
    */

   /**
    * Méthode permettant de récupérer dans la base de données tous les employés travaillants dans une entreprise. Cette
    * méthode n'est sensée être appellée que par un administrateur.
    *
    * @return La liste des employés de l'entreprise
    */
   public ListeEmployes listeEmployes()
   {
      return socketClient.listeEmployes();
   }

   /**
    * Méthode permettant de récupérer dans la base de données un objet Employe dont on connait l'id.
    *
    * @param idEmploye L'id de l'employé dont on veut récupérer l'objet entier
    * @return L'objet Employe correspondant à cette id
    */
   public Employe employeParId(String idEmploye)
   {
      if (!estUnEntierValide(idEmploye))
      {
         return null;
      }

      return socketClient.employeParId(idEmploye);
   }

   /**
    * Méthode permettant de récupérer dans la base de données un objet Projet dont on connait l'id.
    *
    * @param idProjet L'id du projet dont on veut récupérer l'objet entier
    * @return L'objet Projet correspondant à cette id
    */
   public Projet projetParId(String idProjet)
   {
      if (!estUnEntierValide(idProjet))
      {
         return null;
      }

      return socketClient.projetParId(idProjet);
   }

   /**
    * Méthode permettant de récupérer dans la base de données un objet Module dont on connait l'id.
    *
    * @param idModule L'id du module dont on veut récupérer l'objet entier
    * @return L'objet Module correspondant à cette id
    */
   public Module moduleParId(String idModule)
   {
      if (!estUnEntierValide(idModule))
      {
         return null;
      }

      return socketClient.moduleParId(idModule);
   }

   /**
    * Méthode permettant de récupérer dans la base de données un objet Tache dont on connait l'id.
    *
    * @param idTache L'id de la tache dont on veut récupérer l'objet entier
    * @return L'objet Tache correspondant à cette id
    */
   public Tache tacheParId(String idTache)
   {
      if (!estUnEntierValide(idTache))
      {
         return null;
      }

      return socketClient.tacheParId(idTache);
   }

   /**
    * Méthode permettant de récupérer dans la base de données un objet Log dont on connait l'id.
    *
    * @param idLog L'id du log dont on veut récupérer l'objet entier
    * @return L'objet Log correspondant à cette id
    */
   public Log logParId(String idLog)
   {
      if (!estUnEntierValide(idLog))
      {
         return null;
      }

      return socketClient.logParId(idLog);
   }

   /**
    * Méthode permettant d'ajouter un employé dans la base de données. Cette méthode n'est sensée être appellée que par
    * un administrateur.
    *
    * @param nom      Le nom de l'employé à ajouter
    * @param prenom   Le prénom de l'employé à ajouter
    * @param username L'username (nom d'utilisateur unique à saisir à l'authentification) de l'employé à ajouter
    * @param password Le mot de passe de l'employé à ajouter
    * @param role     Le role de l'employé à ajouter
    * @return Une string valant ok si tout c'est passé correctement, une string contenant l'erreur sinon
    */
   public String creerEmploye(String nom, String prenom, String username, String password, Role role)
   {
      Employe employe = new Employe(nom, prenom, username, role, entreprise, new ArrayList<>(), password);

      return socketClient.creerEmploye(employe);
   }

   /**
    * Méthode permettant d'ajouter un projet dans la base de données. Cette méthode n'est sensée être appellée que par
    * un chef de projet.
    *
    * @param nom         Le nom du projet à ajouter
    * @param etat        L'état du projet à ajouter
    * @param description La description du projet à ajouter
    * @param dateRendu   La date de rendu du projet à ajouter
    * @return Une string valant ok si tout c'est passé correctement, une string contenant l'erreur sinon
    */
   public String creerProjet(String nom, Etat etat, String description, LocalDate dateRendu)
   {
      Timestamp dateConstruite = Timestamp.valueOf(dateRendu.atStartOfDay());

      Projet projet = new Projet(nom, etat, description, employe, dateConstruite);

      return socketClient.creerProjet(projet);
   }

   /**
    * Méthode permettant d'ajouter un module dans la base de données. Cette méthode n'est sensée être appellée que par
    * un chef projets.
    *
    * @param nom         Le nom du module à ajouter
    * @param deadline    La deadline du module à ajouter
    * @param priorite    La priorite du module à ajouter
    * @param etat        L'état du module à ajouter
    * @param description La description du module à ajouter
    * @param projet      Le projet qui contient le module à ajouter
    * @param employes    La liste des employes qu'on assigne au module à ajouter
    * @return Une string valant ok si tout c'est passé correctement, une string contenant l'erreur sinon
    */
   public String creerModule(String nom, LocalDate deadline, Priorite priorite, Etat etat, String description,
         Projet projet, List<Employe> employes)
   {
      Timestamp dateConstruite = Timestamp.valueOf(deadline.atStartOfDay());

      Module module = new Module(nom, dateConstruite, priorite, etat, description, projet, employes);

      return socketClient.creerModule(module);
   }

   /**
    * Méthode permettant d'ajouter une tache dans la base de données.
    *
    * @param nom               Nom de la tâche à ajouter
    * @param priorite          Priorite de la tâche à ajouter
    * @param heuresPlanifiees  Nombre d'heures planifiées pour la tâche à ajouter
    * @param minutesPlanifiees Nombre de minutes planifiées pour la tâche à ajouter
    * @param etat              Etat de la tâche à ajouter
    * @param module            Module auquel appartient la tâche à ajouter
    * @return Une string valant ok si tout c'est passé correctement, une string contenant l'erreur sinon
    */
   public String creerTache(String nom, Priorite priorite, String heuresPlanifiees, String minutesPlanifiees, Etat etat,
         Module module)
   {
      if (!estUneDureeMinutesValide(minutesPlanifiees))
      {
         return "Il y a au maximum 59 minutes dans une heure";
      }

      if (!estUnEntierValide(heuresPlanifiees))
      {
         return "Le nombre d'heures doit être un nombre";
      }

      Time dureePrevue = new Time(Integer.parseInt(heuresPlanifiees), Integer.parseInt(minutesPlanifiees), 0);

      Tache tache = new Tache(nom, priorite, dureePrevue, etat, module, employe);

      return socketClient.creerTache(tache);
   }

   /**
    * Méthode permettant d'ajouter un log dans la base de données.
    *
    * @param remarques Remarques du log à ajouter
    * @param debut     Temps de début du log à ajouter
    * @param fin       Temps de fin du log à ajouter
    * @param tache     Tache à laquelle appartient ce log
    * @return
    */
   public String creerLog(String remarques, Timestamp debut, Timestamp fin, Tache tache)
   {
      Log log = new Log(remarques, debut, fin, tache);

      return socketClient.creerLog(log);
   }

   /**
    * Méthode permettant de modifier un employe existant dans la base de données. Elle permet juste d'ajouter un module
    * pour cet utilisateur.
    *
    * @param employe       L'employe dans lequel à ajouter le module
    * @param nouveauModule Le module à ajouter dans l'employé
    * @return Une string valant ok si tout c'est passé correctement, une string contenant l'erreur sinon
    */
   public String modifierEmploye(Employe employe, Module nouveauModule)
   {
      employe.getListeDeModules().add(nouveauModule);

      return socketClient.modifierEmploye(employe);
   }

   /**
    * Méthode permettant de modifier un projet existant dans la base de données. Cette méthode n'est sensée être
    * appellée que par un chef de projet.
    *
    * @param ancienProjet        Le projet qui doit être modifié
    * @param nouvelEtat          Le nouvel état du projet
    * @param nouvelleDeadline    La nouvelle deadline du projet
    * @param nouvelleDescription La nouvelle description du projet
    * @return Une string valant ok si tout c'est passé correctement, une string contenant l'erreur sinon
    */
   public String modifierProjet(Projet ancienProjet, Etat nouvelEtat, LocalDate nouvelleDeadline,
         String nouvelleDescription)
   {
      Timestamp dateConstruite = Timestamp.valueOf(nouvelleDeadline.atStartOfDay());

      ancienProjet.setEtat(nouvelEtat);
      ancienProjet.setDateRendu(dateConstruite);
      ancienProjet.setDescription(nouvelleDescription);

      return socketClient.modifierProjet(ancienProjet);
   }

   /**
    * Méthode permettant de modifier un module existant dans la base de données. Cette méthode n'est sensée être
    * appellée que par un chef de projet.
    *
    * @param ancienModule        Le module qui doit être modifié
    * @param nouvelleDescription La nouvelle description du module
    * @param nouvelEtat          Le nouvel état du module
    * @param nouvellePriorite    La nouvelle priorité du module
    * @return Une string valant ok si tout c'est passé correctement, une string contenant l'erreur sinon
    */
   public String modifierModule(Module ancienModule, String nouvelleDescription, Etat nouvelEtat, Priorite nouvellePriorite)
   {
      ancienModule.setEtat(nouvelEtat);
      ancienModule.setPriorite(nouvellePriorite);
      ancienModule.setDescription(nouvelleDescription);

      return socketClient.modifierModule(ancienModule);
   }

   /**
    * Méthode permettant de modifier une tâche existante dans la base de données.
    *
    * @param ancienneTache La tâche qui doit être modifiée
    * @param nouvelEtat    Le nouvel état de la tâche
    * @return Une string valant ok si tout c'est passé correctement, une string contenant l'erreur sinon
    */
   public String modifierTache(Tache ancienneTache, Etat nouvelEtat)
   {
      ancienneTache.setEtat(nouvelEtat);

      return socketClient.modifierTache(ancienneTache);
   }

   /**
    * Méthode permettant de récupérer dans la base de données les logs existants pour une tâche donnée.
    *
    * @param idTache L'id de la tâche dont on veut connaitre les logs
    * @return La liste des logs associés à cette tache
    */
   public ListeLog listeLogsTache(String idTache)
   {
      if (!estUnEntierValide(idTache))
      {
         return null;
      }

      return socketClient.listeLogsTache(idTache);
   }

   /**
    * Méthode permettant de récupérer dans la base de données un objet Etat dont on connait l'id.
    *
    * @param idEtat L'id de l'état dont on veut récupérer l'objet entier
    * @return L'objet Etat correspondant à cette id
    */
   public Etat etatParId(String idEtat)
   {
      if (estUnEntierValide(idEtat))
      {
         return null;
      }

      return socketClient.etatParId(idEtat);
   }

   /**
    * Méthode permettant de récupérer dans la base de données tous les états disponibles.
    *
    * @return La liste de tous les états disponibles dans la base de données
    */
   public ListeEtats listeEtats()
   {
      ListeEtats reponse = socketClient.listeEtats();
      this.listeEtats = reponse;
      return listeEtats;
   }

   /**
    * Méthode appellée lors de l'authentification. Elle permet d'envoyer vers la base de donnée les credentials a
    * vérifier.
    *
    * @param username   Le nom d'utilisateur à vérifier
    * @param motDePasse Le mot de passe à vérifier
    * @return Une string valant ok si tout c'est passé correctement, une string contenant l'erreur sinon
    */
   public String authentification(String username, String motDePasse)
   {
      ReponseConnexion reponse;
      try
      {
         reponse = socketClient.authentification(username, motDePasse);
      } catch (IOException e)
      {
         return "Impossible d'établir la connexion";
      }

      if (reponse.getToken().getToken().equalsIgnoreCase("invalide"))
      {
         Journalisation.getLOG().severe("Nom d'utilisateur ou mot de passe incorrect");
         return "Nom d'utilisateur ou mot de passe incorrect.";
      }

      this.entreprise = reponse.getEmploye().getEntreprise();
      this.employe = reponse.getEmploye();

      return null;
   }

   /**
    * Méthode appellée lors de la ré-authentification. Elle permet d'envoyer vers la base de données le token
    * reçu lors de l'authentification afin de le valider ou non.
    *
    * @return Une string valant ok si tout c'est passé correctement, une string contenant l'erreur sinon
    */
   public String authentification()
   {
      boolean reauthentifiactionReussie = false;

      try
      {
         reauthentifiactionReussie = socketClient.authentification();
      } catch (IOException e)
      {
         return "Impossible d'établir la connexion";
      }

      if (!reauthentifiactionReussie)
      {
         Journalisation.getLOG().severe("La réauthentification a échouée, token invalide ou expiré");
         return "La réauthentification a échouée, token invalide ou expiré.";
      }

      return ProtocoleWorksOn.RSP_OK;
   }

   public String fermerConnexion()
   {
      try
      {
         socketClient.fermerSocket();
      } catch (IOException e)
      {
         return "La fermeture du socket a échouée";
      }

      return ProtocoleWorksOn.RSP_OK;
   }

   /**
    * Getter pour le socketClient du controleur
    *
    * @return Le socket client du controleur
    */
   public SocketClient getSocketClient()
   {
      return socketClient;
   }

   /**
    * Getter pour l'entreprise gardée dans le controleur
    *
    * @return L'entreprise du client actuel
    */
   public Entreprise getEntreprise()
   {
      return entreprise;
   }

   /**
    * Getter pour l'employé gardé dans le controleur
    *
    * @return L'employé représentant le client actuel
    */
   public Employe getEmploye()
   {
      return employe;
   }

   /**
    * Getter pour le projet gardé dans le controleur
    *
    * @return Le projet représentant l'éventuel projet sur lequel le client se trouve
    */
   public Projet getProjet()
   {
      return projet;
   }

   /**
    * Getter pour le module gardé dans le controleur
    *
    * @return Le module représentant l'éventuel module sur lequel le client se trouve
    */
   public Module getModule()
   {
      return module;
   }

   /**
    * Getter pour la tâche gardée dans le controleur
    *
    * @return La tâche représentant l'éventuelle tâche sur laquelle le client se trouve
    */
   public Tache getTache()
   {
      return tache;
   }

   /**
    * Getter pour le log gardé dans le controleur
    *
    * @return Le log représentant l'éventuel log sur lequel le client se trouve
    */
   public Log getLog()
   {
      return log;
   }

   /**
    * Getter pour la liste des états dans le controleur
    *
    * @return La liste complète des états disponibles
    */
   public ListeEtats getListeEtats()
   {
      return listeEtats;
   }

   /**
    * Setter pour le projet à garder dans le controleur
    *
    * @param projet Le projet dont il faut garder la référence dans le controleur
    */
   public void setProjet(Projet projet)
   {
      this.projet = projet;
   }

   /**
    * Setter pour le module à garder dans le controleur
    *
    * @param module Le module dont il faut garder la référence dans le controleur
    */
   public void setModule(Module module)
   {
      this.module = module;
   }

   /**
    * Setter pour la tâche à garder dans le controleur
    *
    * @param tache La tâche dont il faut garder la référence dans le controleur
    */
   public void setTache(Tache tache)
   {
      this.tache = tache;
   }

   /**
    * Setter pour le log à garder dans le controleur
    *
    * @param log Le log dont il faut garder la référence dans le controleur
    */
   public void setLog(Log log)
   {
      this.log = log;
   }

   /**
    * Setter pour la liste d'états à garder dans le controleur
    *
    * @param listeEtats La liste de tous les états disponibles dont il faut garder la référence dans le controleur
    */
   public void setListeEtats(ListeEtats listeEtats)
   {
      this.listeEtats = listeEtats;
   }

   /**
    * Méthode permettant de vérifier que la string passée en paramètre est un nombre
    *
    * @param id La string à vérifier
    * @return True si la string est un nombre, false sinon
    */
   private boolean estUnEntierValide(String id)
   {
      try
      {
         Integer.parseInt(id);
      } catch (NumberFormatException e)
      {
         return false;
      }

      return true;
   }

   /**
    * Méthode permettant de vérifier que la string passée en paramètre est un nombre valide pour les minutes
    *
    * @param minutes La string à vérifier
    * @return True si la string est valide, false sinon
    */
   private boolean estUneDureeMinutesValide(String minutes)
   {
      if (!estUnEntierValide(minutes))
      {
         return false;
      }

      int minutesEntieres = Integer.parseInt(minutes);

      return !(minutesEntieres < 0 || minutesEntieres >= 60);
   }
}
