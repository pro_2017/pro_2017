package Enums;

/**
 * Classe représentant une enumération pour les priorités
 */
public enum Priorite
{
   HAUTE,
   MOYENNE,
   BASSE;
}
